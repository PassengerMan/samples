﻿/*
Author: krolbakal@gmail.com
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameUI : MonoBehaviour 
{
	private Canvas canvas;
	private Dictionary<string, Button> buttons = new Dictionary<string, Button>();
	private Dictionary<string, Text> labels = new Dictionary<string, Text>();
	private Dictionary<string, Slider> sliders = new Dictionary<string, Slider>();
	private Dictionary<string, Dropdown> dropdowns = new Dictionary<string, Dropdown>();

	private Grid game_grid;

	void Awake()
	{
	}

	void Start () 
	{
		game_grid = FindObjectOfType<Grid>();

		Init();
		Setup();
	}

	void Update () 
	{
	
	}

	void Init()
	{
		canvas = GetComponent<Canvas>();

		Button[] c_buttons = GetAttachedComponents<Button>(gameObject);
		for(int i = 0; i < c_buttons.Length; i++)
		{
			buttons.Add(c_buttons[i].name, c_buttons[i]);
		}
			
		Text[] c_labels = GetAttachedComponents<Text>(gameObject);
		for(int i = 0; i < c_labels.Length; i++)
		{
			labels.Add(c_labels[i].name, c_labels[i]);
		}

		Slider[] c_sliders = GetAttachedComponents<Slider>(gameObject);
		for(int i = 0; i < c_sliders.Length; i++)
			sliders.Add(c_sliders[i].name, c_sliders[i]);

		Dropdown[] c_dropdowns = GetAttachedComponents<Dropdown>(gameObject);
		for(int i = 0; i < c_dropdowns.Length; i++)
			dropdowns.Add(c_dropdowns[i].name, c_dropdowns[i]);
	}

	void Setup()
	{
		labels["InfoText"].CrossFadeAlpha(0, 0.0f, false);

		buttons["GenerateButton"].onClick.AddListener(() =>
		{
			game_grid.Generate(-1);
		});

		buttons["FindPathButton"].onClick.AddListener(() =>
		{
			int path_length = game_grid.FindPath();
			if(path_length == 0)
				StartCoroutine(AnimateInfoText());
		});

		//Map Size
		labels["MapSizeVal"].text = game_grid.grid_size.ToString();
		sliders["MapSizeSlider"].value = game_grid.grid_size;
		sliders["MapSizeSlider"].onValueChanged.AddListener((float val) =>
		{
			game_grid.grid_size = (int)val;
			labels["MapSizeVal"].text = game_grid.grid_size.ToString();
		});

		//Obstacle count
		labels["ObstacleCountVal"].text = game_grid.number_of_obstacles.ToString();
		sliders["ObstacleCountSlider"].value = game_grid.number_of_obstacles;
		sliders["ObstacleCountSlider"].onValueChanged.AddListener((float val) =>
		{
			game_grid.number_of_obstacles = (int)val;
			labels["ObstacleCountVal"].text = game_grid.number_of_obstacles.ToString();
		});

		dropdowns["AlgorithmDropdown"].onValueChanged.AddListener((int val) =>
		{
			game_grid.current_pathfinding_component_index = val;
		});

		//Save
		buttons["SaveButton"].onClick.AddListener(() =>
		{
			SaveGameGridMap save_game  = FindObjectOfType<SaveGameGridMap>();

			save_game.Save("Map01.txt", game_grid);
		});

		//Load
		buttons["LoadButton"].onClick.AddListener(() =>
		{
			SaveGameGridMap save_game  = FindObjectOfType<SaveGameGridMap>();
			save_game.Load("/Map01.txt", game_grid);

			sliders["MapSizeSlider"].value = game_grid.grid_size;
			sliders["ObstacleCountSlider"].value = game_grid.number_of_obstacles;

			game_grid.Generate(game_grid.seed);
		});
	}

	public IEnumerator AnimateInfoText()
	{
		labels["InfoText"].CrossFadeAlpha(1, 0.2f, false);
		yield return new WaitForSeconds(1.0f);
		labels["InfoText"].CrossFadeAlpha(0, 1.0f, false);
		yield return null;
	}

	T[] GetAttachedComponents<T>(GameObject game_object)
	{
		List<T> found_components = new List<T>();

		for(int i = 0; i < game_object.transform.childCount; i++)
		{
			Transform child_transform = game_object.transform.GetChild(i);
			T[] wanted_components = child_transform.GetComponents<T>();
			if(wanted_components.Length > 0)
				found_components.AddRange(wanted_components);
		}

		return found_components.ToArray();
	}
}
