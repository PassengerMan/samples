﻿/*
Author: krolbakal@gmail.com
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridObstacleGroup : MonoBehaviour 
{
	public Vector2 obstacle_size;
	public GameObject obstacle_prefab;

	//
	protected float grid_spacing_margin;
	public Vector2 grid_position;
	protected Vector3 world_position;
	List<GameObject> obstacles = new List<GameObject>();

	public virtual void Awake () 
	{
		
	}	

	public virtual void Start () 
	{
	
	}

	public void ClearObstacles()
	{
		for(int i = 0; i < obstacles.Count; i++)
		{
			Destroy(obstacles[i]);
		}
		obstacles.Clear();
	}

	public virtual void Update () 
	{
	
	}

	public virtual void OnValidate()
	{
		obstacle_size.x = (float)Mathf.Clamp((int)obstacle_size.x, 1, 2); 
		obstacle_size.y = (float)Mathf.Clamp((int)obstacle_size.y, 1, 2);
	}

	public virtual void Init(Vector2 obstacle_size, float grid_spacing_margin, Vector2 grid_position)
	{
		this.grid_spacing_margin = grid_spacing_margin;
		this.grid_position = grid_position;
		this.obstacle_size = obstacle_size;

		MeshFilter obstacle_mesh = obstacle_prefab.gameObject.GetComponentInChildren<MeshFilter>(true);
		Bounds obstacle_bounds = obstacle_mesh.sharedMesh.bounds;

		gameObject.transform.localPosition += new Vector3(0, obstacle_bounds.extents.y*2.0f, 0);
	}

	public virtual void GenerateObstacle()
	{
		Quaternion obstacle_rot = Quaternion.identity;
		Vector3 obstacle_pos = Vector3.zero;

		MeshFilter obstacle_mesh = obstacle_prefab.gameObject.GetComponentInChildren<MeshFilter>(true);
		Bounds obstacle_bounds = obstacle_mesh.sharedMesh.bounds;

		float obstacle_spacing = (obstacle_bounds.extents.x*2.0f) + grid_spacing_margin;

		for(int y = 0; y < obstacle_size.y; y++)
		{
			obstacle_pos.z = (y*obstacle_spacing);
			for(int x = 0; x < obstacle_size.x; x++)
			{
				obstacle_pos.x = (x*obstacle_spacing);
				GameObject obstacle_tile = 
					(GameObject)Instantiate(obstacle_prefab, gameObject.transform.position + obstacle_pos, obstacle_rot);

				obstacles.Add(obstacle_tile);
			}
		}
	}

	public static Vector2 GetRandomObstacleSize()
	{
		Vector2 random_obstacle_size = Vector2.one;

		random_obstacle_size.x = Random.Range((int)1, 3);
		random_obstacle_size.y = Random.Range((int)1, 3);

		return random_obstacle_size;
	}
}
