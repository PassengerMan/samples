﻿/*
Author: krolbakal@gmail.com
*/

using UnityEngine;
using System.Collections;

public class GridObstacle : GridTile
{
	public override void Awake () 
	{
		base.Awake();

		mesh_renderer.material.color = new Color(0.75f, 0, 0);
	}	

	public override void Start () 
	{
	
	}

	public override void Update () 
	{
	
	}
}
