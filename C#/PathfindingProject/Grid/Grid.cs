﻿/*
Author: krolbakal@gmail.com
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TileType
{
	Normal,
	Obstacle
}

public struct TileInfo
{
	public TileType tile_type;
	public GameObject tile_object;
}

public class GridData
{
	public Dictionary<Vector2, TileInfo> grid_tiles;
	public List<GameObject> obstacles;
	public List<Vector2> non_occupied_positions;

	static List<Vector2> neighbour_offsets = new List<Vector2> 
		{ new Vector2(-1, 0), new Vector2(1, 0), new Vector2(0, -1), new Vector2(0, 1) };

	public GridData()
	{
		grid_tiles = new Dictionary<Vector2, TileInfo>();
		obstacles = new List<GameObject>();
		non_occupied_positions = new List<Vector2>();
	}

	public void Init()
	{

	}

	public void ClearData()
	{
		for(int i = 0; i < obstacles.Count; i++)
		{
			obstacles[i].GetComponent<GridObstacleGroup>().ClearObstacles();
			GameObject.Destroy(obstacles[i]);
		}
			
		foreach(Vector2 tile_grid_pos in grid_tiles.Keys)
		{
			GameObject tile = grid_tiles[tile_grid_pos].tile_object;
			GameObject.Destroy(tile);
		}	

		non_occupied_positions.Clear();
		obstacles.Clear();
		grid_tiles.Clear();
	}

	public void AddTile(Vector2 tile_grid_position, TileInfo tile_info)
	{
		grid_tiles.Add(tile_grid_position, tile_info);
		non_occupied_positions.Add(tile_grid_position);
	}

	public Vector2 GetRandomTilePosition()
	{
		Vector2 tile_position = -1*Vector2.one;

		if(non_occupied_positions.Count <= 0)
			return tile_position;

		int random_tile_index = Random.Range(0, non_occupied_positions.Count - 1);
		tile_position = non_occupied_positions[random_tile_index];

		return tile_position;
	}

	public bool CanPlaceObstacle(Vector2 grid_position, Vector2 obstacle_size)
	{
		for(int y = (int)grid_position.y; y < (int)grid_position.y + (int)obstacle_size.y; y++)
		{
			for(int x = (int)grid_position.x; x < (int)grid_position.x + (int)obstacle_size.x; x++)
			{
				Vector2 current_grid_position = Vector2.zero;
				current_grid_position.Set(x, y);

				if(grid_tiles.ContainsKey(current_grid_position) == false)
				{
					return false;
				}

				if(grid_tiles[current_grid_position].tile_type == TileType.Obstacle)
				{
					return false;
				}
			}
		}
		return true;
	}

	public void PlaceObstacle(GameObject obstacle, Vector2 grid_position, Vector2 obstacle_size)
	{
		for(int y = (int)grid_position.y; y < (int)grid_position.y + (int)obstacle_size.y; y++)
		{
			for(int x = (int)grid_position.x; x < (int)grid_position.x + (int)obstacle_size.x; x++)
			{
				Vector2 current_grid_position = Vector2.zero;
				current_grid_position.Set(x, y);

				TileInfo tile_info = grid_tiles[current_grid_position];
				tile_info.tile_type = TileType.Obstacle;

				grid_tiles[current_grid_position] = tile_info;

				MeshRenderer current_tile_renderer = 
					grid_tiles[current_grid_position].tile_object.GetComponent<MeshRenderer>();

				current_tile_renderer.material.color = new Color(1.0f, 0.5f, 0);

				obstacles.Add(obstacle);

				non_occupied_positions.Remove(current_grid_position);
			}
		}
	}

	public List<Vector2> GetNeighbours(Vector2 tile_grid_position)
	{
		List<Vector2> neighbours = new List<Vector2>();

		for(int i = 0; i < neighbour_offsets.Count; i++)
		{
			Vector2 neighbour_pos = tile_grid_position + neighbour_offsets[i];

			if(grid_tiles.ContainsKey(neighbour_pos))
				neighbours.Add(neighbour_pos);
		}

		return neighbours;
	}

	public TileType GetTileType(Vector2 tile_grid_position)
	{
		return grid_tiles[tile_grid_position].tile_type;
	}
}

public class Grid : MonoBehaviour 
{
	//editable variables
	public int grid_size;
	public int number_of_obstacles;
	[Range (0, 0.5f)]
	public float grid_spacing_margin;
	public GameObject tile_prefab;
	public GameObject obstacle_prefab;
	public int seed;

	//private variables
	protected LineRenderer line_renderer;

	protected GridData grid_data;

	public GridData GridDataVar
	{
		set { grid_data = value; }
		get { return grid_data; }
	}

	protected float grid_spacing;

	protected Vector2 start_grid_position;
	protected Vector2 destination_grid_position;

	protected List<PathfindingBase> pathfinding_components = new List<PathfindingBase>();
	public int current_pathfinding_component_index = 0;
	public PathfindingBase CurrentPathfindingComponent 
	{
		get { return pathfinding_components[current_pathfinding_component_index]; }
	}

	protected List<Vector2> found_path = new List<Vector2>();

	public virtual void Awake () 
	{
		Init();
	}	

	public virtual void Start () 
	{
		pathfinding_components.Add(new AStarPathfinding());
		pathfinding_components.Add(new DijkstraPathfinding());
	}

	public virtual void Update () 
	{
	}

	public virtual void OnValidate()
	{
		if(grid_size < 10)
			grid_size = 10;
	}

	public virtual void Init()
	{
		line_renderer = GetComponent<LineRenderer>();

		MeshFilter tile_mesh = tile_prefab.gameObject.GetComponentInChildren<MeshFilter>(true);
		Bounds tile_bounds = tile_mesh.sharedMesh.bounds;

		grid_spacing = (tile_bounds.extents.x*2.0f) + grid_spacing_margin;	

		grid_data = new GridData();
		grid_data.Init();	
	}

	public virtual void ClearGrid()
	{
		line_renderer.SetVertexCount(0);

		found_path.Clear();
		grid_data.ClearData();
	}

	public virtual void Generate(int in_seed)
	{
		if(in_seed < 0)
			Random.seed = Random.Range(0, 205059);
		else 
			Random.seed = in_seed;

		seed = Random.seed;

		ClearGrid();

		GenerateGrid();
		GenerateObstacles();
		GenerateRandomStartEndPositions();
	}

	public virtual void GenerateGrid()
	{
		Quaternion tile_rot = Quaternion.identity;
		Vector3 tile_pos = Vector3.zero;

		for(int y = 0; y < grid_size; y++)
		{
			tile_pos.z = (y*grid_spacing);
			for(int x = 0; x < grid_size; x++)
			{
				tile_pos.x = (x*grid_spacing);
				GameObject spawned_tile = (GameObject)Instantiate(tile_prefab, gameObject.transform.position + tile_pos, tile_rot); 

				//GridTile grid_tile = spawned_tile.GetComponent<GridTile>();

				TileInfo tile_info;
				tile_info.tile_type = TileType.Normal;
				tile_info.tile_object = spawned_tile;

				grid_data.AddTile(new Vector2(x, y), tile_info);
			}
		}
	}

	public virtual void GenerateObstacles()
	{
		int infinite_loop_limit = 5;

		for(int i = 0; i < number_of_obstacles; i++)
		{
			Vector2 random_tile_pos = grid_data.GetRandomTilePosition();
			Vector2 random_obstacle_size = GridObstacleGroup.GetRandomObstacleSize();

			bool can_place_obstacle = grid_data.CanPlaceObstacle(random_tile_pos, random_obstacle_size);
			if(can_place_obstacle == false)
			{
				infinite_loop_limit--;

				if(infinite_loop_limit <= 0)
				{
					infinite_loop_limit = 5;
					continue;
				}

				--i;
				continue;
			}

			Vector3 obstacle_group_world_pos = 
				gameObject.transform.position + new Vector3(random_tile_pos.x, 0, random_tile_pos.y) * grid_spacing;

			GameObject obstacle_obj = (GameObject)Instantiate(
				obstacle_prefab, 
				obstacle_group_world_pos,
				Quaternion.identity);

			GridObstacleGroup obstacle_group = obstacle_obj.GetComponent<GridObstacleGroup>();
			obstacle_group.Init(random_obstacle_size, grid_spacing_margin, random_tile_pos);
			obstacle_group.GenerateObstacle();

			grid_data.PlaceObstacle(obstacle_obj, random_tile_pos, obstacle_group.obstacle_size);
		}
	}

	public virtual void GenerateRandomStartEndPositions()
	{
		start_grid_position = grid_data.GetRandomTilePosition();
		destination_grid_position = grid_data.GetRandomTilePosition();

		MeshRenderer start_tile_renderer = 
			grid_data.grid_tiles[start_grid_position].tile_object.GetComponent<MeshRenderer>();

		MeshRenderer destination_tile_renderer = 
			grid_data.grid_tiles[destination_grid_position].tile_object.GetComponent<MeshRenderer>();

		start_tile_renderer.material.color = new Color(0, 0.75f, 0);
		destination_tile_renderer.material.color = new Color(0, 0, 0.75f);
	}

	public int FindPath()
	{
		CurrentPathfindingComponent.FindPath(grid_data, start_grid_position, destination_grid_position, out found_path);

		line_renderer.SetVertexCount(found_path.Count);

		for(int i = 0; i < found_path.Count - 1; i++)
		{
			Vector2 grid_pos_start = found_path[i];
			Vector2 grid_pos_end = found_path[i+1];

			Vector3 tile_world_pos1 = grid_data.grid_tiles[grid_pos_start].tile_object.transform.position + new Vector3(0, 0.75f, 0);
			Vector3 tile_world_pos2 = grid_data.grid_tiles[grid_pos_end].tile_object.transform.position + new Vector3(0, 0.75f, 0);

			line_renderer.SetPosition(i, tile_world_pos1);
			line_renderer.SetPosition(i+1, tile_world_pos2);
		}

		return found_path.Count;
	}
}
