﻿/*
Author: krolbakal@gmail.com
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DijkstraPathfinding : PathfindingBase
{
	public override void FindPath(GridData grid_data, Vector2 start, Vector2 destination, out List<Vector2> found_path)
	{
		found_path = new List<Vector2>();

		List<Vector2> Q = new List<Vector2>();

		Dictionary<Vector2, int> dist = new Dictionary<Vector2, int>();
		Dictionary<Vector2, Vector2> prev = new Dictionary<Vector2, Vector2>();

		foreach(Vector2 key in grid_data.grid_tiles.Keys)
		{
			if(grid_data.grid_tiles[key].tile_type == TileType.Obstacle)
				continue;

			dist[key] = 99999999;
			Q.Add(key);
		}

		dist[start] = 0;

		while(Q.Count > 0)
		{
			Q.Sort((x,y) => dist[x] - dist[y]);

			Vector2 min_dist_pos = Q[0];
			if(min_dist_pos == destination)
				break;

			Q.Remove(min_dist_pos);

			List<Vector2> tile_neighbours = grid_data.GetNeighbours(min_dist_pos);
			for(int i = 0; i < tile_neighbours.Count; i++)
			{
				Vector2 neighbour_tile_pos = tile_neighbours[i];

				if(grid_data.grid_tiles[neighbour_tile_pos].tile_type == TileType.Obstacle)
					continue;

				int distance = dist[min_dist_pos] + GetDistance(ref min_dist_pos, ref neighbour_tile_pos);

				if(distance < dist[neighbour_tile_pos])
				{
					dist[neighbour_tile_pos] = distance;
					prev[neighbour_tile_pos] = min_dist_pos;
				}
			}
		}

		Vector2 current = destination;
		while(prev.ContainsKey(current))
		{
			found_path.Add(current);
			current = prev[current];
		}
	}

	//manhattan distance
	public int GetDistance(ref Vector2 src, ref Vector2 dst)
	{
		int dx = (int)Mathf.Abs(src.x - dst.x);
		int dy = (int)Mathf.Abs(src.y - dst.y);

		return (int)1 * (dx + dy);
	}
}
