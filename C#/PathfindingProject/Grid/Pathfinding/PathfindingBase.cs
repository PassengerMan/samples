﻿/*
Author: krolbakal@gmail.com
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathfindingBase : IPathfindingInterface
{
	public virtual void FindPath(GridData grid_data, Vector2 start, Vector2 destination, out List<Vector2> found_path)
	{
		found_path = null;
	}
}
