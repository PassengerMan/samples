﻿/*
Author: krolbakal@gmail.com
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IPathfindingInterface
{
	void FindPath(GridData grid_data, Vector2 start, Vector2 destination, out List<Vector2> found_path);
}
