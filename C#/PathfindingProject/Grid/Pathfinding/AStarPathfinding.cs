﻿/*
Author: krolbakal@gmail.com
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public struct PositionPriorityStruct: IComparable<PositionPriorityStruct>
{
	public Vector2 tile_position;
	public int priority;

	public PositionPriorityStruct(Vector2 in_tile_pos, int in_priority)
	{
		tile_position = in_tile_pos;
		priority = in_priority;
	}

	public int CompareTo(PositionPriorityStruct pps)
	{
		if(pps.priority < priority)
			return 1;
		else
			return -1;
	}
}

public class AStarPathfinding : PathfindingBase
{
	public override void FindPath(GridData grid_data, Vector2 start, Vector2 destination, out List<Vector2> found_path)
	{
		found_path = new List<Vector2>();
		
		SortedList<PositionPriorityStruct, Vector2> open_list = new SortedList<PositionPriorityStruct, Vector2>();

		Dictionary<Vector2, Vector2> came_from = new Dictionary<Vector2, Vector2>();
		Dictionary<Vector2, int> cost_so_far = new Dictionary<Vector2, int>();

		came_from.Add(start, start);
		cost_so_far.Add(start, 0);

		open_list.Add(new PositionPriorityStruct(start, 0), start);

		while(open_list.Count > 0)
		{
			Vector2 current_pos = open_list.Values[0];
			if(current_pos == destination)
				break;

			open_list.RemoveAt(0);

			List<Vector2> tile_neighbours = grid_data.GetNeighbours(current_pos);

			for(int i = 0; i < tile_neighbours.Count; i++)
			{
				Vector2 neighbour_tile_pos = tile_neighbours[i];

				int additional_cost = 1;

				if(grid_data.GetTileType(neighbour_tile_pos) == TileType.Obstacle || open_list.ContainsValue(neighbour_tile_pos))
				{
					continue;
				}

				int new_cost = cost_so_far[current_pos] + additional_cost;
				int next_cost = cost_so_far.ContainsKey(neighbour_tile_pos) ? cost_so_far[neighbour_tile_pos] : 0;

				if(cost_so_far.ContainsKey(neighbour_tile_pos) == false || new_cost < next_cost)
				{
					cost_so_far.Add(neighbour_tile_pos, new_cost);
					int priority = new_cost + GetDistance(ref neighbour_tile_pos, ref destination);

					open_list.Add(new PositionPriorityStruct(neighbour_tile_pos, priority), neighbour_tile_pos);
					came_from.Add(neighbour_tile_pos, current_pos);
				}
			}
		}

		Vector2 current = destination;
		while(current != start)
		{
			if(came_from.ContainsKey(current) == false)
				break;

			current = came_from[current];
			found_path.Add(current);
		}
	}

	//manhattan distance
	public int GetDistance(ref Vector2 src, ref Vector2 dst)
	{
		int dx = (int)Mathf.Abs(src.x - dst.x);
		int dy = (int)Mathf.Abs(src.y - dst.y);

		return (int)0.5f * (dx + dy);
	}
}
