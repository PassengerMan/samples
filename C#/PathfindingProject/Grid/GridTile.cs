﻿/*
Author: krolbakal@gmail.com
*/

using UnityEngine;
using System.Collections;

[RequireComponent (typeof(MeshFilter), typeof(MeshRenderer))] 
public class GridTile : MonoBehaviour 
{
	public Vector2 TileGridPosition
	{
		set
		{
			tile_position = value;
		}

		get
		{
			return tile_position;
		}
	}

	protected Vector2 tile_position;
	protected MeshRenderer mesh_renderer;

	public virtual void Awake () 
	{
		mesh_renderer = GetComponent<MeshRenderer>();
	}	

	public virtual void Start () 
	{
	
	}

	public virtual void Update () 
	{
	
	}
}
