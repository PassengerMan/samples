﻿/*
Author: krolbakal@gmail.com
*/

using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveGameBase : MonoBehaviour 
{
	protected string folder_name = "/Saves/";
	protected string file_name;
	protected FileStream file_stream;

	protected FileStream CreateFile(string in_file_name)
	{
		file_name = in_file_name;

		if(Directory.Exists(Application.dataPath + folder_name) == false)
			Directory.CreateDirectory(Application.dataPath + folder_name);

		return File.Create(Application.dataPath + folder_name + in_file_name);
	}

	protected FileStream OpenFile(string in_file_name, FileAccess file_access)
	{
		return File.Open(Application.dataPath + folder_name + in_file_name, FileMode.OpenOrCreate, file_access);
	}

	protected void CloseFile(FileStream in_file_stream)
	{
		in_file_stream.Close();
	}

	public virtual void Save(string in_file_name, GameObject in_obj)
	{
		file_stream = CreateFile(in_file_name);

		BinaryFormatter binary_formatter = new BinaryFormatter();
		binary_formatter.Serialize(file_stream, in_obj);

		CloseFile(file_stream);
	}

	public virtual void Load(string in_file_name, out GameObject in_obj)
	{
		file_stream = OpenFile(in_file_name, FileAccess.Read);

		BinaryFormatter binary_formatter = new BinaryFormatter();
		in_obj = (GameObject)binary_formatter.Deserialize(file_stream);

		CloseFile(file_stream);
	}
}
