﻿/*
Author: krolbakal@gmail.com
*/

using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveGameGridMap : SaveGameBase
{
	public void Save(string in_file_name, Grid grid)
	{
		file_stream = CreateFile(in_file_name);

		BinaryWriter binary_writer = new BinaryWriter(file_stream);

		binary_writer.Write(grid.seed);
		binary_writer.Write(grid.grid_size);
		binary_writer.Write(grid.number_of_obstacles);

		Debug.Log(grid.seed + ", " + grid.grid_size + ", " + grid.number_of_obstacles);

		CloseFile(file_stream);

		Debug.Log("Grid Saved");
	}

	public void Load(string in_file_name, Grid grid)
	{

		file_stream = OpenFile(in_file_name, FileAccess.Read);

		BinaryReader binary_reader = new BinaryReader(file_stream);

		int seed = binary_reader.ReadInt32();
		int map_size = binary_reader.ReadInt32();
		int obstacles_size = binary_reader.ReadInt32();

		grid.seed = seed;
		grid.grid_size = map_size;
		grid.number_of_obstacles = obstacles_size;

		Debug.Log(seed + ", " + map_size + ", " + obstacles_size);

		CloseFile(file_stream);

		Debug.Log("Grid Loaded");
	}
}
