// Fill out your copyright notice in the Description page of Project Settings.

#include "Pandemia.h"
#include "PANCardTableWidget.h"
#include "Runtime/UMG/Public/Components/CanvasPanelSlot.h"
#include "Player/PANPlayerController.h"

UPANCardTableWidget::UPANCardTableWidget(const FObjectInitializer& _oi) : Super(_oi)
{
	bCardPressed = false;
	bCardReleased = false;
	bRecordCardPressMousePosition = false;
	CardDirection = 0.0f;
	DeltaMult = 1.0f;
}

void UPANCardTableWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UPANCardTableWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
	HandleCardMovement(bCardPressed, bCardReleased);
}

FReply UPANCardTableWidget::NativeOnMouseMove(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseMove(InGeometry, InMouseEvent);
	CurrentMousePos = InGeometry.AbsoluteToLocal(InMouseEvent.GetScreenSpacePosition()) - (InGeometry.Size / 2.0f);

	UCanvasPanelSlot* canvasSlot = Cast<UCanvasPanelSlot>(CardWidget->Slot);
	CardDirection = canvasSlot->GetPosition().X / (InGeometry.Size.X / 2.0f);

	if (bRecordCardPressMousePosition)
	{
		OnCardPressedMousePos = CurrentMousePos;

		FVector2D shiftedSize = InGeometry.Size - (InGeometry.Size / 2.0f);
		float distance = 1.0f - FMath::Abs((FMath::Abs(OnCardPressedMousePos.X) - shiftedSize.X) / (InGeometry.Size.X));
		DeltaMult = distance * 2.0f;
		UE_LOG(GameLog, Warning, TEXT("Press Distance to Wall: %f"), DeltaMult);
		bRecordCardPressMousePosition = false;
	}

	MouseDelta = InMouseEvent.GetCursorDelta() * DeltaMult;

	UE_LOG(GameLog, Warning, TEXT("Card Direction %f"), CardDirection);
	//UE_LOG(GameLog, Warning, TEXT("MouseDelta %f, %f"), MouseDelta.X, MouseDelta.Y);
	return FReply::Handled();
}

void UPANCardTableWidget::RegisterCard(UPANCardWidget* _cardWidget)
{
	CardWidget = _cardWidget;

	UCanvasPanelSlot* canvasSlot = Cast<UCanvasPanelSlot>(CardWidget->Slot);
	StartCardPostition = canvasSlot->GetPosition();

	CardWidget->CardOnPressed.BindUObject(this, &UPANCardTableWidget::OnCardPressed);
	CardWidget->CardOnReleased.BindUObject(this, &UPANCardTableWidget::OnCardReleased);
}

void UPANCardTableWidget::RegisterCardDescription(UPANComplexTextWidget* _cardDescriptionWidget)
{
	CardDescriptionWidget = _cardDescriptionWidget;
}

void UPANCardTableWidget::RegisterChoices(UTextBlock* _choice1, UTextBlock* _choice2)
{
	Choice1Block = _choice1;
	Choice2Block = _choice2;

	FSlateColor choice1Color = GetColor(EPANColor::White);
	FSlateColor choice2Color = GetColor(EPANColor::White);

	Choice1Block->SetColorAndOpacity(FSlateColor(FLinearColor(choice1Color.GetSpecifiedColor().R, choice1Color.GetSpecifiedColor().G, choice1Color.GetSpecifiedColor().B, 0.0f)));
	Choice2Block->SetColorAndOpacity(FSlateColor(FLinearColor(choice2Color.GetSpecifiedColor().R, choice2Color.GetSpecifiedColor().G, choice2Color.GetSpecifiedColor().B, 0.0f)));
}

void UPANCardTableWidget::OnCardPressed(int32 _param)
{
	bCardPressed = true;
	bCardReleased = false;

	bRecordCardPressMousePosition = true;
	UE_LOG(GameLog, Warning, TEXT("Card Pressed"));
}

void UPANCardTableWidget::OnCardReleased(int32 _param)
{
	bCardPressed = false;
	bCardReleased = true;
	
	DeltaMult = 1.0f;

	if (CardDirection > 0.3f)
	{
		//chosen right
		GetPANPlayerController()->ExecuteCardChoice(1);
	}
	else if (CardDirection < -0.3f)
	{
		//chosen left
		GetPANPlayerController()->ExecuteCardChoice(-1);
	}
	else
	{
		//didn't choose
	}


	UE_LOG(GameLog, Warning, TEXT("Card Released"));
}

void UPANCardTableWidget::HandleCardMovement(bool _bCardPressed, bool _bCardReleased)
{
	if (_bCardPressed)
	{
		if (CardDirection > 0.1f)
		{
			//visualize
			GetPANPlayerController()->VisualizeCardChoice(1);
		}
		else if (CardDirection < -0.1f)
		{
			//visualize
			GetPANPlayerController()->VisualizeCardChoice(-1);
		}
		else
		{
			//unvisualize
			GetPANPlayerController()->VisualizeCardChoice(0);
		}

		UCanvasPanelSlot* canvasSlot = Cast<UCanvasPanelSlot>(CardWidget->Slot);

		canvasSlot->SetPosition(FVector2D(canvasSlot->GetPosition().X + MouseDelta.X, canvasSlot->GetPosition().Y));
		CardWidget->SetRenderAngle(CardDirection * 15.0f);

		float margin = 0.01f;
		//UE_LOG(GameLog, Warning, TEXT("CardDirection: %f"), CardDirection);
		if (CardDirection > margin)
		{
			float alpha1 = FMath::Lerp(0.0f, 1.0f, CardDirection*2.0f);
			//float alpha2 = FMath::Lerp(1.0f, 0.0f, CardDirection);

			FLinearColor color1 = FLinearColor(Choice1Block->ColorAndOpacity.GetSpecifiedColor().R, Choice1Block->ColorAndOpacity.GetSpecifiedColor().G, Choice1Block->ColorAndOpacity.GetSpecifiedColor().B, alpha1);
			FLinearColor color2 = FLinearColor(Choice2Block->ColorAndOpacity.GetSpecifiedColor().R, Choice2Block->ColorAndOpacity.GetSpecifiedColor().G, Choice2Block->ColorAndOpacity.GetSpecifiedColor().B, 0.0f);

			Choice1Block->SetColorAndOpacity(FSlateColor(color1));
			Choice2Block->SetColorAndOpacity(FSlateColor(color2));
		}
		else if (CardDirection < -margin)
		{
			//float alpha1 = FMath::Lerp(1.0f, 0.0f, -CardDirection);
			float alpha2 = FMath::Lerp(0.0f, 1.0f, -(CardDirection*2.0f));

			FLinearColor color1 = FLinearColor(Choice1Block->ColorAndOpacity.GetSpecifiedColor().R, Choice1Block->ColorAndOpacity.GetSpecifiedColor().G, Choice1Block->ColorAndOpacity.GetSpecifiedColor().B, 0.0f);
			FLinearColor color2 = FLinearColor(Choice2Block->ColorAndOpacity.GetSpecifiedColor().R, Choice2Block->ColorAndOpacity.GetSpecifiedColor().G, Choice2Block->ColorAndOpacity.GetSpecifiedColor().B, alpha2);

			Choice1Block->SetColorAndOpacity(FSlateColor(color1));
			Choice2Block->SetColorAndOpacity(FSlateColor(color2));
		}
		else
		{
			FLinearColor color1 = FLinearColor(Choice1Block->ColorAndOpacity.GetSpecifiedColor().R, Choice1Block->ColorAndOpacity.GetSpecifiedColor().G, Choice1Block->ColorAndOpacity.GetSpecifiedColor().B, 0.0f);
			FLinearColor color2 = FLinearColor(Choice2Block->ColorAndOpacity.GetSpecifiedColor().R, Choice2Block->ColorAndOpacity.GetSpecifiedColor().G, Choice2Block->ColorAndOpacity.GetSpecifiedColor().B, 0.0f);

			Choice1Block->SetColorAndOpacity(FSlateColor(color1));
			Choice2Block->SetColorAndOpacity(FSlateColor(color2));
		}
	}
	else if (_bCardReleased)
	{
		GetPANPlayerController()->VisualizeCardChoice(0);

		UCanvasPanelSlot* canvasSlot = Cast<UCanvasPanelSlot>(CardWidget->Slot);
		FVector2D currentCardPosition = canvasSlot->GetPosition();

		currentCardPosition = FMath::Vector2DInterpTo(currentCardPosition, StartCardPostition, GetWorld()->GetDeltaSeconds(), 10.0f);
		canvasSlot->SetPosition(currentCardPosition);
		
		float currentCardAngle = CardWidget->RenderTransform.Angle;
		currentCardAngle = FMath::FInterpTo(currentCardAngle, 0, GetWorld()->GetDeltaSeconds(), 10.0f);
		CardWidget->SetRenderAngle(currentCardAngle);

		FLinearColor color1 = FLinearColor(Choice1Block->ColorAndOpacity.GetSpecifiedColor().R, Choice1Block->ColorAndOpacity.GetSpecifiedColor().G, Choice1Block->ColorAndOpacity.GetSpecifiedColor().B, 0.0f);
		FLinearColor color2 = FLinearColor(Choice2Block->ColorAndOpacity.GetSpecifiedColor().R, Choice2Block->ColorAndOpacity.GetSpecifiedColor().G, Choice2Block->ColorAndOpacity.GetSpecifiedColor().B, 0.0f);

		Choice1Block->SetColorAndOpacity(FSlateColor(color1));
		Choice2Block->SetColorAndOpacity(FSlateColor(color2));
	}
}

void UPANCardTableWidget::VisualizeOnShow(const FText& _cardDescription, const FText& _leftChoiceText, const FText& _rightChoiceText)
{
	/*
	<color=red>Test!</color> Just <color=black>testing</color> a new feature. <color=green>Coloring</color>text with <color=lightbackground>simple</color> xml like tags.
	*/

	CardDescriptionWidget->SetText(_cardDescription);

	Choice2Block->SetText(_leftChoiceText);
	Choice1Block->SetText(_rightChoiceText);
}
