// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UI/Widgets/SPABaseWidget.h"
#include "SPADamageBarWidget.generated.h"

/**
 * 
 */
UCLASS()
class SPINNERARENA_API USPADamageBarWidget : public USPABaseWidget
{
	GENERATED_BODY()
	
public:
	USPADamageBarWidget(const FObjectInitializer& _oi);

	UFUNCTION(BlueprintCallable)
	void RegisterWidgets(class UTextBlock* _damageTextBlock);

	void InitWidget(const FVector& _position, const FVector& _direction, float _power, const FLinearColor& _color);
	void UpdateWidget(float _deltaTime);

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

protected:
	class UTextBlock* DamageTextBlock;

	FVector CurrentPosition;

	FVector Position;
	FVector Direction;
	float Power;

	FLinearColor Color;

	//animation
	float DeltaAcc;
	float MaxTime;
};
