// Fill out your copyright notice in the Description page of Project Settings.

#include "Pandemia.h"
#include "PANBaseCard.h"

#include "Gameplay/Cards/Choices/PANOkChoice.h"
#include "Gameplay/Cards/Choices/PANChangeLeaderChoice.h"
#include "Gameplay/Cards/Choices/PANAddResourcesChoice.h"
#include "UI/PANHUD.h"


UPANBaseCard::UPANBaseCard(const FObjectInitializer& _oi) : Super(_oi)
{

}

void UPANBaseCard::InitCardData(const FPANCardData& _cardData)
{
	CardData = _cardData;
	OriginalCardData = CardData;
}

void UPANBaseCard::InitFromGameData(const FPANGameData& _data)
{
	LocalGameData = _data;
	ModifiedGameData = LocalGameData;
	
	ClearChoices();

	for (int32 i = 0; i < CardData.ChoiceRightActions.Num(); i++)
	{
		EPANChoiceAction choiceAction = CardData.ChoiceRightActions[i];
		UPANBaseChoice* choice = CreateChoiceFromAction(GetPANGameMode()->GetWorld(), choiceAction);
		choice->Init(LocalGameData);

		if (CardData.ChoiceRightText.IsEmpty())
			CardData.ChoiceRightText = choice->GetCustomChoiceText();

		RightChoices.Add(choice);
	}

	for (int32 i = 0; i < CardData.ChoiceLeftActions.Num(); i++)
	{
		EPANChoiceAction choiceAction = CardData.ChoiceLeftActions[i];
		UPANBaseChoice* choice = CreateChoiceFromAction(GetPANGameMode()->GetWorld(), choiceAction);
		choice->Init(LocalGameData);

		if (CardData.ChoiceLeftText.IsEmpty())
			CardData.ChoiceLeftText = choice->GetCustomChoiceText();

		LeftChoices.Add(choice);
	}
}

void UPANBaseCard::VisualizeOnShow()
{
	GetPANHUD()->VisualizeOnShowCardTable(CardData.Description, CardData.ChoiceLeftText, CardData.ChoiceRightText);
}

const FPANGameData& UPANBaseCard::VisualizeRight()
{
	ModifiedGameData = LocalGameData;
	for (int32 i = 0; i < RightChoices.Num(); i++)
	{
		ModifiedGameData = RightChoices[i]->Visualize(ModifiedGameData);
	}
	return ModifiedGameData;
}

const FPANGameData& UPANBaseCard::VisualizeLeft()
{
	ModifiedGameData = LocalGameData;
	for (int32 i = 0; i < LeftChoices.Num(); i++)
	{
		ModifiedGameData = LeftChoices[i]->Visualize(ModifiedGameData);
	}
	return ModifiedGameData;
}

const FPANGameData& UPANBaseCard::ExecuteRight()
{
	ModifiedGameData = LocalGameData;
	for (int32 i = 0; i < RightChoices.Num(); i++)
	{
		ModifiedGameData = RightChoices[i]->Execute(ModifiedGameData);
	}
	return ModifiedGameData;
}

const FPANGameData& UPANBaseCard::ExecuteLeft()
{
	ModifiedGameData = LocalGameData;
	for (int32 i = 0; i < LeftChoices.Num(); i++)
	{
		ModifiedGameData = LeftChoices[i]->Execute(ModifiedGameData);
	}
	return ModifiedGameData;
}

bool UPANBaseCard::IsMakingTurn()
{
	return CardData.bTurn;
}

void UPANBaseCard::ClearChoices()
{
	for (int32 i = 0; i < RightChoices.Num(); i++)
	{
		RightChoices[i]->RemoveFromRoot();
	}
	RightChoices.Reset();
	for (int32 i = 0; i < LeftChoices.Num(); i++)
	{
		LeftChoices[i]->RemoveFromRoot();
	}
	LeftChoices.Reset();
}

void UPANBaseCard::Clear()
{
	ClearChoices();
	this->RemoveFromRoot();
}

UPANBaseChoice* UPANBaseCard::CreateChoiceFromAction(UWorld* _world, EPANChoiceAction _choiceAction)
{
	UPANBaseChoice* choice = NULL;

	switch (_choiceAction)
	{
	case EPANChoiceAction::NoAction:
	{
		choice = NewObject<UPANOkChoice>(_world);
		break;
	}
	case EPANChoiceAction::FinanceAdd:
	case EPANChoiceAction::FinanceSub:
	case EPANChoiceAction::MoodAdd:
	case EPANChoiceAction::MoodSub:
	case EPANChoiceAction::ResearchAdd:
	case EPANChoiceAction::ResearchSub:
	{
		UPANAddResourcesChoice* addResourcesChoice = NewObject<UPANAddResourcesChoice>(_world);
		addResourcesChoice->InitAction(_choiceAction);
		choice = addResourcesChoice;
		break;
	}
	case EPANChoiceAction::StateOfEmergencyAdd:
	case EPANChoiceAction::StateOfEmergencySub:
	case EPANChoiceAction::BordersClose:
	case EPANChoiceAction::BordersOpen:
	{
		UPANChangeCountryStatusChoice* changeCountryStatusChoice = NewObject<UPANChangeCountryStatusChoice>(_world);
		//changeCountryStatusChoice->InitAction(_choiceAction);
		choice = changeCountryStatusChoice;

		break;
	}
	case EPANChoiceAction::DeliverVaccine:
	{
		break;
	}
	case EPANChoiceAction::ResearchVirus:
	{
		break;
	}
	case EPANChoiceAction::ChangeLeader:
	{
		choice = NewObject<UPANChangeLeaderChoice>(_world);
		break;
	}
	/*case EPANChoiceAction::StartQuest:
	{
		break;
	}*/
	default:
	{
		choice = NewObject<UPANOkChoice>(_world);
		break;
	}
	}

	choice->AddToRoot();

	return choice;
}
