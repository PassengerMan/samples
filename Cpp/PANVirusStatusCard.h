// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Gameplay/PANBaseObject.h"
#include "PANBaseCard.h"
#include "PANVirusStatusCard.generated.h"

/**
 * 
 */
UCLASS()
class PANDEMIA_API UPANVirusStatusCard : public UPANBaseCard
{
	GENERATED_BODY()
	
public:
	UPANVirusStatusCard(const FObjectInitializer& _oi);

	virtual void InitFromGameData(const FPANGameData& _data) override;
protected:

};
