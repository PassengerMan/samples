// Fill out your copyright notice in the Description page of Project Settings.

#include "Pandemia.h"
#include "PANVirusStatusCard.h"
#include "UI/PANHUD.h"


UPANVirusStatusCard::UPANVirusStatusCard(const FObjectInitializer& _oi) : Super(_oi)
{

}

void UPANVirusStatusCard::InitFromGameData(const FPANGameData& _data)
{
	Super::InitFromGameData(_data);

	/*
	<color=red>Test!</color> Just <color=black>testing</color> a new feature. <color=green>Coloring</color>text with <color=lightbackground>simple</color> xml like tags.
	*/

	FString infectedContinents = "";
	for (uint8 i = 0; i < (uint8)EPANContinentType::MAX; i++)
	{
		bool bIsContinentInfected = _data.ContinentsGameplayData.Continents[i].InfectionPercent > 0;
		if (bIsContinentInfected)
		{
			FText& continentName = GetPANGameSettings()->ContinentsUIData[i].ContinentName;

			infectedContinents.Append("<color=red>");
			infectedContinents.Append(continentName.ToString());
			infectedContinents.Append("</color>,");
		}
	}

	infectedContinents.RemoveAt(infectedContinents.Len() - 1); //remove last ,

	FString virusTransmission = "";
	for (int32 i = 0; i < _data.VirusGameplayData.TransmissionTypes.Num(); i++)
	{
		FText& transmissionName = GetPANGameSettings()->VirusUIData.TransmissionNames[(uint8)_data.VirusGameplayData.TransmissionTypes[i]];
		virusTransmission.Append("<color=red>");
		virusTransmission.Append(transmissionName.ToString());
		virusTransmission.Append("</color>,");
	}

	virusTransmission.RemoveAt(virusTransmission.Len() - 1); //remove last ,

	if (virusTransmission.Len() <= 0)
		virusTransmission = "Unknown";
	
	FFormatOrderedArguments orderedArgs;
	orderedArgs.Add(FText::FromString(infectedContinents));
	orderedArgs.Add(FText::FromString(virusTransmission));

	CardData.Description = FText::Format(OriginalCardData.Description, orderedArgs);
}
