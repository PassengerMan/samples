// Fill out your copyright notice in the Description page of Project Settings.

#include "Pandemia.h"
#include "PandemiaGameModeBase.h"

APandemiaGameModeBase::APandemiaGameModeBase(const FObjectInitializer& _oi) : Super(_oi)
{
	PrimaryActorTick.bCanEverTick = true;

	DeltaAcc = 0.0f;
	Delay = 1.0f;
}

void APandemiaGameModeBase::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	GameData = UPANGameplayHelpers::GetGameSettings(GetWorld())->GameData;
	ModifiedGameData = GameData;

	DeckManager.Init(this);
}

void APandemiaGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	DeckManager.TakeCard();
}

void APandemiaGameModeBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	DeckManager.Clear();
}

void APandemiaGameModeBase::TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{
	Super::TickActor(DeltaTime, TickType, ThisTickFunction);

	DeltaAcc += DeltaTime;
	if (DeltaAcc >= Delay)
	{
		NextTurn(true);
		DeltaAcc = 0.0f;
	}
}

const FPANGameData& APandemiaGameModeBase::GetGameData()
{
	return GameData;
}

FPANGameData& APandemiaGameModeBase::GetModifiedGameData()
{
	return ModifiedGameData;
}

const FPANDeckManager& APandemiaGameModeBase::GetDeckManager()
{
	return DeckManager;
}

void APandemiaGameModeBase::RestoreModifiedGameData()
{
	ModifiedGameData = GameData;
}

void APandemiaGameModeBase::NextTurn(bool bTurn)
{
	GameData = ModifiedGameData; //Apply changes

	if(bTurn)
		GameData.NextTurn();
}

void APandemiaGameModeBase::VisualizeCardChoice(int32 _direction)
{
	//get current card
	//card->Visualize
	DeckManager.VisualizeCard(_direction);
}

void APandemiaGameModeBase::ExecuteCardChoice(int32 _direction)
{
	//get current card
	//card->Execute
	DeckManager.ExecuteCard(_direction);
	DeckManager.TakeCard();
}
