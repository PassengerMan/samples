// Fill out your copyright notice in the Description page of Project Settings.

#include "Pandemia.h"
#include "PANResourceWidget.h"
#include "Runtime/UMG/Public/Components/Image.h"


UPANResourceWidget::UPANResourceWidget(const FObjectInitializer& _oi) : Super(_oi)
{

}

void UPANResourceWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UPANResourceWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UPANResourceWidget::RegisterWidgets(UImage* _resourceIcon, UImage* _upState, UImage* _downState)
{
	ResourceIcon = _resourceIcon;
	UpState = _upState;
	DownState = _downState;

	UpState->SetVisibility(ESlateVisibility::Hidden);
	DownState->SetVisibility(ESlateVisibility::Hidden);

	ResourceMaterial = ResourceIcon->GetDynamicMaterial();
	FPANResourceUIData resourceUIData;
	if (GetResourceUIData(resourceUIData))
	{
		ResourceMaterial->SetTextureParameterValue("Texture", resourceUIData.ResourceIcon);
	}
	ResourceMaterial->SetVectorParameterValue("FontColor", GetColor(EPANColor::Background).GetSpecifiedColor());
	ResourceMaterial->SetVectorParameterValue("FontBlendColor", GetColor(EPANColor::White).GetSpecifiedColor());
}

void UPANResourceWidget::Update(const FPANResourceGameplayData& _resourceGameplayData)
{
	if (ResourceIcon == NULL || UpState == NULL || DownState == NULL || ResourceMaterial == NULL)
		return;

	const FPANGameData& gameData = GetPANGameMode()->GetGameData();
	float originalResourceValue = gameData.ResourcesGameplayData.Resources[(uint8)ResourceType].Percent;
	float modifiedResourceValue = _resourceGameplayData.Percent;

	if (modifiedResourceValue < originalResourceValue)
	{
		UpState->SetVisibility(ESlateVisibility::Hidden);
		DownState->SetVisibility(ESlateVisibility::Visible);
	}
	else if(modifiedResourceValue > originalResourceValue)
	{
		UpState->SetVisibility(ESlateVisibility::Visible);
		DownState->SetVisibility(ESlateVisibility::Hidden);
	}
	else
	{
		UpState->SetVisibility(ESlateVisibility::Hidden);
		DownState->SetVisibility(ESlateVisibility::Hidden);
	}

	ResourceMaterial->SetScalarParameterValue("BlendAlpha", modifiedResourceValue);
}

bool UPANResourceWidget::GetResourceUIData(FPANResourceUIData& _resourceUIData)
{
	if (ResourceType == EPANResourceType::MAX)
		return false;

	UPANGameSettings* gameSettings = GetPANGameSettings();
	if (gameSettings)
	{
		_resourceUIData = gameSettings->ResourcesUIData[(uint8)ResourceType];
		return true; 
	}
	return false;
}