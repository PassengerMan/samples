// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Gameplay/PANBaseObject.h"
#include "Gameplay/Cards/Choices/PANBaseChoice.h"
#include "PANBaseCard.generated.h"

/**
 * 
 */
UCLASS()
class PANDEMIA_API UPANBaseCard : public UPANBaseObject
{
	GENERATED_BODY()
	
public:
	UPANBaseCard(const FObjectInitializer& _oi);

	virtual void InitCardData(const FPANCardData& _cardData);

	virtual void InitFromGameData(const FPANGameData& _data);
	virtual void VisualizeOnShow();

	virtual const FPANGameData& VisualizeRight();
	virtual const FPANGameData& VisualizeLeft();

	virtual const FPANGameData& ExecuteRight();
	virtual const FPANGameData& ExecuteLeft();

	bool IsMakingTurn();

	void ClearChoices();
	virtual void Clear();

	const FPANCardData& GetCardData() { return OriginalCardData; }
protected:
	static UPANBaseChoice* CreateChoiceFromAction(class UWorld* _world, EPANChoiceAction _choiceAction);

	FPANGameData LocalGameData;
	FPANGameData ModifiedGameData;
	FPANCardData CardData;
	FPANCardData OriginalCardData;

	TArray<UPANBaseChoice*> RightChoices;
	TArray<UPANBaseChoice*> LeftChoices;
};
