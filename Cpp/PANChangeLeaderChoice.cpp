// Fill out your copyright notice in the Description page of Project Settings.

#include "Pandemia.h"
#include "PANChangeLeaderChoice.h"

UPANChangeLeaderChoice::UPANChangeLeaderChoice(const FObjectInitializer& _oi) : Super(_oi)
{

}

void UPANChangeLeaderChoice::Init(const FPANGameData& _data)
{
	Super::Init(_data);

	FPANResourceGameplayData multipliers[EPANResourceType::MAX];
	for (uint8 i = 0; i < (uint8)EPANResourceType::MAX; i++)
	{
		multipliers[i].SetResource(FMath::RandRange(-30, 30) / 100.0f);
		LocalGameData.LeaderGameplayData.ResourcesMultiplier.SetResource(multipliers[i], (EPANResourceType)i);
	}

	FPANLeadersUIData& leadersUIData = GetPANGameSettings()->LeadersUIData;

	int32 leaderImageIndex = -1;
	int32 numberOfLeaderImages = leadersUIData.LeaderImages.Num();
	if(numberOfLeaderImages > 0)
		leaderImageIndex = FMath::RandRange(0, numberOfLeaderImages-1);

	int32 leaderNameIndex = -1;
	int32 numberOfLeaderNames = leadersUIData.LeaderNames.Num();
	if (numberOfLeaderNames > 0)
		leaderNameIndex = FMath::RandRange(0, numberOfLeaderNames-1);

	LocalGameData.LeaderGameplayData.LeaderImage = leaderImageIndex != -1 ? leadersUIData.LeaderImages[leaderImageIndex] : NULL;
	LocalGameData.LeaderGameplayData.LeaderName = leaderNameIndex != -1 ? leadersUIData.LeaderNames[leaderNameIndex] : FText::FromString("Empty");
}

FPANGameData& UPANChangeLeaderChoice::Visualize(FPANGameData& _data)
{
	_data = Super::Visualize(_data);
	_data.LeaderGameplayData = LocalGameData.LeaderGameplayData;
	return _data;
}

FPANGameData& UPANChangeLeaderChoice::Execute(FPANGameData& _data)
{
	_data = Super::Execute(_data);
	_data.LeaderGameplayData = LocalGameData.LeaderGameplayData;
	return _data;
}

const FText& UPANChangeLeaderChoice::GetCustomChoiceText()
{
	return LocalGameData.LeaderGameplayData.LeaderName;
}