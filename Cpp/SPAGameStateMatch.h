// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SPAGameStateBase.h"

class FSPAGameStateMatch : public FSPAGameStateBase
{

public:
	FSPAGameStateMatch() {};

	virtual void InitBase() override;
	virtual void Enter() override;
	void InitMatch(const FSPAEnemyData& _enemyData, const FSPASpinnerData& _playerSpinnerData);

	virtual void Exit() override;
	virtual void Update(float _deltaTime) override;

protected:
	void ChangeMatchStateWithDelay(ESPAMatchState _nextMatchState, float _delay);
	bool UpdateMatchStateWithDelay(float _deltaTime);

	void OnMatchStateChanged(ESPAMatchState _currentMatchState);
	void OnSpinnerStopped(ASPASpinnerActor* _spinnerActor);
	void OnSpinnerHit(ASPASpinnerActor* _spinnerActor, const FVector& _dir, float _power);

	FSPAEnemyData EnemyData;
	FSPASpinnerData PlayerSpinnerData;

	class ASPAArenaActor* Arena;

	ASPASpinnerActor* PlayerSpinner;
	TArray<class ASPASpinnerActor*> EnemySpinners;

private:
	bool bStartedSwiping;
	FVector SwipeStartPosition;
	FVector SwipeEndPosition;

	//
	bool bStartedDashing;

	//
	FSPAMatchGameplayData MatchGameplayData;

	ESPAMatchState NextMatchState;
	float MatchStateDelay;
	float MatchStateDelaySumDelta;

	//
	bool bSpinnerStopped;

	//UI
	class USPAGameWidget* GameWidget;
};

