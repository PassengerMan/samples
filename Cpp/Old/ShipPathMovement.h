// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/FloatingPawnMovement.h"
#include "ShipPathMovement.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMovementFinishedSignature, int32, path_index);

UCLASS()
class TREASUREHUNT_API UShipPathMovement : public UFloatingPawnMovement
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditDefaultsOnly)
	float RotationSpeed;

public:
	UShipPathMovement(const FObjectInitializer& OI);

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	virtual void InitializeComponent() override;

	virtual bool AddInput(const FVector& DeltaLocation, const FRotator& NewRotation, float distance, bool bRotateOnly, bool bForce = false);
	
	virtual void Move(const TArray<FVector>& PathArray);
	virtual void CancelMovement();
	int32 GetCurrentPathIndex();
	void GetPathArray(TArray<FVector>& OutPath);

	bool IsFinishedMoving();

	UPROPERTY(BlueprintAssignable, Category = ShipPathMovementComponent)
	FOnMovementFinishedSignature OnMovementFinished;

protected:
	UPROPERTY()
	TArray<FVector> Path;
	int32 CurrentPathIndex;

	bool bMove;
	bool bMoveFinished;
	bool bAllMovesFinished;
	bool bMoveToBeCanceled;
	bool bCancelMovement;

	FVector MoveDestination;

	//bool bIsRotating;
	//bool bIsMoving;

	virtual void StepMoveTo(float DeltaTime);
	virtual bool MoveTo(const FVector& WorldLocation);
};
