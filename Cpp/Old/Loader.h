#pragma once
#include "Loader.generated.h"

UCLASS()
class TREASUREHUNT_API ULoader : public UObject
{
	GENERATED_BODY()
public:
	ULoader(const FObjectInitializer& OI);

	template<typename T>
	T* LoadAsset(const FString& AssetPath);

	template<typename T>
	T* FindAndLoadAsset(const FString& AssetName);

private:
};

template<typename T>
T* ULoader::LoadAsset(const FString& AssetPath)
{
	return Cast<T>(StaticLoadObject(T::StaticClass(), NULL, *AssetPath));
}

template<typename T>
T* ULoader::FindAndLoadAsset(const FString& AssetName)
{
	return Cast<T>(FindObject<T>(nullptr, *AssetName));
}
