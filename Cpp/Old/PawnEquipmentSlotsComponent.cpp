#include "TreasureHunt.h"
#include "PawnEquipmentSlotsComponent.h"


UPawnEquipmentSlotsComponent::UPawnEquipmentSlotsComponent(const FObjectInitializer& PCIP)
{
	this->ComponentTags.Add(FName("Save"));

	WeaponSlotsSocketMapping.Reset();

	WeaponSlotsSocketMapping.Add(FString("WeaponSocket0"), (uint8)EEquipmentSlot::WeaponSlot0);
	WeaponSlotsSocketMapping.Add(FString("WeaponSocket1"), (uint8)EEquipmentSlot::WeaponSlot1);
	WeaponSlotsSocketMapping.Add(FString("WeaponSocket2"), (uint8)EEquipmentSlot::WeaponSlot2);
	WeaponSlotsSocketMapping.Add(FString("WeaponSocket3"), (uint8)EEquipmentSlot::WeaponSlot3);
	WeaponSlotsSocketMapping.Add(FString("WeaponSocket4"), (uint8)EEquipmentSlot::WeaponSlot4);
	WeaponSlotsSocketMapping.Add(FString("WeaponSocket5"), (uint8)EEquipmentSlot::WeaponSlot5);
	WeaponSlotsSocketMapping.Add(FString("WeaponSocket6"), (uint8)EEquipmentSlot::WeaponSlot6);
	WeaponSlotsSocketMapping.Add(FString("WeaponSocket7"), (uint8)EEquipmentSlot::WeaponSlot7);
	WeaponSlotsSocketMapping.Add(FString("WeaponSocket8"), (uint8)EEquipmentSlot::WeaponSlot8);
	WeaponSlotsSocketMapping.Add(FString("WeaponSocket9"), (uint8)EEquipmentSlot::WeaponSlot9);
	WeaponSlotsSocketMapping.Add(FString("WeaponSocket10"), (uint8)EEquipmentSlot::WeaponSlot10);
	WeaponSlotsSocketMapping.Add(FString("WeaponSocket11"), (uint8)EEquipmentSlot::WeaponSlot11);

}

AWeaponPickup* UPawnEquipmentSlotsComponent::GetWeapon(const FString& slot_name)
{
	if (WeaponSlotsSocketMapping.Find(slot_name))
	{
		uint8 slot_type = WeaponSlotsSocketMapping[slot_name];

		if (EquipmentSlots.Find(slot_type))
		{
			return (AWeaponPickup*)EquipmentSlots[slot_type].SlotContent;
		}
	}
	return nullptr;
}

void UPawnEquipmentSlotsComponent::InitSlots(AGamePawn* GamePawn)
{
	PawnOwner = GamePawn;

	EquipmentSlots.Reset();
	if (DefaultSlots.Num() > 0)
	{
		for (int32 i = 0; i < DefaultSlots.Num(); i++)
		{
			EquipmentSlots.Add((uint8)DefaultSlots[i].SlotType, DefaultSlots[i]);
		}
	}

	TArray<FName> SocketNames = PawnOwner->MeshComponent->GetAllSocketNames();
	for (int32 i = 0; i < SocketNames.Num(); i++)
	{
		FString SocketString;
		SocketNames[i].ToString(SocketString);
		if (SocketString.Contains(FString("WeaponSocket")))
		{
			uint8 slot_type = WeaponSlotsSocketMapping[SocketString];
			EquipmentSlots.Add(slot_type, FEquipmentSlot((EEquipmentSlot)slot_type));
		}
	}
}

void UPawnEquipmentSlotsComponent::GetAvailableWeaponSlots(TArray<uint8>& OutWeaponSlots)
{
	OutWeaponSlots.Reset();
	for (auto SlotsIter = EquipmentSlots.CreateIterator(); SlotsIter; ++SlotsIter)
	{
		if (SlotsIter->Key > ((uint8)EEquipmentSlot::WeaponSlot0) - 1 && SlotsIter->Key < ((uint8)EEquipmentSlot::WeaponSlot11) + 1)
		{
			OutWeaponSlots.Add(SlotsIter->Key);
		}
	}
}

void UPawnEquipmentSlotsComponent::GetAvailableOtherSlots(TArray<uint8>& OutOtherSlots)
{
	OutOtherSlots.Reset();
	for (auto SlotsIter = EquipmentSlots.CreateIterator(); SlotsIter; ++SlotsIter)
	{
		//UE_LOG(DebugLog, Warning, TEXT("Iter: %d, low: %d, high: %d"), SlotsIter->Key, (uint8)EEquipmentSlot::WeaponSlot0, (uint8)EEquipmentSlot::WeaponSlot11);

		if (SlotsIter->Key < (uint8)EEquipmentSlot::WeaponSlot0 || SlotsIter->Key > (uint8)EEquipmentSlot::WeaponSlot11)
		{
			OutOtherSlots.Add(SlotsIter->Key);
		}
	}
}

void UPawnEquipmentSlotsComponent::Equip(uint8 slot_type, APickup* pickup)
{
	if (EquipmentSlots.Find(slot_type))
	{
		if (EquipmentSlots[slot_type].SlotContent)
		{
			APickup* equipped_pickup = EquipmentSlots[slot_type].SlotContent;
			if (equipped_pickup)
			{
				equipped_pickup->GetEquippableComponent()->SetIsEquipped(false);
				equipped_pickup->GetEquippableComponent()->SetEquipmentSlot((uint8)EEquipmentSlot::None);
				equipped_pickup->Hide(true);
			}
		}
		EquipmentSlots[slot_type].SlotContent = pickup;
		pickup->GetEquippableComponent()->SetEquipmentSlot(slot_type);
		pickup->GetEquippableComponent()->SetIsEquipped(true);
	}

	if (pickup->getType() == EPickupType::Weapon)
	{
		const FString* slot_name = WeaponSlotsSocketMapping.FindKey(slot_type);
		AGamePawn* PawnOwner = Cast<AGamePawn>(GetOwner());
		if (PawnOwner && slot_name)
		{
			const USkeletalMeshSocket* socket = PawnOwner->MeshComponent->GetSocketByName(FName(*(*slot_name)));
			pickup->Hide(false);
			socket->AttachActor(pickup, PawnOwner->MeshComponent);
		}
	}
}

void UPawnEquipmentSlotsComponent::UnequipFromSlot(uint8 slot_type)
{
	if (EquipmentSlots.Find(slot_type))
	{
		APickup* equipped_pickup = EquipmentSlots[slot_type].SlotContent;

		if (equipped_pickup)
		{
			equipped_pickup->GetEquippableComponent()->SetIsEquipped(false);
			equipped_pickup->GetEquippableComponent()->SetEquipmentSlot((uint8)EEquipmentSlot::None);
			equipped_pickup->Hide(true);
		}
		EquipmentSlots[slot_type].SlotContent = nullptr;
	}
}

void UPawnEquipmentSlotsComponent::Unequip(APickup* pickup)
{
	if (pickup)
	{
		uint8 slot_type = pickup->GetEquippableComponent()->GetEquipmentSlot();

		if (EquipmentSlots.Find(slot_type))
		{
			if (EquipmentSlots[slot_type].SlotContent)
			{
				APickup* equipped_pickup = EquipmentSlots[slot_type].SlotContent;

				if (equipped_pickup && pickup)
				{
					if (equipped_pickup == pickup)
					{
						equipped_pickup->GetEquippableComponent()->SetIsEquipped(false);
						equipped_pickup->GetEquippableComponent()->SetEquipmentSlot((uint8)EEquipmentSlot::None);
						equipped_pickup->Hide(true);
					}
				}
				EquipmentSlots[slot_type].SlotContent = nullptr;
			}
		}
	}

}



