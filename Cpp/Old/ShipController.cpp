// Fill out your copyright notice in the Description page of Project Settings.

#include "TreasureHunt.h"
#include "ShipController.h"



AShipController::AShipController(const class FObjectInitializer& OI)
	: Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	bEnableMouseOverEvents = true;
	bEnableTouchOverEvents = true;

	PlayerShipState = EPlayerState::Exploring;
}

void AShipController::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	UE_LOG(DebugLog, Warning, TEXT("PostInitializeComponents"));
}

void AShipController::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(DebugLog, Warning, TEXT("BeginPlay"));

	SplineTilePath = GetWorld()->SpawnActor<ASplinePath>(BP_SplinePath->GeneratedClass);
	CameraDirector = GetWorld()->SpawnActor<ACameraDirector>(ACameraDirector::StaticClass());

	WorldMapUIWidget =
		GetWorld()->SpawnActor<AWorldMapUIWrapper>(
		AWorldMapUIWrapper::StaticClass());

	WorldMapUIWidget->Init(this);
	WorldMapUIWidget->Show(true);

	InventoryWidget = 
		GetWorld()->SpawnActor<AInventoryWidgetWrapper>(
		AInventoryWidgetWrapper::StaticClass());
}

void AShipController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	WorldMapUIWidget->Show(false);
}

void AShipController::Possess(APawn* aPawn)
{
	Super::Possess(aPawn);

	UE_LOG(DebugLog, Warning, TEXT("Possess"));

	ShipPawn = Cast<AShipPawn>(GetPawn());

	if (ShipPawn && ShipPawn->MovementComponent)
	{
		ShipPawn->MovementComponent->OnMovementFinished.AddDynamic(this, &AShipController::OnMovementFinished);
	}
}

void AShipController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	if (this->IsPaused())
		return;

	CommandStream.Update();

	if (PlayerShipState == EPlayerState::Building)
	{
	}

	if (PlayerShipState == EPlayerState::Exploring)
	{

	}

	if (PlayerShipState == EPlayerState::Fighting)
	{

	}
}

void AShipController::ProcessPlayerInput(const float DeltaTime, const bool bGamePaused)
{
	Super::ProcessPlayerInput(DeltaTime, bGamePaused);
}

void AShipController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AShipController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &AShipController::OnSetDestinationReleased);

	InputComponent->BindAction("ShowInventory", IE_Pressed, this, &AShipController::OnShowInventory);
}

void AShipController::OnSetDestinationPressed()
{
	UE_LOG(DebugLog, Warning, TEXT("OnSetDestinationPressed"));

	if (ShipPawn->IsFinishedMoving() == false)
		return;

	float mouse_x, mouse_y;
	GetMousePosition(mouse_x, mouse_y);
	MouseLocation.Set(mouse_x, mouse_y);

	FVector mouse_world_direction;

	bool deproject_successfull = this->DeprojectScreenPositionToWorld(
		MouseLocation.X, MouseLocation.Y, MouseWorldLocation, mouse_world_direction);

	MouseWorldLocation = FMath::LinePlaneIntersection(
		MouseWorldLocation, MouseWorldLocation + (mouse_world_direction*500.0f), FPlane(FVector(-100, 0, 0), FVector(0, 0, 1)));

	FHitResult HitResult;

	FCollisionQueryParams TraceParams(FName(TEXT("HexTileComponentTrace")));
	TraceParams.bTraceComplex = true;

	GetWorld()->LineTraceSingleByChannel(
		HitResult,
		MouseWorldLocation + FVector(0,0, -500),
		MouseWorldLocation + FVector(0, 0, 500),
		ECollisionChannel::ECC_Visibility,
		TraceParams);

	AWorldHexGrid* world_hex_grid = Cast<AWorldHexGrid>(HitResult.GetActor());
	UInstancedStaticMeshComponent* tile_component = Cast<UInstancedStaticMeshComponent>(HitResult.GetComponent());
	if (world_hex_grid && tile_component && 
		world_hex_grid->IsValidLowLevel() && tile_component->IsValidLowLevel())
	{
		if (WorldHexGrid == nullptr)
			WorldHexGrid = world_hex_grid;

		EWorldHexTileEnvType hit_tile = 
			(EWorldHexTileEnvType)WorldHexGrid->GetTileTypeFromComponent(tile_component);
		UWorldHexTile* found_tile = 
			Cast<UWorldHexTile>(WorldHexGrid->GetTile(tile_component, HitResult.Item));

		WorldHexGrid->SetPlayerDestinationTile(found_tile);
		
		AWorldLocationBase* world_location_hit = WorldHexGrid->GetWorldLocationActor(found_tile->GetGridPosition());
		WorldMapUIWidget->showContextMenu(true, found_tile->GetPosition(), this, world_location_hit);

		//Camera Movement
		FTransform cam_transform;
		cam_transform.SetIdentity();
		cam_transform = ShipPawn->CameraArm->GetComponentTransform();

		FVector center_pos = (found_tile->GetPosition() - ShipPawn->GetActorLocation()) / 2.0f;
		cam_transform.SetLocation(ShipPawn->GetActorLocation() + center_pos * 1.2f); //world location
	
		CameraDirector->TransferTo(ShipPawn, cam_transform);
	}
}

void AShipController::OnSetDestinationReleased()
{

}

void AShipController::UpdateMouseLocation()
{
	float mouse_x, mouse_y;
	GetMousePosition(mouse_x, mouse_y);
	MouseLocation.Set(mouse_x, mouse_y);

	FVector mouse_world_direction;

	bool deproject_successfull = this->DeprojectScreenPositionToWorld(
		MouseLocation.X, MouseLocation.Y, MouseWorldLocation, mouse_world_direction);

	MouseWorldLocation = FMath::LinePlaneIntersection(
		MouseWorldLocation, MouseWorldLocation+(mouse_world_direction*500.0f), FPlane(FVector(-100, 0, 0), FVector(0, 0, 1)));
}

void AShipController::HandleWorldMapAction(uint8 action_type)
{
	EWorldMapLocationActionType the_action = (EWorldMapLocationActionType)action_type;

	switch (the_action)
	{
		case EWorldMapLocationActionType::ACTION_MOVE:
		{
			CommandStream.Add(new FMoveToCommand(ShipPawn, WorldHexGrid));
			CommandStream.Start();

			if (SplineTilePath)
			{
				SplineTilePath->Reset();

				SplineTilePath->AddPoint(FVector(
					WorldHexGrid->GetPlayerOccupiedTile()->GetPosition().X,
					WorldHexGrid->GetPlayerOccupiedTile()->GetPosition().Y,
					WorldHexGrid->GetPlayerOccupiedTile()->GetPosition().Z + 10));

				TArray<FVector> TilePath;
				ShipPawn->MovementComponent->GetPathArray(TilePath);
				for (int32 i = 0; i < TilePath.Num(); i++)
				{
					SplineTilePath->AddPoint(FVector(
						TilePath[i].X,
						TilePath[i].Y,
						TilePath[i].Z + 10));
				}
				SplineTilePath->Show();
			}
			CameraDirector->TransferBack();
			break;
		}

		case EWorldMapLocationActionType::ACTION_CANCEL_MOVE:
		{
			CommandStream.Clear();

			break;
		}

		case EWorldMapLocationActionType::ACTION_INFO:
		{
			break;
		}

		case EWorldMapLocationActionType::ACTION_TALK:
		{
			break;
		}

		case EWorldMapLocationActionType::ACTION_ENTER_TOWN:
		{
			break;
		}

		case EWorldMapLocationActionType::ACTION_SURVEY:
		{
			break;
		}

		case EWorldMapLocationActionType::ACTION_ENTER_BATTLE:
		{
			break;
		}
	}
}

void AShipController::OnMovementFinished(int32 path_index)
{
	SplineTilePath->Reset();

	if (WorldHexGrid == nullptr)
		return;

	UE_LOG(DebugLog, Warning, TEXT("OnMovementFinished: %d"), path_index);

	if (path_index < 0)
		return;

	//WorldHexGrid->SetPlayerOccupiedTile(TilePath[path_index]);
}

void AShipController::OnShowInventory()
{
	bShowInventory = !bShowInventory;
	if (InventoryWidget)
	{
		InventoryWidget->showInventoryWidget(bShowInventory, ShipPawn, ShipPawn->getInventory());
	}
}

