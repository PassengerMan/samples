#include "Survival.h"
#include "CharacterBonePartitionComponent.h"


UCharacterBonePartitionComponent::UCharacterBonePartitionComponent(const FObjectInitializer& PCIP)
{
	bWantsInitializeComponent = true;
}

void UCharacterBonePartitionComponent::InitializeComponent()
{
	BoneHits.Reset();
	for (int i = 0; i < RemovableHeadBones.Num(); i++)
	{
		FRemovableBoneStruct removable_bone_struct;
		removable_bone_struct.BoneName = RemovableHeadBones[i].BoneName;
		removable_bone_struct.HitCount = 0;
		removable_bone_struct.HitsNeededToRemove = RemovableHeadBones[i].HitsNeededToRemove;

		BoneHits.Add(RemovableHeadBones[i].BoneName, removable_bone_struct);
	}
	for (int i = 0; i < RemovableArmBones.Num(); i++)
	{
		FRemovableBoneStruct removable_bone_struct;
		removable_bone_struct.BoneName = RemovableArmBones[i].BoneName;
		removable_bone_struct.HitCount = 0;
		removable_bone_struct.HitsNeededToRemove = RemovableArmBones[i].HitsNeededToRemove;

		BoneHits.Add(RemovableArmBones[i].BoneName, removable_bone_struct);
	}
	for (int i = 0; i < RemovableLegBones.Num(); i++)
	{
		FRemovableBoneStruct removable_bone_struct;
		removable_bone_struct.BoneName = RemovableLegBones[i].BoneName;
		removable_bone_struct.HitCount = 0;
		removable_bone_struct.HitsNeededToRemove = RemovableLegBones[i].HitsNeededToRemove;

		BoneHits.Add(RemovableLegBones[i].BoneName, removable_bone_struct);
	}

	UE_LOG(LogTemp, Warning, TEXT("CharacterBonePartitionComponent Initialized"));
}

bool UCharacterBonePartitionComponent::RemoveBone(FName& BoneName, USkeletalMeshComponent* SkeletalMesh)
{
	if (SkeletalMesh->IsBoneHiddenByName(BoneName) == false)
	{
		SkeletalMesh->HideBoneByName(BoneName, EPhysBodyOp::PBO_Term);
		return true;
	}
	return false;
}

bool UCharacterBonePartitionComponent::HitBone(FName& BoneName, const FVector& HitLoc, USkeletalMeshComponent* SkeletalMesh)
{
	FString BoneString = BoneName.ToString();
	bool bBoneRemoved = false;

	for (int i = 0; i < RemovableHeadBones.Num(); i++)
	{
		FString RemovableBoneString = RemovableHeadBones[i].BoneName.ToString();

		if (BoneString.Contains(RemovableBoneString, ESearchCase::IgnoreCase, ESearchDir::FromStart))
		{
			BoneHits[RemovableHeadBones[i].BoneName].HitCount++;

			if (BoneHits[RemovableHeadBones[i].BoneName].HitCount >= BoneHits[RemovableHeadBones[i].BoneName].HitsNeededToRemove)
			{
				bBoneRemoved = RemoveBone(BoneName, SkeletalMesh);
				bHeadRemoved = true;
			}
		}
	}

	for (int i = 0; i < RemovableArmBones.Num(); i++)
	{
		FString RemovableBoneString = RemovableArmBones[i].BoneName.ToString();

		if (BoneString.Contains(RemovableBoneString, ESearchCase::IgnoreCase, ESearchDir::FromStart))
		{
			BoneHits[RemovableArmBones[i].BoneName].HitCount++;

			if (BoneHits[RemovableArmBones[i].BoneName].HitCount >= BoneHits[RemovableArmBones[i].BoneName].HitsNeededToRemove)
			{
				bBoneRemoved = RemoveBone(BoneName, SkeletalMesh);
				bArmRemoved = true;
			}
		}
	}

	for (int i = 0; i < RemovableLegBones.Num(); i++)
	{
		FString RemovableBoneString = RemovableLegBones[i].BoneName.ToString();

		if (BoneString.Contains(RemovableBoneString, ESearchCase::IgnoreCase, ESearchDir::FromStart))
		{
			BoneHits[RemovableLegBones[i].BoneName].HitCount++;

			if (BoneHits[RemovableLegBones[i].BoneName].HitCount >= BoneHits[RemovableLegBones[i].BoneName].HitsNeededToRemove)
			{
				bBoneRemoved = RemoveBone(BoneName, SkeletalMesh);
				bLegRemoved = true;
			}
		}
	}

	if (Hit_ParticleSystem)
	{
		UGameplayStatics::SpawnEmitterAtLocation(
			GetWorld(),
			Hit_ParticleSystem,
			HitLoc, 
			FRotator::ZeroRotator);
	}

	if (Hit_BoneRemoved_ParticleSystem)
	{
		if (bBoneRemoved)
		{
			UGameplayStatics::SpawnEmitterAttached(
				Hit_BoneRemoved_ParticleSystem,
				SkeletalMesh,
				NAME_None,
				SkeletalMesh->GetSocketLocation(BoneName),
				FRotator::ZeroRotator,
				EAttachLocation::KeepWorldPosition);
		}
	}

	return bBoneRemoved;
}

