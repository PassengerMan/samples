#pragma once

#include "Globals/GlobalTypes.h"
#include "PawnEquipmentSlotsComponent.generated.h"

USTRUCT()
struct FEquipmentSlot
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, SaveGame, Category = EquipmentSlot)
	EEquipmentSlot SlotType;
	UPROPERTY(EditAnywhere, SaveGame, Category = EquipmentSlot)
	bool bIsLocked;
	UPROPERTY()
	APickup* SlotContent;

	FEquipmentSlot()
	{
		SlotContent = nullptr;
	}

	FEquipmentSlot(EEquipmentSlot InSlotType)
	{
		SlotType = InSlotType;
		SlotContent = nullptr;
	}

	FEquipmentSlot(EEquipmentSlot InSlotType, APickup* InSlotContent)
	{
		SlotType = InSlotType;
		SlotContent = InSlotContent;
	}

	void SetSlotContent(APickup* InSlotContent)
	{
		SlotContent = InSlotContent;
	}
};

UCLASS()
class UPawnEquipmentSlotsComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPawnEquipmentSlotsComponent(const FObjectInitializer& PCIP);

	UPROPERTY()
	AGamePawn* PawnOwner;

	UPROPERTY()
	TMap<uint8, FEquipmentSlot> EquipmentSlots;

	UPROPERTY(EditAnywhere, SaveGame, Category = PawnEquipmentSlots)
	TArray<FEquipmentSlot> DefaultSlots;

	TMap<FString, uint8> WeaponSlotsSocketMapping;

	UFUNCTION(BlueprintCallable, Category = PawnEquipmentSlots)
	void InitSlots(AGamePawn* GamePawn);

	UFUNCTION(BlueprintCallable, Category = PawnEquipmentSlots)
	void Equip(uint8 slot_type, APickup* pickup);

	UFUNCTION(BlueprintCallable, Category = PawnEquipmentSlots)
	void UnequipFromSlot(uint8 slot_type);

	UFUNCTION(BlueprintCallable, Category = PawnEquipmentSlots)
	void Unequip(APickup* pickup);

	UFUNCTION(BlueprintCallable, Category = PawnEquipmentSlots)
	AWeaponPickup* GetWeapon(const FString& slot_name);

	//
	UFUNCTION(BlueprintCallable, Category = PawnEquipmentSlots)
	void GetAvailableWeaponSlots(TArray<uint8>& OutWeaponSlots);

	UFUNCTION(BlueprintCallable, Category = PawnEquipmentSlots)
	void GetAvailableOtherSlots(TArray<uint8>& OutOtherSlots);
};