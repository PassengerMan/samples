#include "TreasureHunt.h"
#include "HexGridBase.h"

AHexGridBase::AHexGridBase(const FObjectInitializer& OI) : Super(OI)
{
	UArrowComponent* ArrowComp = OI.CreateDefaultSubobject<UArrowComponent>(this, TEXT("ArrowComp"));
	ArrowComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	ArrowComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = ArrowComp;
}

void AHexGridBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AHexGridBase::BeginPlay()
{
	Super::BeginPlay();
}

void AHexGridBase::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
}

UHexTileBase* AHexGridBase::GetTile(FIntPoint position)
{
	return tiles[position];
}

UHexTileBase* AHexGridBase::GetTile(UInstancedStaticMeshComponent* component, int32 instance_id)
{
	return tiles[tiles_instance_id_lookup[component][instance_id]];
}

TMap<FIntPoint, UHexTileBase*>& AHexGridBase::GetTiles()
{
	return tiles;
}

void AHexGridBase::Init(int32 size_x, int32 size_y, int32 seed)
{
	grid_size.X = size_x;
	grid_size.Y = size_y;

	grid_seed = seed;

	seeded_stream.Initialize(seed);
}

void AHexGridBase::GenerateGrid()
{
	if (grid_size.X <= 0 || grid_size.Y <= 0)
		return;
	//Generate in parent class
}

void AHexGridBase::GenerateMeshes()
{
	FRandomStream RandomStream;

	for (auto TilesIter = tiles.CreateIterator(); TilesIter; ++TilesIter)
	{
		UHexTileBase* current_tile = TilesIter->Value;
		uint8 current_tile_type = current_tile->GetTileType();

		if (HexMesh)
		{
			UInstancedStaticMeshComponent** InstancedMeshComponentPtr =
				InstancedMeshComponents.Find(current_tile_type);

			UInstancedStaticMeshComponent* InstancedMeshComponent;

			if (InstancedMeshComponentPtr == nullptr)
			{
				InstancedMeshComponent = NewObject<UInstancedStaticMeshComponent>(this);
				ref_hack_array.Add(InstancedMeshComponent);

				InstancedMeshComponent->RegisterComponent();
				InstancedMeshComponent->SetWorldTransform(RootComponent->GetComponentTransform());
				InstancedMeshComponent->SetStaticMesh(HexMesh);

				UMaterialInstanceDynamic* MaterialInstance =
					InstancedMeshComponent->CreateDynamicMaterialInstance(
					0, UGlobals::Get().WorldHexTileInfoDatabase->GetMaterialInstance(current_tile_type));

				InstancedMeshComponent->SetMaterial(0, MaterialInstance);


				InstancedMeshComponents.Add(current_tile_type, InstancedMeshComponent);
				tiles_instance_id_lookup.Add(InstancedMeshComponent, TMap<int32, FIntPoint>());
			}
			else
			{
				InstancedMeshComponent = *InstancedMeshComponentPtr;
			}

			FVector instance_scale;
			instance_scale.Set(1.0f, 1.0f, 1.0f);

			if ((EWorldHexTileEnvType)current_tile_type == EWorldHexTileEnvType::Land_Mountains)
				instance_scale.Set(1.0f, 1.0f, 1.0f + (RandomStream.RandRange(70, 250) / 100.0f));
			if ((EWorldHexTileEnvType)current_tile_type == EWorldHexTileEnvType::Land_Forest)
				instance_scale.Set(1.0f, 1.0f, 1.0f + (RandomStream.RandRange(40, 100) / 100.0f));
			if ((EWorldHexTileEnvType)current_tile_type == EWorldHexTileEnvType::Land_Shore)
				instance_scale.Set(1.0f, 1.0f, 1.0f + (RandomStream.RandRange(20, 70) / 100.0f));

			current_tile->SetScale(instance_scale.X, instance_scale.Y, instance_scale.Z);

			int32 instance_id = InstancedMeshComponent->AddInstance(current_tile->GetTransform());
			tiles_instance_id_lookup[InstancedMeshComponent].Add(instance_id, current_tile->GetGridPosition());
		}

		/*UTextRenderComponent* text_render_component = NewObject<UTextRenderComponent>(this);
		text_render_component->RegisterComponent();
		tile_debug_texts.Add(current_tile->GetGridPosition(), text_render_component);
		ref_hack_array.Add(text_render_component);*/
	}
}

uint8 AHexGridBase::GetTileTypeFromComponent(UInstancedStaticMeshComponent* component)
{
	const uint8* found_tile_type = InstancedMeshComponents.FindKey(component);
	return *found_tile_type;
}

void AHexGridBase::LogContainerSizes()
{
	UE_LOG(DebugLog, Warning, TEXT("UISMC size: %d"), InstancedMeshComponents.Num());
	UE_LOG(DebugLog, Warning, TEXT("Tiles size: %d"), tiles.Num());
	UE_LOG(DebugLog, Warning, TEXT("Tiles_lookup size: %d"), tiles_instance_id_lookup.Num());
}

void AHexGridBase::ClearContainers()
{
	//clear UHexTiles
	for (auto TilesIterator = tiles.CreateIterator(); TilesIterator; ++TilesIterator)
	{
		if (tiles[TilesIterator->Key])
		{
			if (tiles[TilesIterator->Key]->IsValidLowLevel())
				tiles[TilesIterator->Key]->ConditionalBeginDestroy();
		}
	}

	tiles.Empty(0);

	//clear tiles lookup
	for (auto TilesLookupIterator = tiles_instance_id_lookup.CreateIterator(); TilesLookupIterator; ++TilesLookupIterator)
	{
		if (tiles_instance_id_lookup[TilesLookupIterator->Key].Num() > 0)
		{
			tiles_instance_id_lookup[TilesLookupIterator->Key].Empty(0);
		}
	}

	//clear mesh instances
	for (auto IMCIterator = InstancedMeshComponents.CreateIterator(); IMCIterator; ++IMCIterator)
	{
		InstancedMeshComponents[IMCIterator->Key]->ClearInstances();
	}
	tiles_instance_id_lookup.Empty(0);
	InstancedMeshComponents.Empty(0);

	tile_debug_texts.Empty(0);

	for (int32 i = 0; i < ref_hack_array.Num(); i++)
	{
		AActor* the_actor = Cast<AActor>(ref_hack_array[i]);
		if (the_actor && the_actor->IsValidLowLevel())
		{
			the_actor->Destroy();
		}
	}
	ref_hack_array.Empty(0);
}


void AHexGridBase::GetNeighbours(const FIntPoint& TileGridPos, TArray<FIntPoint>& out_tiles)
{
	out_tiles.Reset();
	for (int32 y = -1; y <= 1; y++)
	{
		for (int32 x = -1; x <= 1; x++)
		{
			if (TileGridPos.Y % 2 == 1)
			{
				if (x == -1 && y == -1)
					continue;
				if (x == -1 && y == 1)
					continue;
			}
			else
			{
				if (x == 1 && y == 1)
					continue;
				if (x == 1 && y == -1)
					continue;
			}

			if (x >= grid_size.X || y >= grid_size.Y)
				continue;

			/*if (x == 0 && y == 0)
				continue;*/

			FIntPoint neighbouring_point(TileGridPos.X + x, TileGridPos.Y + y);

			if (neighbouring_point.X < 0 || neighbouring_point.Y < 0 ||
				neighbouring_point.X >= grid_size.X || neighbouring_point.Y >= grid_size.Y)
				continue;

			out_tiles.Add(neighbouring_point);
		}
	}
}

int32 AHexGridBase::TileDistance(const FIntPoint& c1, const FIntPoint& c2)
{
	FIntPoint dist_vec(FMath::Abs(c1.X - c2.X), FMath::Abs(c1.Y - c2.Y));
	return 1 * (dist_vec.X + dist_vec.Y);
}

void AHexGridBase::FindPath(UHexTileBase* StartTile, UHexTileBase* DestinationTile, TArray<UHexTileBase*>& out_tiles, TArray<FVector>& out_path, bool& was_cancelled)
{
	out_tiles.Reset();
	out_path.Reset();

	bool bExit = false;
	int32 max_cost = 100;

	FIntPoint PreviousTileGridPoint = StartTile->GetGridPosition();
	FIntPoint NextTileGridPoint = DestinationTile->GetGridPosition();

	TMap<FIntPoint, FIntPoint> came_from;
	TMap<FIntPoint, int32> cost_so_far;

	TArray<FGridPointPriority> PriorityQueue;
	PriorityQueue.HeapPush(
		FGridPointPriority(PreviousTileGridPoint, 0),
		FGridPointPriorityPredicate());

	came_from.Add(PreviousTileGridPoint, PreviousTileGridPoint);
	cost_so_far.Add(PreviousTileGridPoint, 0);

	while (PriorityQueue.Num() != 0)
	{
		FGridPointPriority current(FIntPoint(-1,-1), -1);
		PriorityQueue.HeapPop(current, FGridPointPriorityPredicate());

		if (current.GridPoint == NextTileGridPoint || bExit == true)
		{
			break;
		}

		TArray<FIntPoint> neighbour_points;
		GetNeighbours(current.GridPoint, neighbour_points);

		for (int32 i = 0; i < neighbour_points.Num(); i++)
		{
			FIntPoint next = neighbour_points[i];
			int32 additional_cost = 1;

			if (tiles.Find(next) != nullptr)
			{
				UHexTileBase* tile = tiles[next];
				if (tile->non_traversable == true)
					additional_cost = max_cost;
			}

			int32 new_cost = cost_so_far.FindOrAdd(current.GridPoint) + additional_cost;

			int32* next_cost_ptr = cost_so_far.Find(next);
			int32 next_cost;
			if (next_cost_ptr == nullptr)
				next_cost = 0;
			else
				next_cost = *next_cost_ptr;

			if (next_cost_ptr == nullptr || new_cost < next_cost)
			{
				cost_so_far.Add(next, new_cost);

				int32 priority = new_cost + TileDistance(next, NextTileGridPoint);
				PriorityQueue.HeapPush(
					FGridPointPriority(next, priority),
					FGridPointPriorityPredicate());

				came_from.Add(next, current.GridPoint);
			}
		}
	}

	TArray<FIntPoint> path;
	FIntPoint current = NextTileGridPoint;

	path.Add(current);
	while (current != PreviousTileGridPoint)
	{
		if (came_from.Find(current) == nullptr)
			break;
		current = came_from[current];
		path.Add(current);
	}

	bool cancel_path = false;
	for (int32 i = path.Num()-1; i >= 0; i--)
	{
		UHexTileBase* tile = tiles[path[i]];

		if (tile->non_traversable == true) //hack for water holes inside islands
		{
			cancel_path = true;
			break;
		}
		//
		out_tiles.Add(tile);

		if (i == 0)
			out_tiles.Add(DestinationTile);
		//
		out_path.Add(tile->GetPosition());

		if (i == 0)
			out_path.Add(DestinationTile->GetPosition());


	}

	//
	if (cancel_path == true && path.Num() <= 0)
		out_tiles.Reset();

	if (out_tiles.Num() > 0)
		out_tiles.RemoveAt(0);
	//
	if (cancel_path == true && path.Num() <= 0)
		out_path.Reset();

	if (out_path.Num() > 0)
		out_path.RemoveAt(0);

	was_cancelled = cancel_path;
}

void AHexGridBase::GetNeighbourOffsets(const FIntPoint& TileGridPos, TArray<FIntPoint>& out_offsets)
{
	out_offsets.Reset();
	if (TileGridPos.Y % 2 == 0)
	{
		out_offsets.Add(FIntPoint(-1, -1));
		out_offsets.Add(FIntPoint(0, -1));
		out_offsets.Add(FIntPoint(1, 0));
		out_offsets.Add(FIntPoint(0, 1));
		out_offsets.Add(FIntPoint(-1, 1));
		out_offsets.Add(FIntPoint(-1, 0));
	}
	else
	{
		out_offsets.Add(FIntPoint(0, -1));
		out_offsets.Add(FIntPoint(1, -1));
		out_offsets.Add(FIntPoint(1, 0));
		out_offsets.Add(FIntPoint(1, 1));
		out_offsets.Add(FIntPoint(0, 1));
		out_offsets.Add(FIntPoint(-1, 0));
	}
}

void AHexGridBase::SetPlayerOccupiedTile(UHexTileBase* tile)
{
	PlayerOccuppiedTile = tile;
}

void AHexGridBase::SetPlayerDestinationTile(UHexTileBase* tile)
{
	PlayerDestinationTile = tile;
}

UHexTileBase* AHexGridBase::GetPlayerOccupiedTile()
{
	return PlayerOccuppiedTile;
}

UHexTileBase* AHexGridBase::GetPlayerDestinationTile()
{
	return PlayerDestinationTile;
}


