// Fill out your copyright notice in the Description page of Project Settings.

#include "TreasureHunt.h"
#include "ShipPathMovement.h"


UShipPathMovement::UShipPathMovement(const FObjectInitializer& OI) : Super(OI)
{
	Acceleration = 200;
	Deceleration = 0;
	MaxSpeed = 500;

	RotationSpeed = 95.5f; //constant

	bMove = false;
	bMoveFinished = true;
	bAllMovesFinished = true;
	bMoveToBeCanceled = false;
	bCancelMovement = false;
}

void UShipPathMovement::InitializeComponent()
{
	Super::InitializeComponent();

	UE_LOG(DebugLog, Warning, TEXT("UShipMovement::InitializeComponent"));
}

bool UShipPathMovement::AddInput(const FVector& DeltaLocation, const FRotator& NewRotation, float distance, bool bRotateOnly, bool bForce /*= false*/)
{
	FRotator component_rotation = UpdatedComponent->GetComponentRotation();
	FRotator delta_rotation = FMath::RInterpConstantTo(component_rotation, NewRotation, GetWorld()->GetDeltaSeconds(), RotationSpeed);

	UpdatedComponent->SetWorldRotation(delta_rotation);

	//UE_LOG(DebugLog, Warning, TEXT("DeltaLocation: %s"), *DeltaLocation.ToCompactString());
	//UE_LOG(DebugLog, Warning, TEXT("Distance: %f"), FMath::Abs(distance));

	if (bRotateOnly == false)
		//AddInputVector(UpdatedComponent->GetComponentRotation().Vector()*Acceleration, bForce);
		Velocity = UpdatedComponent->GetComponentRotation().Vector()*Acceleration;

	if (FMath::Abs(distance) < 20)
	{
		StopActiveMovement();
		StopMovementImmediately();

		if (bMoveToBeCanceled == true)
			bCancelMovement = true;
		//UE_LOG(DebugLog, Warning, TEXT("UShipMovement::MoveFinished"));
		return true;
	}

	return false;
}

void UShipPathMovement::Move(const TArray<FVector>& PathArray)
{
	Path = PathArray;
	CurrentPathIndex = 0;

	bMove = true;
	bMoveFinished = false;
	bAllMovesFinished = false;
	bCancelMovement = false;
	bMoveToBeCanceled = false;
}

void UShipPathMovement::StepMoveTo(float DeltaTime)
{
	if (CurrentPathIndex >= Path.Num() || bCancelMovement == true)
	{
		bAllMovesFinished = true;
		bMoveFinished = true;
		bMove = false;
		bCancelMovement = false;
		bMoveToBeCanceled = false;

		if (OnMovementFinished.IsBound())
			OnMovementFinished.Broadcast(CurrentPathIndex-1);

		return;
	}

	FVector CurrentPathLocation = Path[CurrentPathIndex];
	MoveDestination = CurrentPathLocation;

	bMoveFinished = MoveTo(MoveDestination);
	if (bMoveFinished)
	{
		CurrentPathIndex++;
	}
}

void UShipPathMovement::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bMove)
	{
		StepMoveTo(DeltaTime);
	}
}

bool UShipPathMovement::MoveTo(const FVector& WorldLocation)
{
	AActor* ComponentOwner = this->GetOwner();

	float distance = FVector::Dist(WorldLocation, ComponentOwner->GetActorLocation());

	FVector DeltaLoc = WorldLocation - ComponentOwner->GetActorLocation();
	DeltaLoc.Normalize();

	FRotator Rotation = DeltaLoc.Rotation();

	FVector ship_direction = ComponentOwner->GetActorForwardVector();
	float dot_product = FVector::DotProduct(DeltaLoc, ship_direction);

	bool is_finished_moving = false;

	if (!FMath::IsNearlyEqual(dot_product, 1.0f, 0.05f))
		AddInput(FVector::ZeroVector, Rotation, distance, true);
	else
		is_finished_moving = AddInput(DeltaLoc, Rotation, distance, false);

	return is_finished_moving;
}

void UShipPathMovement::CancelMovement()
{
	bMoveToBeCanceled = true;
}

bool UShipPathMovement::IsFinishedMoving()
{
	return (bMove == false && bMoveFinished == true && bAllMovesFinished == true);
}

int32 UShipPathMovement::GetCurrentPathIndex()
{
	return CurrentPathIndex - 1;
}

void UShipPathMovement::GetPathArray(TArray<FVector>& OutPath)
{
	OutPath = Path;
}


