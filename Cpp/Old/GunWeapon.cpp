#include "Survival.h"
#include "GunWeapon.h"

#include "Character/SurvivalCharacter.h"
#include "Character/CharacterInventory.h"

AGunWeapon::AGunWeapon(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
	setUniqueTypeID(20); //default gun type

	AimStart = PCIP.CreateDefaultSubobject<UArrowComponent>(this, "AimStart");
	AimStart->AttachTo(mesh_component);
}

void AGunWeapon::Attack_Implementation()
{
	if (current_ammo <= 0 && CurrentGunState != EGunState::Reloading)
	{
		EAmmoState reloading_state = BeginReloading();
		if (reloading_state == EAmmoState::NoAmmo)
			return;
	}

	if (CurrentGunState == EGunState::Reloading)
		return;

	CurrentGunState = EGunState::Shooting;
	CurrentAmmoState = EAmmoState::Ok;

	CurrentAmmoPickup->setNumberOnStack(CurrentAmmoPickup->getNumberOnStack() - 1);
	current_ammo -= 1;

	FVector AimStartVec = AimStart->GetComponentLocation();
	FVector AimStartForwardVec = AimStart->GetForwardVector();

	FVector RandomConeDir = FMath::VRandCone(AimStartForwardVec, spread, spread);
	AimStartForwardVec += RandomConeDir;
	AimStartForwardVec *= range;
	
	FVector AimEndVec = AimStartVec + AimStartForwardVec;

	FHitResult HitResult;

	FCollisionQueryParams TraceParams(FName(TEXT("WeaponShootTrace")));
	TraceParams.bTraceComplex = true;
	TraceParams.AddIgnoredActor(this);

	GetWorld()->LineTraceSingleByChannel(
		HitResult,
		AimStartVec,
		AimEndVec,
		ECollisionChannel::ECC_Visibility,
		TraceParams);

	DrawDebugLine(
		GetWorld(),
		AimStartVec,
		AimEndVec,
		FColor(255, 0, 0),
		false,
		-1, 
		0,
		2
		);

	FVector HitDirection = AimStartVec - AimEndVec;
	HitDirection.Normalize();

	if (HitResult.GetActor())
	{
		FDamageDescription dmg = GetDamage();
		dmg.Target = HitResult.GetActor();
		dmg.location = HitResult.Location;

		ASurvivalCharacter* event_instigator = UGameplayHelpers::GetControllerOwner<ASurvivalCharacter>(this);

		UMyGameplayStatics::ApplyCustomDamage(HitResult.GetActor(), dmg, HitDirection, HitResult, event_instigator->GetController(), this, UDamageType::StaticClass());
	}

	CurrentGunState = EGunState::Idle;
	CurrentAmmoState = EAmmoState::Ok;
}

AAmmoPickup* AGunWeapon::FindAmmoFromInventory()
{
	ASurvivalCharacter* MyCharacter = nullptr;

	UWorld* MyWorld = GetWorld();
	if (MyWorld && MyWorld->IsGameWorld())
	{
		APlayerController* MyPlayerController = MyWorld->GetFirstPlayerController();
		APawn* MyPawn = MyPlayerController->GetPawn();

		MyCharacter = Cast<ASurvivalCharacter>(MyPawn);
	}
	if (MyCharacter == nullptr)
		return nullptr;

	ACharacterInventory* MyInventory = MyCharacter->getInventory();

	TArray<AAmmoPickup*> AmmoItems;
	MyInventory->getAmmo(AmmoItems);

	if (AmmoItems.Num() == 0)
		return nullptr;

	for (int i = 0; i < AmmoItems.Num(); i++)
	{
		AAmmoPickup* AmmoPickup = AmmoItems[i];
		if (AmmoPickup->GetAmmoType() == GetWeaponType())
		{
			return AmmoPickup;
		}
	}
	return nullptr;
}

bool AGunWeapon::GetIsReloading()
{
	return bIsReloading;
}

EAmmoState AGunWeapon::BeginReloading()
{
	CurrentAmmoPickup = FindAmmoFromInventory();
	if (CurrentAmmoPickup == nullptr)
	{
		CurrentGunState = EGunState::Idle;
		CurrentAmmoState = EAmmoState::NoAmmo;
		return EAmmoState::NoAmmo;
	}

	if (CurrentAmmoPickup->getNumberOnStack() <= 0)
	{
		CurrentGunState = EGunState::Idle;
		CurrentAmmoState = EAmmoState::NoAmmo;
		return EAmmoState::NoAmmo;
	}
		

	bIsReloading = true;
	CurrentGunState = EGunState::Reloading;
	CurrentAmmoState = EAmmoState::Reloading;

	return EAmmoState::Reloading;
}

void AGunWeapon::EndReloading()
{
	int32 total_ammo = CurrentAmmoPickup->getNumberOnStack();

	if (total_ammo < magazine_capacity)
		current_ammo = total_ammo;
	else
		current_ammo = magazine_capacity;

	bIsReloading = false;
	CurrentGunState = EGunState::Idle;
	CurrentAmmoState = EAmmoState::Ok;
}

EGunState AGunWeapon::GetCurrentGunState()
{
	return CurrentGunState;
}

EAmmoState AGunWeapon::GetCurrentAmmoState()
{
	return CurrentAmmoState;
}

FText AGunWeapon::getItemDescription()
{
	FString weapon_description;

	ASurvivalCharacter* pawn = UGameplayHelpers::GetControllerOwner<ASurvivalCharacter>(this);
	if (pawn)
	{
		if (pawn->GetCharacterAttributesComponent())
		{
			weapon_description = weapon_properties.GenerateDescriptionWithStats(
				pawn->GetCharacterAttributesComponent()->AttributeStats);
		}
		else
		{
			weapon_description = weapon_properties.GenerateDescription();
		}
	}
	else
	{
		weapon_description = weapon_properties.GenerateDescription();
	}

	FString s_spread = FString::Printf(TEXT("Spread: %d%% \n"), (int)(spread*100));
	FString s_magazine = FString::Printf(TEXT("Mag Capacity: %d \n"), magazine_capacity);

	weapon_description.Append(s_spread);
	weapon_description.Append(s_magazine);

	return FText::FromString(weapon_description);
}
