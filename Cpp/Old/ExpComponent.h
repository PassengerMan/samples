#pragma once

#include "ExpComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLevelUpSignature, int32, NewLevel, int32, LevelsGained);

UCLASS()
class TREASUREHUNT_API UExpComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UExpComponent(const FObjectInitializer& PCIP);

	UPROPERTY(EditDefaultsOnly, SaveGame, Category = ExpComponent)
		UCurveFloat* LevelCurve;

	UPROPERTY(EditDefaultsOnly, SaveGame, Category = ExpComponent)
		UCurveFloat* ExpCurve;

	UPROPERTY(SaveGame)
		float Exp;

	UPROPERTY(SaveGame)
		int32 Level;

	UFUNCTION(BlueprintCallable, Category = ExpComponent)
		void AddExp(float ExpReceived);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = ExpComponent)
		float GetExp();

	UFUNCTION(BlueprintCallable, Category = ExpComponent)
		void SetLevel(int32 NewLevel);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = ExpComponent)
		int32 GetLevel();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = ExpComponent)
		float GetExpOnDeath();

	UPROPERTY(BlueprintAssignable, Category = ExpComponent)
	FOnLevelUpSignature OnLevelUp;

};