// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "Pawns/ShipPawn.h"
#include "World/Grid/WorldHexGrid.h"
#include "World/SplinePath.h"
#include "UI/Wrappers/WorldMapUIWrapper.h"
#include "UI/Wrappers/InventoryWidgetWrapper.h"
#include "Game/CameraDirector.h"

#include "Game/Commands/CommandStream.h"
#include "Game/Commands/MoveToCommand.h"

#include "ShipController.generated.h"


/**
*
*/
UENUM()
enum class EPlayerState: uint8
{
	Exploring,
	Building,
	Fighting,
};



/**
 * 
 */
UCLASS()
class TREASUREHUNT_API AShipController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AShipController(const FObjectInitializer& OI);

	virtual void PlayerTick(float DeltaTime) override;

	virtual void ProcessPlayerInput(const float DeltaTime, const bool bGamePaused) override;

	virtual void SetupInputComponent() override;

	virtual void PostInitializeComponents() override;

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void Possess(APawn* aPawn) override;
	
	virtual void HandleWorldMapAction(uint8 action_type);

	UPROPERTY()
	EPlayerState PlayerShipState;

	UPROPERTY()
	AShipPawn* ShipPawn;

	UPROPERTY()
	ACameraDirector* CameraDirector;

	UPROPERTY()
	AWorldHexGrid* WorldHexGrid;

	UPROPERTY(EditAnywhere)
	UBlueprint* BP_SplinePath;

	UPROPERTY()
	ASplinePath* SplineTilePath;

	FVector2D MouseLocation;
	FVector MouseWorldLocation;

	UPROPERTY()
	AWorldMapUIWrapper* WorldMapUIWidget;

	UPROPERTY()
	AInventoryWidgetWrapper* InventoryWidget;

	UPROPERTY()
	bool bShowInventory;

	//
	FCommandStream CommandStream;

protected:
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

	void OnShowInventory();

	void UpdateMouseLocation();

	UFUNCTION()
	virtual void OnMovementFinished(int32 path_index);
};
