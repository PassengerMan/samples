#pragma once

#include "Game/DamageDescription.h"
#include "TimerManager.h"
#include "DoTComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FTakeDamageSignature, float, damage, AActor*, DamageInstigator);

USTRUCT()
struct FDoTData
{
	GENERATED_USTRUCT_BODY()

public:
	bool bStarted;
	float DamageToApply;
	float CurrentTime;
	float Duration;
	AActor* DamageInstigator;

	UParticleSystemComponent* particle_system_component;
};

USTRUCT()
struct FDoTParticleData
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DoTParticleData)
	EDamageElementType element_type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DoTParticleData)
	UParticleSystem* particle_system;
};

UCLASS()
class TREASUREHUNT_API UDoTComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UDoTComponent(const FObjectInitializer& PCIP);

	virtual void InitializeComponent() override;

	UFUNCTION(BlueprintCallable, Category = DoTComponent)
	void StartDoT(EDamageElementType damage_element_type, float damage_over_time, float duration, AActor* DamageInstigator);

	void TakeDamage(float DeltaTime);

	UFUNCTION(BlueprintCallable, Category = DoTComponent)
	void StopAll();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction);

	TMap<uint8, FDoTData> DoTDatas;
	float AccumulatedTime;

	UPROPERTY(BlueprintAssignable, Category = DoTComponent)
	FTakeDamageSignature OnTakeDamage;

	UPROPERTY(EditAnywhere, SaveGame, Category = DoTComponent)
	TArray<FDoTParticleData> Particles;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = DoTComponent)
	UParticleSystem* GetParticleSystem(EDamageElementType element_type);
};
