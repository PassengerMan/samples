// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "Game/Inventory.h"
#include "Game/Components/ExpComponent.h"
#include "Game/Components/PawnAttributesComponent.h"
#include "Game/Components/HealthComponent.h"
#include "Game/Components/DoTComponent.h"
#include "Game/Components/PawnEquipmentSlotsComponent.h"
#include "GamePawn.generated.h"

UENUM(BlueprintType)
enum class EPawnType : uint8
{
	Player,
	Neutral,
	Enemy
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_EightParams(FTakeCustomDamageSignature, const FDamageDescription&, DamageDescription, class AController*, InstigatedBy, FVector, HitLocation, class UPrimitiveComponent*, FHitComponent, FName, BoneName, FVector, ShotFromDirection, const class UDamageType*, DamageType, AActor*, DamageCauser);

UCLASS(Blueprintable)
class AGamePawn : public APawn
{
	GENERATED_BODY()

public:
	AGamePawn(const FObjectInitializer& OI);

	virtual void PostInitializeComponents() override;

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
		USkeletalMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, SaveGame, Category = "GamePawn")
		EPawnType pawn_type;

	UFUNCTION(BlueprintCallable, Category = "GamePawn")
		EPawnType GetPawnType();

	////// Weapons + Inventory
	UPROPERTY(SaveGame)
	AInventory* inventory;

	UFUNCTION(BlueprintCallable, Category = Inventory)
		void setInventory(AInventory* _inventory);

	UFUNCTION(BlueprintCallable, Category = Inventory)
		AInventory* getInventory();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
		AWeaponPickup* GetActiveWeapon();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
		AWeaponPickup* GetWeapon(const FString& slot_name);

	/////// Damage
	UPROPERTY(BlueprintAssignable, Category = "Game|Damage")
		FTakeCustomDamageSignature OnTakeCustomDamage;

	UFUNCTION(BlueprintImplementableEvent, BlueprintAuthorityOnly, meta = (DisplayName = "CustomDamage"), Category = "Damage")
		void ReceiveCustomDamage(FDamageDescription DamageDescription, const class UDamageType* DamageType, FVector HitLocation, FVector HitNormal, class UPrimitiveComponent* HitComponent, FName BoneName, FVector ShotFromDirection, class AController* InstigatedBy, AActor* DamageCauser);

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	virtual FDamageDescription& ModifyDamage(FDamageDescription& DamageDescription);

	///Level/Exp
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, meta = (DisplayName = "OnLevelUp"), Category = "Character")
		void OnLevelUp_Notification(int32 NewLevel, int32 LevelsGained);
	UFUNCTION()
		virtual void OnLevelUp(int32 NewLevel, int32 LevelsGained);

	///Death
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, meta = (DisplayName = "OnDied"), Category = "Character")
		void OnDied_Notification(AActor* DamageInstigator);
	UFUNCTION()
		virtual void Die(AActor* DamageInstigator);

	UFUNCTION()
		UExpComponent* GetExpComponent();

	UFUNCTION()
		UPawnAttributesComponent* GetPawnAttributesComponent();

	UFUNCTION()
		UPawnEquipmentSlotsComponent* GetPawnEquipmentSlotsComponent();

	UFUNCTION()
		UHealthComponent* GetHealthComponent();

	UFUNCTION()
		UDoTComponent* GetDoTComponent();

private:
	UPROPERTY(VisibleAnywhere, Category = Components)
		UExpComponent* exp_component;

	UPROPERTY(VisibleAnywhere, Category = Components)
		UPawnAttributesComponent* pawn_attributes_component;

	UPROPERTY(VisibleAnywhere, Category = Components)
		UPawnEquipmentSlotsComponent* pawn_equipment_slots_component;

	UPROPERTY(VisibleAnywhere, Category = Components)
		UHealthComponent* health_component;

	UPROPERTY(VisibleAnywhere, Category = Components)
		UDoTComponent* dot_component;
};

