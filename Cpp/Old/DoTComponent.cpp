#include "TreasureHunt.h"
#include "TimerManager.h"
#include "DoTComponent.h"

UDoTComponent::UDoTComponent(const FObjectInitializer& PCIP)
{
	bWantsInitializeComponent = true;
	PrimaryComponentTick.bCanEverTick = true;
}

void UDoTComponent::InitializeComponent()
{

}

void UDoTComponent::StartDoT(EDamageElementType damage_element_type, float damage_over_time, float duration, AActor* DamageInstigator)
{
	bool already_exists = DoTDatas.Contains((uint8)damage_element_type);

	if (already_exists == true)
	{
		FDoTData& dot_data = DoTDatas[(uint8)damage_element_type];

		if (dot_data.bStarted == true)
			return;

		dot_data.CurrentTime = 0.0f;
		dot_data.DamageToApply = damage_over_time;
		dot_data.Duration = duration;
		dot_data.DamageInstigator = DamageInstigator;

		if (GetParticleSystem(damage_element_type))
		{
			dot_data.particle_system_component->DeactivateSystem();

			UParticleSystemComponent* particle_system_comp =
				UGameplayStatics::SpawnEmitterAttached(GetParticleSystem(damage_element_type), GetOwner()->GetRootComponent());

			dot_data.particle_system_component = particle_system_comp;
		}

		dot_data.bStarted = true;
	}

	if (already_exists == false)
	{
		FDoTData dot_data;
		dot_data.bStarted = true;
		dot_data.CurrentTime = 0.0f;
		dot_data.DamageToApply = damage_over_time;
		dot_data.Duration = duration;
		dot_data.DamageInstigator = DamageInstigator;

		if (GetParticleSystem(damage_element_type))
		{
			UParticleSystemComponent* particle_system_comp =
				UGameplayStatics::SpawnEmitterAttached(GetParticleSystem(damage_element_type), GetOwner()->GetRootComponent());
			dot_data.particle_system_component = particle_system_comp;
		}

		DoTDatas.Add((uint8)damage_element_type, dot_data);
	}
}

void UDoTComponent::TakeDamage(float DeltaTime)
{
	AccumulatedTime += DeltaTime;
	if (AccumulatedTime < 0.5f)
		return;

	//UE_LOG(LogTemp, Warning, TEXT("AccTime: %f"), AccumulatedTime);

	for (auto ElemIter = DoTDatas.CreateIterator(); ElemIter; ++ElemIter)
	{
		FDoTData& dot_data = ElemIter->Value;

		if (dot_data.bStarted == true)
		{
			if (dot_data.CurrentTime >= dot_data.Duration)
			{
				//UE_LOG(LogTemp, Warning, TEXT("DoT End: T: %.1f"), dot_data.CurrentTime);
				dot_data.CurrentTime = 0.0f;
				dot_data.bStarted = false;

				if (dot_data.particle_system_component)
					dot_data.particle_system_component->DeactivateSystem();

				continue;
			}

			dot_data.CurrentTime += AccumulatedTime;
			//UE_LOG(LogTemp, Warning, TEXT("DoT Apply: T: %.1f, D: %.1f"), dot_data.CurrentTime, dot_data.Duration);
			if (OnTakeDamage.IsBound())
			{
				OnTakeDamage.Broadcast(dot_data.DamageToApply, dot_data.DamageInstigator);
			}
		}
	}
	AccumulatedTime = 0.0f;
}

void UDoTComponent::StopAll()
{
	for (auto ElemIter = DoTDatas.CreateIterator(); ElemIter; ++ElemIter)
	{
		FDoTData& dot_data = ElemIter->Value;
		dot_data.CurrentTime = 0.0f;
		dot_data.bStarted = false;
		if (dot_data.particle_system_component)
			dot_data.particle_system_component->DeactivateSystem();
	}
}

void UDoTComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	TakeDamage(DeltaTime);
}

UParticleSystem* UDoTComponent::GetParticleSystem(EDamageElementType element_type)
{
	for (int i = 0; i < Particles.Num(); i++)
	{
		if (Particles[i].element_type == element_type)
			return Particles[i].particle_system;
	}
	return nullptr;
}

