#include "TreasureHunt.h"
#include "MoveToCommand.h"

FMoveToCommand::FMoveToCommand(AShipPawn* in_ship_pawn, AHexGridBase* in_grid)
{
	ship_pawn = in_ship_pawn;
	grid = in_grid;
}

void FMoveToCommand::Execute()
{
	if (grid == nullptr)
		return;

	TArray<FVector> Path;
	TilePath.Reset();

	bool was_path_cancelled;
	grid->FindPath(
		grid->GetPlayerOccupiedTile(),
		grid->GetPlayerDestinationTile(),
		TilePath, Path,
		was_path_cancelled);

	if (TilePath.Num() <= 0)
	{
		grid->SetPlayerDestinationTile(grid->GetPlayerOccupiedTile());
		return;
	}
	grid->SetPlayerDestinationTile(TilePath[TilePath.Num() - 1]); //hack for water holes inside islands

	ship_pawn->Move(Path);
}

void FMoveToCommand::Cancel()
{
	ship_pawn->CancelMovement();
}

bool FMoveToCommand::IsFinished()
{
	if (ship_pawn->IsFinishedMoving())
	{
		if(TilePath.IsValidIndex(ship_pawn->MovementComponent->GetCurrentPathIndex()))
			grid->SetPlayerOccupiedTile(TilePath[ship_pawn->MovementComponent->GetCurrentPathIndex()]);
	}

	return ship_pawn->IsFinishedMoving();
}

void FMoveToCommand::StepUpdate()
{

}

void FMoveToCommand::Update()
{

}
