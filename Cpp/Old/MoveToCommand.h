#pragma once

#include "Game/Commands/BaseCommand.h"
#include "Pawns/ShipPawn.h"
#include "World/Grid/HexGridBase.h"

class FMoveToCommand : public FBaseCommand
{
public:
	FMoveToCommand(AShipPawn* in_ship_pawn, AHexGridBase* in_grid);

	virtual void Execute() override;

	virtual void Cancel() override;

	virtual bool IsFinished() override;

	virtual void StepUpdate() override;

	virtual void Update() override;

private:
	AShipPawn* ship_pawn;
	AHexGridBase* grid;

	TArray<UHexTileBase*> TilePath;
};