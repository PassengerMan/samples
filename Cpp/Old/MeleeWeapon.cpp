#include "Survival.h"
#include "MeleeWeapon.h"

#include "Character/SurvivalCharacter.h"

AMeleeWeapon::AMeleeWeapon(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
	setUniqueTypeID(40); //default melee weapon type
	MeleeState = EMeleeState::None;

	TraceStart = PCIP.CreateDefaultSubobject<UArrowComponent>(this, "TraceStart");
	TraceStart->AttachTo(mesh_component);

	TraceEnd = PCIP.CreateDefaultSubobject<UArrowComponent>(this, "TraceEnd");
	TraceEnd->AttachTo(mesh_component);

	mesh_component->OnComponentBeginOverlap.AddDynamic(this, &AMeleeWeapon::OnOverlap);
}

FText AMeleeWeapon::getItemDescription()
{
	FString weapon_description;

	ASurvivalCharacter* pawn = UGameplayHelpers::GetControllerOwner<ASurvivalCharacter>(this);
	if (pawn)
	{
		if (pawn->GetCharacterAttributesComponent())
		{
			weapon_description =  weapon_properties.GenerateDescriptionWithStats(
				pawn->GetCharacterAttributesComponent()->AttributeStats);
		}
		else
		{
			weapon_description = weapon_properties.GenerateDescription();
		}
	}
	else
	{
		weapon_description = weapon_properties.GenerateDescription();
	}

	return FText::FromString(weapon_description);
}

void AMeleeWeapon::Attack_Implementation()
{
	if (MeleeState == EMeleeState::None)
		MeleeState = EMeleeState::Attacking;
}

void AMeleeWeapon::OnOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ASurvivalCharacter* MyChar = Cast<ASurvivalCharacter>(OtherActor);
	if (MyChar == UGameplayHelpers::GetControllerOwner<ASurvivalCharacter>(this))
		return;

	if (MeleeState == EMeleeState::Attacking)
	{
		FCollisionQueryParams CollisionQueryParams;
		CollisionQueryParams.bTraceComplex = true;
		CollisionQueryParams.AddIgnoredActor(this);

		FHitResult HitResult;
		bool trace_found = OtherComp->LineTraceComponent(
			HitResult,
			TraceStart->GetComponentLocation(),
			TraceEnd->GetComponentLocation(),
			CollisionQueryParams);

		if (trace_found == false)
			return;

		DrawDebugLine(
			GetWorld(),
			TraceStart->GetComponentLocation(),
			TraceEnd->GetComponentLocation(),
			FColor(255, 0, 0),
			false,
			0.2f,
			0,
			2
			);

		FDamageDescription dmg = GetDamage();
		dmg.Target = OtherActor;
		dmg.location = HitResult.Location;

		ASurvivalCharacter* event_instigator = UGameplayHelpers::GetControllerOwner<ASurvivalCharacter>(this);
		UMyGameplayStatics::ApplyCustomDamage(HitResult.GetActor(), dmg, FVector(0, 0, 0), HitResult, event_instigator->GetController(), this, UDamageType::StaticClass());
	
		MeleeState = EMeleeState::Hit;
	}
}

EMeleeState AMeleeWeapon::GetMeleeState()
{
	return MeleeState;
}

void AMeleeWeapon::EndAttack()
{
	MeleeState = EMeleeState::None;
}
