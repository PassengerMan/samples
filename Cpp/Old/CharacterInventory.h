#pragma once

#include "GameFramework/Actor.h"

#include "Pickups/Pickup.h"
#include "Pickups/CurrencyPickup.h"
#include "Pickups/QuestPickup.h"
#include "Pickups/WeaponPickup.h"
#include "Pickups/MeleeWeapon.h"
#include "Pickups/GunWeapon.h"
#include "Pickups/AmmoPickup.h"

#include "CharacterInventoryGrid.h"

#include "CharacterInventory.generated.h"

UENUM(BlueprintType)
namespace ERemoveItemResult
{
	enum Result
	{
		E_NOT_ENOUGH_ON_STACK UMETA(DisplayName = "Not enough on stack"),
		E_SUCCESS UMETA(DisplayName = "Success"),
		MAX UMETA(Hidden)
	};
}

UENUM(BlueprintType)
namespace EAddItemResult
{
	enum Result
	{
		E_NOT_ENOUGH_SPACE UMETA(DisplayName = "Not enough space"),
		E_SUCCESS UMETA(DisplayName = "Success"),
		MAX UMETA(Hidden)
	};
}

UENUM(BlueprintType)
enum class EAddAmmoResult
{
	Success,
	Full,
	Error
};
/**
 * 
 */
UCLASS()
class SURVIVAL_API ACharacterInventory : public AActor
{
	GENERATED_UCLASS_BODY()

private:
	UPROPERTY(SaveGame)
	TArray<APickup*> items;

	UPROPERTY(SaveGame)
	TArray<AAmmoPickup*> ammo_items;

	UPROPERTY(SaveGame)
	int32 currency;

	UPROPERTY()
	FVector2D grid_size;

	UPROPERTY()
	int32 grid_tile_size;

public:
	UPROPERTY()
	FCharacterInventoryGrid character_inventory_grid;

	UFUNCTION(BlueprintCallable, Category = Inventory)
	EAddItemResult::Result addItem(APickup* item);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void removeItem(APickup* item);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void removeAllByTypeID(int32 type_id);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	ERemoveItemResult::Result removeByTypeID(int32 type_id, int32 count);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void removeAllByPickupTypeID(APickup* pickup);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	ERemoveItemResult::Result removeByPickupTypeID(APickup* pickup, int32 count);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	APickup* getItem(FVector2D inventory_position);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	int32 getItemCountByTypeID(int32 type_id);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	APickup* getFirstItemByClass(UClass* subclass);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void getItemsByClass(UClass* subclass, TArray<APickup*>& out_array);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	TArray<APickup*>& getItems();

	UFUNCTION(BlueprintCallable, Category = Inventory)
	int32 getCurrency();

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void setCurrency(int32 currency_total);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool addCurrency(int32 currency_amount);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool removeCurrency(int32 currency_amount);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	EAddAmmoResult addAmmo(AAmmoPickup* ammo_pickup);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void getAmmo(TArray<AAmmoPickup*>& out_ammo_items);

	UFUNCTION()
	int32 getGridTileSize();
	UFUNCTION()
	FVector2D& getGridSize();

};
