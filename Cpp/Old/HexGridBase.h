#pragma once

#include "HexTileBase.h"
#include "HexGridBase.generated.h"

UCLASS(Blueprintable)
class AHexGridBase : public AActor
{
	GENERATED_BODY()

public:
	AHexGridBase(const FObjectInitializer& OI);

	virtual void PostInitializeComponents() override;
	virtual void OnConstruction(const FTransform& Transform) override;

	virtual void BeginPlay() override;

	UHexTileBase* GetTile(FIntPoint position);
	UHexTileBase* GetTile(UInstancedStaticMeshComponent* component, int32 instance_id);
	TMap<FIntPoint, UHexTileBase*>& GetTiles();


	uint8 GetTileTypeFromComponent(UInstancedStaticMeshComponent* component);
	
	void ClearContainers();
	void LogContainerSizes();

	virtual void Init(int32 size_x, int32 size_y, int32 seed);
	virtual void GenerateGrid();
	virtual void GenerateMeshes();

	virtual void GetNeighbours(const FIntPoint& TileGridPos, TArray<FIntPoint>& out_tiles);
	virtual void GetNeighbourOffsets(const FIntPoint& TileGridPos, TArray<FIntPoint>& out_offsets);
	virtual int32 TileDistance(const FIntPoint& c1, const FIntPoint& c2); //chebyshev
	virtual void FindPath(UHexTileBase* StartTile, UHexTileBase* DestinationTile, TArray<UHexTileBase*>& out_tiles, TArray<FVector>& out_path, bool& was_cancelled);

	void SetPlayerOccupiedTile(UHexTileBase* tile);
	void SetPlayerDestinationTile(UHexTileBase* tile);

	UHexTileBase* GetPlayerOccupiedTile();
	UHexTileBase* GetPlayerDestinationTile();

	TMap<FIntPoint, UTextRenderComponent*> tile_debug_texts;
protected:
	TMap<FIntPoint, UHexTileBase*> tiles;
	
	TMap<UInstancedStaticMeshComponent*, TMap<int32, FIntPoint>> tiles_instance_id_lookup;
	TMap<uint8, UInstancedStaticMeshComponent*> InstancedMeshComponents;

	UPROPERTY()
	TArray<UObject*> ref_hack_array;


	UPROPERTY(EditAnywhere)
	FIntPoint grid_size;

	UPROPERTY(EditAnywhere)
	float grid_spacing;

	UPROPERTY()
	int32 grid_seed;

	UPROPERTY()
	FRandomStream seeded_stream;

	UPROPERTY()
		UHexTileBase* PlayerOccuppiedTile;
	UPROPERTY()
		UHexTileBase* PlayerDestinationTile;

public:
	UPROPERTY(EditAnywhere)
	UStaticMesh* HexMesh; //StaticMesh'/Game/Meshes/Hex.Hex'

};

//Path finding structs
struct FGridPointPriority
{
	explicit FGridPointPriority(const FIntPoint& InGridPoint, int32 InPriority)
	{
		GridPoint = InGridPoint;
		Priority = InPriority;
	}

	FIntPoint GridPoint;
	int32 Priority;
};

struct FGridPointPriorityPredicate
{
	bool operator()(const FGridPointPriority& A, const FGridPointPriority& B) const
	{
		// Inverted compared to std::priority_queue - higher priorities float to the top
		return A.Priority < B.Priority;
	}
};