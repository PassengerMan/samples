// Fill out your copyright notice in the Description page of Project Settings.

#include "TreasureHunt.h"
#include "ShipPawn.h"


// Sets default values
AShipPawn::AShipPawn(const FObjectInitializer& OI) : Super(OI)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MovementComponent = OI.CreateDefaultSubobject<UShipPathMovement>(this, TEXT("ShipPathMovementComponent"));
	MovementComponent->bAutoRegisterUpdatedComponent = true;

	DefaultCameraDistance = 1200.0f;
	DefaultCameraZoomDistance = 750.0f;
	DefaultCameraRotation = FRotator(-90.0f, 0.0f, 0.0f);

	// Create a camera boom...
	CameraArm = OI.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("CameraArm"));
	CameraArm->AttachTo(RootComponent);
	CameraArm->bAbsoluteRotation = true;
	CameraArm->TargetArmLength = DefaultCameraDistance;
	CameraArm->RelativeRotation = DefaultCameraRotation;
	CameraArm->bDoCollisionTest = false;
	CameraArm->bEnableCameraLag = true;
	CameraArm->CameraLagSpeed = 10.0f;

	// Create a camera...
	Camera = OI.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Camera"));
	Camera->AttachTo(CameraArm, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm	

	GetHealthComponent()->SetDefaultHealth(100);

	UE_LOG(DebugLog, Warning, TEXT("AShipPawn::AShipPawn"));
}

void AShipPawn::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);
}

// Called when the game starts or when spawned
void AShipPawn::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(DebugLog, Warning, TEXT("AShipPawn::PostInitializeComponents"));
}


void AShipPawn::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//UGlobals::Get().GameSaves->SavePlayer(this);

	Super::EndPlay(EndPlayReason);
}


// Called every frame
void AShipPawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

// Called to bind functionality to input
void AShipPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

void AShipPawn::Move(const TArray<FVector>& Path)
{
	if (MovementComponent)
	{
		MovementComponent->Move(Path);
	}
}

bool AShipPawn::IsFinishedMoving()
{
	if (MovementComponent)
	{
		return MovementComponent->IsFinishedMoving();
	}
	return true;
}

void AShipPawn::CancelMovement()
{
	if (MovementComponent)
	{
		MovementComponent->CancelMovement();
	}
}
