#pragma once

#include "CharacterBonePartitionComponent.generated.h"

USTRUCT(BlueprintType)
struct FRemovableBoneStruct
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RemovableBoneStruct)
	FName BoneName; //
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RemovableBoneStruct)
	int32 HitsNeededToRemove;

	int32 HitCount;
};

UCLASS()
class UCharacterBonePartitionComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UCharacterBonePartitionComponent(const FObjectInitializer& PCIP);

	virtual void InitializeComponent();

	TMap<FName, FRemovableBoneStruct> BoneHits;

	UPROPERTY(EditDefaultsOnly, Category = CharacterBonePartitionComponent)
	TArray<FRemovableBoneStruct> RemovableHeadBones;
	UPROPERTY(EditDefaultsOnly, Category = CharacterBonePartitionComponent)
	TArray<FRemovableBoneStruct> RemovableArmBones;
	UPROPERTY(EditDefaultsOnly, Category = CharacterBonePartitionComponent)
	TArray<FRemovableBoneStruct> RemovableLegBones;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterBonePartitionComponent)
	UParticleSystem* Hit_ParticleSystem;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterBonePartitionComponent)
	UParticleSystem* Hit_BoneRemoved_ParticleSystem;

	UPROPERTY(BlueprintReadWrite, Category = CharacterBonePartitionComponent)
	bool bHeadRemoved;

	UPROPERTY(BlueprintReadWrite, Category = CharacterBonePartitionComponent)
	bool bArmRemoved;

	UPROPERTY(BlueprintReadWrite, Category = CharacterBonePartitionComponent)
	bool bLegRemoved;


	UFUNCTION(BlueprintCallable, Category = CharacterBonePartitionComponent)
	bool RemoveBone(FName& BoneName, USkeletalMeshComponent* SkeletalMesh); //return true if removed

	UFUNCTION(BlueprintCallable, Category = CharacterBonePartitionComponent)
	bool HitBone(FName& BoneName, const FVector& HitLoc, USkeletalMeshComponent* SkeletalMesh); //return true if removed
};