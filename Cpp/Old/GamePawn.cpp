// Fill out your copyright notice in the Description page of Project Settings.

#include "TreasureHunt.h"
#include "GamePawn.h"


// Sets default values
AGamePawn::AGamePawn(const FObjectInitializer& OI)
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	UArrowComponent* ArrowComp = OI.CreateDefaultSubobject<UArrowComponent>(this, TEXT("ArrowComp"));
	ArrowComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	ArrowComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = ArrowComp;

	MeshComponent = OI.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("SkeletalMeshComponent"));
	MeshComponent->AttachTo(RootComponent);

	exp_component = OI.CreateDefaultSubobject<UExpComponent>(this, "ExpComponent");
	exp_component->OnLevelUp.AddDynamic(this, &AGamePawn::OnLevelUp);

	pawn_attributes_component = OI.CreateDefaultSubobject<UPawnAttributesComponent>(this, "PawnAttributesComponent");
	pawn_equipment_slots_component = OI.CreateDefaultSubobject<UPawnEquipmentSlotsComponent>(this, "PawnEquipmentSlotsComponent");

	dot_component = OI.CreateDefaultSubobject<UDoTComponent>(this, "DoTComponent");
	health_component = OI.CreateDefaultSubobject<UHealthComponent>(this, "HealthComponent");
	health_component->OnDie.AddDynamic(this, &AGamePawn::Die);
	health_component->SetDoTComponent(dot_component);
}

void AGamePawn::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}


void AGamePawn::BeginPlay()
{
	if (pawn_equipment_slots_component)
	{
		pawn_equipment_slots_component->InitSlots(this);
	}

	FActorSpawnParameters spawn_params;
	spawn_params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawn_params.Owner = this;
	inventory = GetWorld()->SpawnActor<AInventory>(AInventory::StaticClass(), FTransform(), spawn_params);
}


void AGamePawn::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (inventory && inventory->IsValidLowLevel())
	{
		inventory->Destroy();
	}
}


EPawnType AGamePawn::GetPawnType()
{
	return pawn_type;
}

void AGamePawn::setInventory(AInventory* _inventory)
{
	this->inventory = _inventory;
}

AInventory* AGamePawn::getInventory()
{
	return inventory;
}

AWeaponPickup* AGamePawn::GetActiveWeapon()
{
	return nullptr;
}

AWeaponPickup* AGamePawn::GetWeapon(const FString& slot_name)
{
	return pawn_equipment_slots_component->GetWeapon(slot_name);
}

UExpComponent* AGamePawn::GetExpComponent()
{
	return exp_component;
}

UPawnAttributesComponent* AGamePawn::GetPawnAttributesComponent()
{
	return pawn_attributes_component;
}

UPawnEquipmentSlotsComponent* AGamePawn::GetPawnEquipmentSlotsComponent()
{
	return pawn_equipment_slots_component;
}

UHealthComponent* AGamePawn::GetHealthComponent()
{
	return health_component;
}

UDoTComponent* AGamePawn::GetDoTComponent()
{
	return dot_component;
}

float AGamePawn::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float local_damage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	UDamageType const* const DamageTypeCDO = DamageEvent.DamageTypeClass ? DamageEvent.DamageTypeClass->GetDefaultObject<UDamageType>() : GetDefault<UDamageType>();
	if (DamageEvent.IsOfType(FCustomDamageEvent::ClassID))
	{
		FCustomDamageEvent* const CustomDamageEvent = (FCustomDamageEvent*)&DamageEvent;

		if (CustomDamageEvent == nullptr)
			return local_damage;

		FDamageDescription& dmg_description = ModifyDamage(CustomDamageEvent->GetDamageDescription());

		float total_dmg = dmg_description.damage_amount;
		total_dmg += dmg_description.fire_damage_amount;
		total_dmg += dmg_description.lighting_damage_amount;
		total_dmg += dmg_description.poison_damage_amount;

		if (GetHealthComponent())
		{
			//UE_LOG(LogTemp, Warning, TEXT("Total dmg: %.1f"), total_dmg);

			GetHealthComponent()->TakeDamage(total_dmg, EventInstigator->GetPawn());
			if (CustomDamageEvent->GetDamageDescription().fire_damage_amount > 0)
			{
				int32 status_rand = FMath::RandRange(0, 100);
				if (GetDoTComponent() && status_rand < 10)
					GetDoTComponent()->StartDoT(EDamageElementType::Fire, CustomDamageEvent->GetDamageDescription().fire_damage_amount / 10.0f, 10, EventInstigator->GetPawn());
			}
			if (CustomDamageEvent->GetDamageDescription().lighting_damage_amount > 0)
			{
				int32 status_rand = FMath::RandRange(0, 100);
				if (GetDoTComponent() && status_rand < 10)
					GetDoTComponent()->StartDoT(EDamageElementType::Lighting, CustomDamageEvent->GetDamageDescription().lighting_damage_amount / 10.0f, 10, EventInstigator->GetPawn());
			}
			if (CustomDamageEvent->GetDamageDescription().poison_damage_amount > 0)
			{
				int32 status_rand = FMath::RandRange(0, 100);
				if (GetDoTComponent() && status_rand < 10)
					GetDoTComponent()->StartDoT(EDamageElementType::Poison, CustomDamageEvent->GetDamageDescription().poison_damage_amount / 10.0f, 10, EventInstigator->GetPawn());
			}
		}

		ReceiveCustomDamage(dmg_description, DamageTypeCDO, CustomDamageEvent->HitInfo.ImpactPoint, CustomDamageEvent->HitInfo.ImpactNormal, CustomDamageEvent->HitInfo.Component.Get(), CustomDamageEvent->HitInfo.BoneName, CustomDamageEvent->ShotDirection, EventInstigator, DamageCauser);
		OnTakeCustomDamage.Broadcast(dmg_description, EventInstigator, CustomDamageEvent->HitInfo.ImpactPoint, CustomDamageEvent->HitInfo.Component.Get(), CustomDamageEvent->HitInfo.BoneName, CustomDamageEvent->ShotDirection, DamageTypeCDO, DamageCauser);
	}
	return local_damage;
}

FDamageDescription& AGamePawn::ModifyDamage(FDamageDescription& DamageDescription)
{
	/*TArray<APickup*> armor_items;
	inventory->getItemsByClass(AArmorPickup::StaticClass(), armor_items);

	if (armor_items.Num() == 0)
		return DamageDescription;

	FArmorProperties ArmorProperties;
	for (int i = 0; i < armor_items.Num(); i++)
	{
		AArmorPickup* armor_item = Cast<AArmorPickup>(armor_items[i]);
		if (armor_item)
		{
			UItemEquippableComponent* equippable_comp =
				Cast<UItemEquippableComponent>(armor_item->GetComponentByClass(UItemEquippableComponent::StaticClass()));

			if (equippable_comp)
			{
				if (equippable_comp->GetIsEquipped() == true)
				{
					ArmorProperties += armor_item->armor_properties;
				}
			}
		}
	}*/

	//@TODO implement char stats 
	/*if (GetPawnAttributesComponent())
	{
		int32 rand_dodge = FMath::RandRange(0, 100);
		if (rand_dodge < ArmorProperties.inc_dodge_chance + pawn_attributes_component->AttributeStats.inc_dodge_chance)
		{
			DamageDescription.damage_amount = 0.0f;
			DamageDescription.fire_damage_amount = 0.0f;
			DamageDescription.lighting_damage_amount = 0.0f;
			DamageDescription.poison_damage_amount = 0.0f;
			//no dmg
			return DamageDescription;
		}

		DamageDescription.damage_amount -=
			(DamageDescription.damage_amount*((ArmorProperties.red_damage_taken + pawn_attributes_component->AttributeStats.red_damage_taken) / 100.0f));
		DamageDescription.fire_damage_amount -=
			(DamageDescription.fire_damage_amount*((ArmorProperties.inc_fire_res + pawn_attributes_component->AttributeStats.inc_fire_res) / 100.0f));
		DamageDescription.lighting_damage_amount -=
			(DamageDescription.lighting_damage_amount*((ArmorProperties.inc_lighting_res + pawn_attributes_component->AttributeStats.inc_lighting_res) / 100.0f));
		DamageDescription.poison_damage_amount -=
			(DamageDescription.poison_damage_amount*((ArmorProperties.inc_poison_res + pawn_attributes_component->AttributeStats.inc_poison_res) / 100.0f));
	}
	else
	{
		int32 rand_dodge = FMath::RandRange(0, 100);
		if (rand_dodge < ArmorProperties.inc_dodge_chance)
		{
			DamageDescription.damage_amount = 0.0f;
			DamageDescription.fire_damage_amount = 0.0f;
			DamageDescription.lighting_damage_amount = 0.0f;
			DamageDescription.poison_damage_amount = 0.0f;
			//no dmg
			return DamageDescription;
		}

		DamageDescription.damage_amount -= (DamageDescription.damage_amount*(ArmorProperties.red_damage_taken / 100.0f));
		DamageDescription.fire_damage_amount -= (DamageDescription.fire_damage_amount*(ArmorProperties.inc_fire_res / 100.0f));
		DamageDescription.lighting_damage_amount -= (DamageDescription.lighting_damage_amount*(ArmorProperties.inc_lighting_res / 100.0f));
		DamageDescription.poison_damage_amount -= (DamageDescription.poison_damage_amount*(ArmorProperties.inc_poison_res / 100.0f));
	}*/

	return DamageDescription;
}

void AGamePawn::Die(AActor* DamageInstigator)
{
	if (GetHealthComponent())
		GetHealthComponent()->SetHealth(0.0f);

	if (GetDoTComponent())
		GetDoTComponent()->StopAll();

	AGamePawn* DamageDealer = Cast<AGamePawn>(DamageInstigator);
	if (DamageDealer)
	{
		if (GetExpComponent() && DamageDealer->GetExpComponent())
			DamageDealer->GetExpComponent()->AddExp(GetExpComponent()->GetExpOnDeath());
	}

	OnDied_Notification(DamageInstigator);
}

void AGamePawn::OnLevelUp(int32 NewLevel, int32 LevelsGained)
{
	if (GetPawnAttributesComponent())
	{
		pawn_attributes_component->AddPointsToAllocate(LevelsGained);
	}

	OnLevelUp_Notification(NewLevel, LevelsGained);
}



