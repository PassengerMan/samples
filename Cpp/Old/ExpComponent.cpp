#include "TreasureHunt.h"
#include "ExpComponent.h"


UExpComponent::UExpComponent(const FObjectInitializer& PCIP) : Super(PCIP)
{
	this->ComponentTags.Add(FName("Save"));
}

void UExpComponent::AddExp(float ExpReceived)
{
	Exp += ExpReceived;

	if (ExpCurve)
	{
		int32 new_lvl = (int32)ExpCurve->GetFloatValue(Exp);
		if (new_lvl > Level)
		{
			int32 levels_gained = new_lvl - Level;
			Level = new_lvl;
			if (OnLevelUp.IsBound())
			{
				OnLevelUp.Broadcast(Level, levels_gained);
			}
				
		}
	}
}

float UExpComponent::GetExp()
{
	return Exp;
}

void UExpComponent::SetLevel(int32 NewLevel)
{
	int32 levels_gained = NewLevel - Level;
	Level = NewLevel;

	if (LevelCurve)
	{
		Exp = LevelCurve->GetFloatValue(Level);

		if (OnLevelUp.IsBound())
			OnLevelUp.Broadcast(Level, levels_gained);
	}
}

int32 UExpComponent::GetLevel()
{
	return Level;
}

float UExpComponent::GetExpOnDeath()
{
	float exp_on_death = (Exp / (float)FMath::RandRange(50, 100));
	return exp_on_death;
}
