// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "GamePawn.h"
#include "ShipPathMovement.h"
#include "Game/Components/HealthComponent.h"

#include "ShipPawn.generated.h"

UCLASS()
class TREASUREHUNT_API AShipPawn : public AGamePawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AShipPawn(const FObjectInitializer& OI);

	UPROPERTY(VisibleAnywhere, Category = "Movement")
	UShipPathMovement* MovementComponent;

	UPROPERTY(VisibleAnywhere, Category = "Camera")
	UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, Category = "Camera")
	USpringArmComponent* CameraArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, SaveGame, Category = "Camera")
	float DefaultCameraDistance;

	UPROPERTY(EditDefaultsOnly, SaveGame, Category = "Camera")
	float DefaultCameraZoomDistance;

	UPROPERTY(EditDefaultsOnly, SaveGame, Category = "Camera")
	FRotator DefaultCameraRotation;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void Serialize(FArchive& Ar) override;

	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UFUNCTION(BlueprintCallable, Category = "Ship")
	void Move(const TArray<FVector>& Path);

	UFUNCTION(BlueprintCallable, Category = "Ship")
	void CancelMovement();

	UFUNCTION(BlueprintCallable, Category = "Ship")
	bool IsFinishedMoving();

};
