

#include "Survival.h"
#include "CharacterInventory.h"


ACharacterInventory::ACharacterInventory(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
	grid_tile_size = 64;
	grid_size = FVector2D(12, 9);

	character_inventory_grid.setInventoryPtr(this);
	character_inventory_grid.initEmptyGrid();
}

int32 ACharacterInventory::getGridTileSize()
{
	return grid_tile_size;
}

FVector2D& ACharacterInventory::getGridSize()
{
	return grid_size;
}

EAddItemResult::Result ACharacterInventory::addItem(APickup* item)
{
	bool add_new_item = true;

	if (items.Num() == 0)
	{
		item->setInventoryPosition(0, 0);
		item->calculateInventorySizeFromImage(grid_tile_size);
		items.Add(item);
		item->SetOwner(this);
		item->Hide(true);

		character_inventory_grid.fillGridWithInventory();
		return EAddItemResult::E_SUCCESS;
	}

	for (int32 i = 0; i < items.Num(); i++)
	{
		if (items[i] == nullptr || item == nullptr)
			continue; 

		if (items[i]->getUniqueTypeID() == item->getUniqueTypeID())
		{
			if (items[i]->getNumberOnStack() >= items[i]->getMaxStack()) //avoid already full stack
			{
				continue;
			}

			int32 left_overs = items[i]->addNewOnStack(item->getNumberOnStack());
			if (left_overs <= 0)
			{
				item->Destroy();
				add_new_item = false;
				break;
			}
			else if (left_overs >= 1)
			{
				item->setNumberOnStack(left_overs);

				item->calculateInventorySizeFromImage(grid_tile_size);

				FVector2D free_position;
				bool enough_space = character_inventory_grid.getFreeGridPosition(
					item->getInventorySize().X,
					item->getInventorySize().Y,
					free_position);

				if (enough_space == true)
				{
					item->setInventoryPosition(free_position.X, free_position.Y);
					items.Add(item);
					item->SetOwner(this);
					item->Hide(true);
				}
				else
				{
					return EAddItemResult::E_NOT_ENOUGH_SPACE;
				}

				add_new_item = false;
				continue;
			}
		}
	}

	if (add_new_item == true)
	{
		item->calculateInventorySizeFromImage(grid_tile_size);

		FVector2D free_position;
		bool enough_space = character_inventory_grid.getFreeGridPosition(
			item->getInventorySize().X,
			item->getInventorySize().Y,
			free_position);
	

		if (enough_space == true)
		{
			item->setInventoryPosition(free_position.X, free_position.Y);
			items.Add(item);
			item->SetOwner(this);
			item->Hide(true);
		}
		else
		{
			return EAddItemResult::E_NOT_ENOUGH_SPACE;
		}
	}

	character_inventory_grid.fillGridWithInventory();

	return EAddItemResult::E_SUCCESS;
}

void ACharacterInventory::removeItem(APickup* item)
{
	item->Destroy();
	items.Remove(item);
}

void ACharacterInventory::removeAllByTypeID(int32 type_id)
{
	int32 number_of_items = items.Num();
	for (int32 i = 0; i < number_of_items; i++)
	{
		if (items[i]->getUniqueTypeID() == type_id)
		{
			items[i]->Destroy();
			items.RemoveAt(i);
		}
	}
}


ERemoveItemResult::Result ACharacterInventory::removeByTypeID(int32 type_id, int32 count)
{
	int32 number_of_items = items.Num();
	int32 left_overs = 0;

	int32 total_on_stack = getItemCountByTypeID(type_id);

	if (count > total_on_stack)
	{
		return ERemoveItemResult::E_NOT_ENOUGH_ON_STACK;
	}

	for (int32 i = 0; i < number_of_items; i++)
	{
		if (items[i]->getUniqueTypeID() == type_id)
		{
			int32 number_on_stack = items[i]->getNumberOnStack();
			left_overs = number_on_stack - count;

			if (left_overs > 0)
			{
				items[i]->setNumberOnStack(left_overs);
				break;
			}
			if (left_overs == 0)
			{
				items[i]->Destroy();
				items.RemoveAt(i);
				break;
			}
			if (left_overs < 0)
			{
				count = -left_overs; //make it positive
				items[i]->Destroy();
				items.RemoveAt(i);
			}
		}
	}
	return ERemoveItemResult::E_SUCCESS;
}

void ACharacterInventory::removeAllByPickupTypeID(APickup* pickup)
{
	removeAllByTypeID(pickup->getUniqueTypeID());
}

ERemoveItemResult::Result ACharacterInventory::removeByPickupTypeID(APickup* pickup, int32 count)
{
	return removeByTypeID(pickup->getUniqueTypeID(), count);
}

APickup* ACharacterInventory::getItem(FVector2D inventory_position)
{
	for (int32 i = 0; i < items.Num(); i++)
	{
		if (items[i]->getInventoryPosition().X == inventory_position.X &&
			items[i]->getInventoryPosition().Y == inventory_position.Y)
		{
			return items[i];
		}
	}
	return NULL;
}

int32 ACharacterInventory::getItemCountByTypeID(int32 type_id)
{
	int32 number_on_stack = 0;
	for (int32 i = 0; i < items.Num(); i++)
	{
		if (items[i]->getUniqueTypeID() == type_id)
		{
			number_on_stack += items[i]->getNumberOnStack();
		}
	}
	return number_on_stack;
}



APickup* ACharacterInventory::getFirstItemByClass(UClass* subclass)
{
	for (int32 i = 0; i < items.Num(); i++)
	{
		if (items[i]->IsA(subclass))
		{
			return items[i];
		}
	}
	return nullptr;
}

void ACharacterInventory::getItemsByClass(UClass* subclass, TArray<APickup*>& out_array)
{
	out_array.Reset();
	for (int32 i = 0; i < items.Num(); i++)
	{
		if (items[i]->IsA(subclass))
		{
			out_array.Add(items[i]);
		}
	}
}

TArray<APickup*>& ACharacterInventory::getItems()
{
	return items;
}

int32 ACharacterInventory::getCurrency()
{
	return currency;
}

void ACharacterInventory::setCurrency(int32 currency_total)
{
	currency = currency_total;
}

bool ACharacterInventory::addCurrency(int32 currency_amount)
{
	currency += currency_amount;
	return true;
}

bool ACharacterInventory::removeCurrency(int32 currency_amount)
{
	int temp = currency - currency_amount;
	if (temp < 0)
	{
		return false; //not able to buy check
	}

	currency -= currency_amount;
	return true;
}

EAddAmmoResult ACharacterInventory::addAmmo(AAmmoPickup* ammo_pickup)
{
	bool add_new_item = true;

	if (ammo_items.Num() == 0)
	{
		ammo_pickup->setInventoryPosition(-1, -1);
		ammo_pickup->calculateInventorySizeFromImage(grid_tile_size);
		ammo_items.Add(ammo_pickup);
		ammo_pickup->Hide(true);
		return EAddAmmoResult::Success;
	}

	for (int32 i = 0; i < ammo_items.Num(); i++)
	{
		if (ammo_items[i] == nullptr || ammo_pickup == nullptr)
			return EAddAmmoResult::Error;

		if (ammo_items[i]->getUniqueTypeID() == ammo_pickup->getUniqueTypeID())
		{
			if (ammo_items[i]->getNumberOnStack() >= ammo_items[i]->getMaxStack()) //avoid already full stack
			{
				return EAddAmmoResult::Full;
			}

			int32 left_overs = ammo_items[i]->addNewOnStack(ammo_pickup->getNumberOnStack());
			if (left_overs <= 0)
			{
				ammo_pickup->Destroy();
				add_new_item = false;
				break;
			}
		}
	}

	if (add_new_item == true)
	{
		ammo_pickup->calculateInventorySizeFromImage(grid_tile_size);

		ammo_pickup->setInventoryPosition(-1, -1);
		ammo_items.Add(ammo_pickup);
		ammo_pickup->Hide(true);
	}
	return EAddAmmoResult::Success;
}

void ACharacterInventory::getAmmo(TArray<AAmmoPickup*>& out_ammo_items)
{
	out_ammo_items.Reset();
	out_ammo_items.Append(ammo_items);
}
