// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UI/Widgets/PANBaseWidget.h"
#include "UI/Widgets/PANCardWidget.h"
#include "UI/Widgets/PANComplexTextWidget.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "PANCardTableWidget.generated.h"

/**
 * 
 */
UCLASS()
class PANDEMIA_API UPANCardTableWidget : public UPANBaseWidget
{
	GENERATED_BODY()
	
public:
	UPANCardTableWidget(const FObjectInitializer& _oi);
	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	
	virtual FReply NativeOnMouseMove(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

	UFUNCTION(BlueprintCallable, Category = UICardTable)
	void RegisterCard(UPANCardWidget* _cardWidget);

	UFUNCTION(BlueprintCallable, Category = UICardTable)
	void RegisterCardDescription(UPANComplexTextWidget* _cardDescriptionWidget);

	UFUNCTION(BlueprintCallable, Category = UICardTable)
	void RegisterChoices(UTextBlock* _choice1, UTextBlock* _choice2);

	UFUNCTION()
	void OnCardPressed(int32 _param);
	UFUNCTION()
	void OnCardReleased(int32 _param);

	void HandleCardMovement(bool _bCardPressed, bool _bCardReleased);

	void VisualizeOnShow(const FText& _cardDescription, const FText& _leftChoiceText, const FText& _rightChoiceText);
protected:
	UPROPERTY()
	UPANCardWidget* CardWidget;

	FVector2D StartCardPostition;
	float CardDirection;

	bool bCardPressed;
	bool bCardReleased;
	bool bRecordCardPressMousePosition;

	FVector2D CurrentMousePos;
	FVector2D OnCardPressedMousePos;
	FVector2D MouseDelta;
	float DeltaMult;

	UPROPERTY()
	UPANComplexTextWidget* CardDescriptionWidget;

	UPROPERTY()
	UTextBlock* Choice1Block;

	UPROPERTY()
	UTextBlock* Choice2Block;
};
