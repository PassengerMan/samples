// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SPAGameStateBase.h"

class FSPAGameStateGarage : public FSPAGameStateBase
{

public:
	FSPAGameStateGarage() {};

	virtual void InitBase() override;
	virtual void Enter() override;
	virtual void Exit() override;
	virtual void Update(float _deltaTime) override;

	void OnNavLeftClicked(class USPAButtonWidget* _button);
	void OnNavRightClicked(class USPAButtonWidget* _button);
protected:
	class ASPAGarageActor* Garage;

	FVector SwipeDelta;

	//UI
	class USPAGarageWidget* GarageWidget;
};

