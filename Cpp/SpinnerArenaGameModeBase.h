// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "SpinnerArenaGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SPINNERARENA_API ASpinnerArenaGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ASpinnerArenaGameModeBase(const FObjectInitializer& _oi);
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason);
	virtual void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;
	
	void SetGameState(ESPAGameState _newGameState);

	void EnterMatchState(const FSPAEnemyData& _enemyData, const FSPASpinnerData& _playerSpinnerData);
	//test
	void JoinRandomMatch();

	void GenerateNewWorld();
	void SaveWorld();
	void LoadWorld();

	FSPAEnemyData CreateRandomEnemyData();

	class ASPAEffects* GetEffects();

	class ASPAPlayerController* GetSPAPlayerController();
	class ASPAHUD* GetSPAHUD();
	class USPAGameInstance* GetSPAGameInstance();
	class USPAGameSettings* GetSPAGameSettings();
protected:
	ESPAGameState CurrentGameState;
	ESPAGameState PreviousGameState;

	class FSPAGameStateBase* GameStates[(uint8)ESPAGameState::MAX];

	//random
	int32 Seed;
	FRandomStream RandomStream;

	//
	UPROPERTY()
	class ASPAEffects* Effects;
};
