// Fill out your copyright notice in the Description page of Project Settings.

#include "SpinnerArena.h"
#include "SPADamageBarWidget.h"
#include "Components/TextBlock.h"
#include "Components/CanvasPanelSlot.h"
#include "WidgetLayoutLibrary.h"

USPADamageBarWidget::USPADamageBarWidget(const FObjectInitializer& _oi) : Super(_oi)
{
	DeltaAcc = 0;
	MaxTime = 1.3f;
}

void USPADamageBarWidget::RegisterWidgets(class UTextBlock* _damageTextBlock)
{
	DamageTextBlock = _damageTextBlock;
}

void USPADamageBarWidget::InitWidget(const FVector& _position, const FVector& _direction, float _power, const FLinearColor& _color)
{
	CurrentPosition = Position = _position;
	Direction = _direction;
	Power = _power;

	Color = _color;

	DamageTextBlock->SetText(FText::FromString(FString::FromInt((int32)_power)));
	DamageTextBlock->SetColorAndOpacity(_color);
}

void USPADamageBarWidget::UpdateWidget(float _deltaTime)
{
	CurrentPosition = FMath::InterpEaseOut(CurrentPosition, Position + (Direction * 1000), DeltaAcc / MaxTime, 2.0f);

	float alpha = FMath::InterpEaseOut(1.0f, 0.0f, (DeltaAcc - 0.5f) / (MaxTime - 0.5f), 2.0f);

	UCanvasPanelSlot* canvasPanelSlot = Cast<UCanvasPanelSlot>(this->Slot);
	if (canvasPanelSlot)
	{
		ASPAPlayerController* pc = GetSPAPlayerController();
		if (pc)
		{
			FVector2D screenPosition;
			UWidgetLayoutLibrary::ProjectWorldLocationToWidgetPosition(pc, CurrentPosition, screenPosition);
			canvasPanelSlot->SetPosition(screenPosition);

			FLinearColor currentColor = this->ColorAndOpacity;
			currentColor.A = alpha;
			this->SetColorAndOpacity(currentColor);
		}
	}

	DeltaAcc += _deltaTime;
	if (DeltaAcc >= MaxTime)
	{
		RemoveFromParent();
	}
}

void USPADamageBarWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	UpdateWidget(InDeltaTime);
}