// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UI/Widgets/PANBaseWidget.h"
#include "PANResourceWidget.generated.h"

/**
 * 
 */
UCLASS()
class PANDEMIA_API UPANResourceWidget : public UPANBaseWidget
{
	GENERATED_BODY()
	
public:
	UPANResourceWidget(const FObjectInitializer& _oi);
	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	
	UFUNCTION(BlueprintCallable, Category = UIResource)
	void RegisterWidgets(UImage* _resourceIcon, UImage* _upState, UImage* _downState);

	void Update(const FPANResourceGameplayData& _resourceGameplayData);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = UIResource)
	bool GetResourceUIData(FPANResourceUIData& _resourceUIData);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EPANResourceType ResourceType;
	
private:
	UImage* ResourceIcon;
	UImage* UpState;
	UImage* DownState;

	UMaterialInstanceDynamic* ResourceMaterial;
};
