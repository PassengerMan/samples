// Fill out your copyright notice in the Description page of Project Settings.

#include "SpinnerArena.h"
#include "SPASpinnerInfoWidget.h"
#include "UI/Widgets/Elements/SPASpinnerAttributesWidget.h"
#include "UI/Widgets/Elements/SPASpinnerSkillsWidget.h"
#include "Runtime/UMG/Public/Components/Image.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"

USPASpinnerInfoWidget::USPASpinnerInfoWidget(const FObjectInitializer& _oi) : Super(_oi)
{

}

void USPASpinnerInfoWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void USPASpinnerInfoWidget::RegisterWidgets(
	class USPASpinnerAttributesWidget* _speedAttribute, class USPASpinnerAttributesWidget* _weightAttribute, class USPASpinnerAttributesWidget* _hullAttribute,
	class UTextBlock* _levelTextBlock, class UTextBlock* _expTextBlock,
	class UTextBlock* _speedAttributeTextBlock, class UTextBlock* _weightAttributeTextBlock, class UTextBlock* _hullAttributeTextBlock,
	class UTextBlock* _spinnerTypeTextBlock, class UImage* _spinnerTypeImage, class UImage* _expBar, class UImage* _hpBar,
	class USPASpinnerSkillsWidget* _skillsWidget,
	class UImage* _backgroundImage)
{
	SpeedAttribute = _speedAttribute;
	WeightAttribute = _weightAttribute;
	HullAttribute = _hullAttribute;

	LevelTextBlock = _levelTextBlock;
	ExpTextBlock = _expTextBlock;
	SpeedAttributeTextBlock = _speedAttributeTextBlock;
	WeightAttributeTextBlock = _weightAttributeTextBlock;
	HullAttributeTextBlock = _hullAttributeTextBlock;
	SpinnerTypeTextBlock = _spinnerTypeTextBlock;
	SpinnerTypeImage = _spinnerTypeImage;
	ExpBar = _expBar;
	HpBar = _hpBar;

	SkillsWidget = _skillsWidget;

	BackgroundImage = _backgroundImage;
}

void USPASpinnerInfoWidget::SetSpinnerData(ESPASpinnerType _spinnerType, const FSPASpinnerAttributes& _spinnerAttributes, const FSPASpinnerSkills& _spinnerSkills)
{
	SpinnerType = _spinnerType;
	SpinnerAttributes = _spinnerAttributes;
	SpinnerSkills = _spinnerSkills;

	SpeedAttribute->SetAttributeData(ESPASpinnerAttributeType::Speed, SpinnerAttributes.GetCurrentAttributeValue(ESPASpinnerAttributeType::Speed), SpinnerAttributes.GetAttributeValue(ESPASpinnerAttributeType::Speed, false));
	WeightAttribute->SetAttributeData(ESPASpinnerAttributeType::Weight, SpinnerAttributes.GetCurrentAttributeValue(ESPASpinnerAttributeType::Weight), SpinnerAttributes.GetAttributeValue(ESPASpinnerAttributeType::Weight, false));
	HullAttribute->SetAttributeData(ESPASpinnerAttributeType::Hull, SpinnerAttributes.GetCurrentAttributeValue(ESPASpinnerAttributeType::Hull), SpinnerAttributes.GetAttributeValue(ESPASpinnerAttributeType::Hull, false));

	SkillsWidget->SetSkillsData(_spinnerSkills);

	LevelTextBlock->SetText(FText::FromString(FString::Printf(TEXT("%d"), SpinnerAttributes.Level)));
	ExpTextBlock->SetText(FText::FromString(FString::Printf(TEXT("%d/%d"), SpinnerAttributes.Exp, (SpinnerAttributes.Level+1)*1000)));

	UMaterialInstanceDynamic* expMaterial = ExpBar->GetDynamicMaterial();
	expMaterial->SetScalarParameterValue(TEXT("Percent"), (float)SpinnerAttributes.Exp / ((SpinnerAttributes.Level + 1) * 1000));

	USPAGameSettings* gameSettings = GetSPAGameSettings();
	FSPAUITextIconData* spinnerTypes = gameSettings->UIData.SpinnerTypes;
	FSPAUITextIconData* spinnerAttributeTypes = gameSettings->UIData.SpinnerAttributeTypes;

	SpeedAttributeTextBlock->SetText(spinnerAttributeTypes[(uint8)ESPASpinnerAttributeType::Speed].Text);
	WeightAttributeTextBlock->SetText(spinnerAttributeTypes[(uint8)ESPASpinnerAttributeType::Weight].Text);
	HullAttributeTextBlock->SetText(spinnerAttributeTypes[(uint8)ESPASpinnerAttributeType::Hull].Text);

	SpinnerTypeTextBlock->SetText(spinnerTypes[(uint8)SpinnerType].Text);
	SpinnerTypeImage->SetBrushFromTexture(spinnerTypes[(uint8)SpinnerType].Icon, false);

	UMaterialInstanceDynamic* hpMaterial = HpBar->GetDynamicMaterial();
	hpMaterial->SetScalarParameterValue(TEXT("Percent"), (float)_spinnerAttributes.Hp / 100.0f);
}

void USPASpinnerInfoWidget::SetSpinnerVisualData(const FLinearColor& _spinnerColor)
{
	FLinearColor modifiedColor = _spinnerColor*0.15f;
	modifiedColor.A = 0.90f;
	BackgroundImage->SetColorAndOpacity(modifiedColor);

	SkillsWidget->SetBackgroundColor(modifiedColor);
}