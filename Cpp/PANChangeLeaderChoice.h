// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Gameplay/Cards/Choices/PANBaseChoice.h"
#include "PANChangeLeaderChoice.generated.h"

/**
 * 
 */
UCLASS()
class PANDEMIA_API UPANChangeLeaderChoice : public UPANBaseChoice
{
	GENERATED_BODY()

public:
	UPANChangeLeaderChoice(const FObjectInitializer& _oi);

	virtual void Init(const FPANGameData& _data) override;
	virtual FPANGameData& Visualize(FPANGameData& _data) override;
	virtual FPANGameData& Execute(FPANGameData& _data) override;
	
	virtual const FText& GetCustomChoiceText() override;
};
