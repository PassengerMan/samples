// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "PANGameplayTypes.generated.h"

UENUM(BlueprintType)
enum class EPANColor : uint8
{
	White,
	Black,
	Red,
	Green,

	Background,
	MediumBackground,
	LightBackground,

	Border,

	MAX
};

//VIRUS
UENUM(BlueprintType)
enum class EPANVirusTransmission : uint8
{
	Air,
	Contact,
	Droplets,
	Blood,

	HomeAnimals,
	WildAnimals,

	MAX
};

UENUM(BlueprintType)
enum class EPANResourceType : uint8
{
	Finance,
	Population,
	Research,
	MAX
};

USTRUCT(BlueprintType)
struct FPANResourceUIData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText ResourceName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* ResourceIcon;

};

USTRUCT(BlueprintType)
struct FPANResourceGameplayData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Percent;

	FPANResourceGameplayData()
	{
		Percent = 0.0f;
	}

	FPANResourceGameplayData& operator =(const FPANResourceGameplayData& _resourcesGameplayData)
	{
		SetResource(_resourcesGameplayData.Percent);

		return (*this);
	}

	void SetResource(float _percent)
	{
		Percent = _percent;
	}

	void AddResource(float _percentDelta)
	{
		Percent = FMath::Clamp(Percent + _percentDelta, 0.0f, 1.0f);
	}

	void SetResource(const FPANResourceGameplayData& _resource)
	{
		SetResource(_resource.Percent);
	}

	void AddResource(const FPANResourceGameplayData& _resourceDelta)
	{
		AddResource(_resourceDelta.Percent);
	}
};

USTRUCT(BlueprintType)
struct FPANResourcesGameplayData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FPANResourceGameplayData Resources[EPANResourceType::MAX];

	FPANResourcesGameplayData& operator =(const FPANResourcesGameplayData& _resourcesGameplayData)
	{
		for (uint8 i = 0; i < (uint8)EPANResourceType::MAX; i++)
		{
			Resources[i] = _resourcesGameplayData.Resources[i];
		}

		return (*this);
	}

	void SetResource(const FPANResourceGameplayData& _resource, EPANResourceType _resourceType)
	{
		Resources[(uint8)_resourceType].SetResource(_resource);
	}

	void AddResource(const FPANResourceGameplayData& _resourceDelta, EPANResourceType _resourceType)
	{
		Resources[(uint8)_resourceType].AddResource(_resourceDelta);
	}

	void SetResources(FPANResourceGameplayData* _resources)
	{
		for (uint8 i = 0; i < (uint8)EPANResourceType::MAX; i++)
		{
			SetResource(_resources[i], (EPANResourceType)i);
		}
	}

	void AddResources(FPANResourceGameplayData* _resourcesDelta)
	{
		for (uint8 i = 0; i < (uint8)EPANResourceType::MAX; i++)
		{
			AddResource(_resourcesDelta[i], (EPANResourceType)i);
		}
	}
};

USTRUCT(BlueprintType)
struct FPANVirusUIData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	TArray<FText> VirusNames;

	UPROPERTY(EditAnywhere)
	FText TransmissionNames[EPANVirusTransmission::MAX];
};

USTRUCT(BlueprintType)
struct FPANVirusGameplayData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MutationChance;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float SpreadChance;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<EPANVirusTransmission> TransmissionTypes;

	FPANVirusGameplayData()
	{
		Name = FText::FromString("Deadly Virus");
		MutationChance = 0.2f;

		uint8 numOfTransmissions = (uint8)EPANVirusTransmission::MAX - 1;
		uint8 startingTransmission = FMath::RandRange(0, numOfTransmissions);
		AddTransmissionType((EPANVirusTransmission)startingTransmission);
	}

	FPANVirusGameplayData& operator =(const FPANVirusGameplayData& _virusGameplayData)
	{
		Name = _virusGameplayData.Name;
		MutationChance = _virusGameplayData.MutationChance;
		SpreadChance = _virusGameplayData.SpreadChance;
		TransmissionTypes = _virusGameplayData.TransmissionTypes;

		return (*this);
	}

	void SetVirusName(const FText& _virusName)
	{
		Name = _virusName;
	}

	void SetMutationChance(float _mutationChance)
	{
		MutationChance = _mutationChance;
	}

	void UpdateSpreadChance()
	{
		SpreadChance = (float)TransmissionTypes.Num() / ((uint8)EPANVirusTransmission::MAX - 1);
	}

	void SetTransmissionTypes(const TArray<EPANVirusTransmission>& _transmissionTypes)
	{
		TransmissionTypes = _transmissionTypes;
	}

	void AddMutationChance(float _mutationChanceDelta)
	{
		MutationChance = FMath::Clamp(MutationChance + _mutationChanceDelta, 0.0f, 1.0f);
	}

	void AddTransmissionType(EPANVirusTransmission _newTransmissionType)
	{
		TransmissionTypes.AddUnique(_newTransmissionType);
		UpdateSpreadChance();
	}

	void TryToMutate()
	{
		float randomFloat = FMath::RandRange(0.0f, 1.0f);
		if (randomFloat < MutationChance)
		{
			TArray<EPANVirusTransmission> TransmissionsBag;
			for (uint8 i = 0; i < (uint8)EPANVirusTransmission::MAX; i++)
			{
				if (TransmissionTypes.Contains((EPANVirusTransmission)i))
					continue;

				TransmissionsBag.Add((EPANVirusTransmission)i);
			}

			if (TransmissionsBag.Num() > 0)
			{
				int32 randomTransmission = FMath::RandRange(0, TransmissionsBag.Num() - 1);
				AddTransmissionType(TransmissionsBag[randomTransmission]);
			}
		}
	}
};

UENUM(BlueprintType)
enum class EPANContinentType : uint8
{
	NorthAmerica,
	SouthAmerica,
	Africa,
	Europe,
	Asia,
	Australia,
	Antarctica,
	MAX
};

USTRUCT(BlueprintType)
struct FPANContinentUIData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText ContinentName;
};

USTRUCT(BlueprintType)
struct FPANContinentGameplayData
{
	GENERATED_USTRUCT_BODY()

	//dynamic
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float InfectionPercent;

	//constants
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Population;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float StateOfEmergency;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float ClosedBorders;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<EPANContinentType> NeighbourContinents;

	//calculated
	float InfectionRatePerTurn;
	float InfectionBorderSpreadChance;

	//states
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bStateOfEmergency;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bClosedBorders;

	FPANContinentGameplayData()
	{
		InfectionPercent = 0.0f;

		//different per continent
		Population = 0.5f;
		StateOfEmergency = 0.5f;
		ClosedBorders = 0.5f;

		InfectionRatePerTurn = 0.0f;
		InfectionBorderSpreadChance = 0.0f;

		bStateOfEmergency = false;
		bClosedBorders = false;

		NeighbourContinents.Reset();
	}

	FPANContinentGameplayData& operator =(const FPANContinentGameplayData& _continentGameplayData)
	{
		InfectionPercent = _continentGameplayData.InfectionPercent;

		Population = _continentGameplayData.Population;
		StateOfEmergency = _continentGameplayData.StateOfEmergency;
		ClosedBorders = _continentGameplayData.ClosedBorders;

		InfectionRatePerTurn = _continentGameplayData.InfectionRatePerTurn;
		InfectionBorderSpreadChance = _continentGameplayData.InfectionBorderSpreadChance;

		bStateOfEmergency = _continentGameplayData.bStateOfEmergency;
		bClosedBorders = _continentGameplayData.bClosedBorders;

		NeighbourContinents = _continentGameplayData.NeighbourContinents;

		return (*this);
	}

	void SetInfectionPercent(float _infectionPercent)
	{
		InfectionPercent = _infectionPercent;
	}

	void SetInfectionRatePerTurn(float _infectionRatePerTurn)
	{
		InfectionRatePerTurn = _infectionRatePerTurn;
	}

	void SetInfectionBorderSpreadChance(float _infectionBorderSpreadChance)
	{
		InfectionBorderSpreadChance = _infectionBorderSpreadChance;
	}

	void SetStateOfEmergency(bool _bStateOfEmergency)
	{
		bStateOfEmergency = _bStateOfEmergency;
	}

	void SetClosedBorders(bool _bClosedBorders)
	{
		bClosedBorders = _bClosedBorders;
	}

	void AddInfectionPercent(float _infectionPercentDelta)
	{
		InfectionPercent = FMath::Clamp(InfectionPercent + _infectionPercentDelta, 0.0f, 1.0f);
	}

	void SetData(const FPANContinentGameplayData& _continentData)
	{
		Population = _continentData.Population;
		StateOfEmergency = _continentData.StateOfEmergency;
		ClosedBorders = _continentData.ClosedBorders;

		SetInfectionPercent(_continentData.InfectionPercent);
		SetInfectionRatePerTurn(_continentData.InfectionRatePerTurn);
		SetInfectionBorderSpreadChance(_continentData.InfectionBorderSpreadChance);
	}

	//InfectionRatePerTurn = 1.5f * Population * InfectionPercent * (1.0f - StateOfEmergency)
	//InfectionBorderSpreadChance = 1.5f * Population * InfectionPercent * (1.0f - StateOfEmergency) * (1.0f - ClosedBorders);
	void NextTurn(const FPANVirusGameplayData& _virusData, const FPANResourcesGameplayData& _resourcesData, bool& _infectNewNeighbour)
	{
		_infectNewNeighbour = false;

		float stateOfEmergency = bStateOfEmergency ? StateOfEmergency : 0.0f;
		float closedBorders = bClosedBorders ? ClosedBorders : 0.0f;

		InfectionRatePerTurn = 1.5f * Population * InfectionPercent * (1.0f - stateOfEmergency) * _virusData.SpreadChance;
		InfectionBorderSpreadChance = Population * InfectionPercent * (1.0f - stateOfEmergency) * (1.0f - closedBorders) * _virusData.SpreadChance; //@TODO: Add neighbours closedBorders status

		float resourcesRatio = 
			(_resourcesData.Resources[(uint8)EPANResourceType::Population].Percent*1.0f +
			 _resourcesData.Resources[(uint8)EPANResourceType::Research].Percent*1.0f) / 2.0f;

		InfectionRatePerTurn *= (2.0f - resourcesRatio);
		InfectionBorderSpreadChance *= (2.0f - resourcesRatio);

		float randomFloat = FMath::RandRange(0.0f, 1.0f);
		if (randomFloat < InfectionBorderSpreadChance)
			_infectNewNeighbour = true;

		AddInfectionPercent(InfectionRatePerTurn);
	}
};

USTRUCT(BlueprintType)
struct FPANContinentsGameplayData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FPANContinentGameplayData Continents[EPANContinentType::MAX];


	FPANContinentsGameplayData& operator =(const FPANContinentsGameplayData& _continentsGameplayData)
	{
		for (uint8 i = 0; i < (uint8)EPANContinentType::MAX; i++)
		{
			Continents[i] = _continentsGameplayData.Continents[i];
		}
		
		return (*this);
	}

	void NextTurn(const FPANVirusGameplayData& _virusData, const FPANResourcesGameplayData& _resourcesData)
	{
		for (uint8 i = 0; i < (uint8)EPANContinentType::MAX; i++)
		{
			bool infectNewNeighbour = false;
			Continents[i].NextTurn(_virusData, _resourcesData, infectNewNeighbour);

			if (infectNewNeighbour)
			{
				TArray<EPANContinentType>& neighbourContinents = Continents[i].NeighbourContinents;
				for (int32 j = 0; j < neighbourContinents.Num(); j++)
				{
					bool bSpreadVirus = false;
					if (Continents[(uint8)neighbourContinents[j]].bClosedBorders == true)
					{
						float spreadChance = 1.0f - (Continents[(uint8)neighbourContinents[j]].ClosedBorders / 2.0f);
						float randomFloat = FMath::RandRange(0.0f, 1.0f);
						if (randomFloat < spreadChance)
						{
							bSpreadVirus = true;
						}
					}

					if (Continents[(uint8)neighbourContinents[j]].InfectionPercent <= 0.0f && bSpreadVirus == true)
					{
						Continents[(uint8)neighbourContinents[j]].SetInfectionPercent(0.01f);
						break;
					}
				}
			}
		}
	}

};

USTRUCT(BlueprintType)
struct FPANLeadersUIData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	TArray<UTexture2D*> LeaderImages;

	UPROPERTY(EditAnywhere)
	TArray<FText> LeaderNames;
};

USTRUCT(BlueprintType)
struct FPANLeaderGameplayData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FPANResourcesGameplayData ResourcesMultiplier;

	UPROPERTY(BlueprintReadOnly)
	UTexture2D* LeaderImage;

	UPROPERTY(BlueprintReadOnly)
	FText LeaderName;
};

USTRUCT(BlueprintType)
struct FPANGameData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FPANVirusGameplayData VirusGameplayData;

	UPROPERTY(EditAnywhere)
	FPANContinentsGameplayData ContinentsGameplayData;

	UPROPERTY(EditAnywhere)
	FPANResourcesGameplayData ResourcesGameplayData;

	UPROPERTY(EditAnywhere)
	FPANLeaderGameplayData LeaderGameplayData;

	int32 NumberOfTurns;

	bool bVisualization;

	FPANGameData()
	{
		NumberOfTurns = 0;
		bVisualization = false;
	}

	FPANGameData& operator =(const FPANGameData& _gameData)
	{
		VirusGameplayData = _gameData.VirusGameplayData;
		ContinentsGameplayData = _gameData.ContinentsGameplayData;
		ResourcesGameplayData = _gameData.ResourcesGameplayData;
		LeaderGameplayData = _gameData.LeaderGameplayData;

		NumberOfTurns = _gameData.NumberOfTurns;
		bVisualization = _gameData.bVisualization;

		return (*this);
	}

	void NextTurn()
	{
		VirusGameplayData.TryToMutate();
		ContinentsGameplayData.NextTurn(VirusGameplayData, ResourcesGameplayData);

		NumberOfTurns += 1;

		//UE_LOG(GameLog, Warning, TEXT("Turns: %d"), NumberOfTurns);
	}

	//GameData Analyze Functions
	EPANResourceType GetLowestResource()
	{
		EPANResourceType minResource = EPANResourceType::MAX;
		float min = ResourcesGameplayData.Resources[(uint8)EPANResourceType::Finance].Percent;
		for (uint8 i = 0; i < (uint8)EPANResourceType::MAX; i++)
		{
			float temp = ResourcesGameplayData.Resources[i].Percent;
			if (temp <= min)
			{
				min = temp;
				minResource = (EPANResourceType)i;
			}
		}
		return minResource;
	}

	EPANResourceType GetHighestResource()
	{
		EPANResourceType maxResource = EPANResourceType::MAX;
		float max = ResourcesGameplayData.Resources[(uint8)EPANResourceType::Finance].Percent;
		for (uint8 i = 0; i < (uint8)EPANResourceType::MAX; i++)
		{
			float temp = ResourcesGameplayData.Resources[i].Percent;
			if (temp >= max)
			{
				max = temp;
				maxResource = (EPANResourceType)i;
			}
		}
		return maxResource;
	}

	void GetContinentNeighbours(EPANContinentType _continent, TArray<EPANContinentType>& _neighbours)
	{
		_neighbours.Reset();
		_neighbours = ContinentsGameplayData.Continents[(uint8)_continent].NeighbourContinents;
	}

	void GetRandomInfectedContinents(int32 _maxNoOfContinents, TArray<EPANContinentType>& _infectedContinents)
	{
		_infectedContinents.Reset();
		
		TArray<EPANContinentType> infectedContinents;
		for (uint8 i = 0; i < (uint8)EPANContinentType::MAX; i++)
		{
			float infection = ContinentsGameplayData.Continents[i].InfectionPercent;
			if (infection > 0)
			{
				infectedContinents.Add((EPANContinentType)i);
			}
		}

		for (int32 i = 0; i < FMath::Min((int32)EPANContinentType::MAX, _maxNoOfContinents); i++)
		{
			int32 randContinentIdx = FMath::RandRange(0, infectedContinents.Num());
			_infectedContinents.Add(infectedContinents[randContinentIdx]);
			infectedContinents.RemoveAt(randContinentIdx);
		}
	}

	void GetRandomNeighbours(int32 _maxNoOfNeighbours, bool _bClosedBorders, bool _bStateOfEmergency, EPANContinentType _sourceContinent, TArray<EPANContinentType>& _neighbourContinents)
	{
		_neighbourContinents.Reset();

		TArray<EPANContinentType> filteredNeighbours;
		TArray<EPANContinentType> neighbours = ContinentsGameplayData.Continents[(uint8)_sourceContinent].NeighbourContinents;
		for (int32 i = 0; i < neighbours.Num(); i++)
		{
			bool bClosedBorders = ContinentsGameplayData.Continents[(uint8)neighbours[i]].bClosedBorders;
			bool bStateOfEmergency = ContinentsGameplayData.Continents[(uint8)neighbours[i]].bStateOfEmergency;

			if (bClosedBorders == _bClosedBorders || bStateOfEmergency == _bStateOfEmergency)
			{
				filteredNeighbours.Add(neighbours[i]);
			}
		}

		for (int32 i = 0; i < FMath::Min((int32)EPANContinentType::MAX, _maxNoOfNeighbours); i++)
		{
			int32 randNeighbourIdx = FMath::RandRange(0, filteredNeighbours.Num());
			_neighbourContinents.Add(filteredNeighbours[randNeighbourIdx]);
			filteredNeighbours.RemoveAt(randNeighbourIdx);
		}
	}
};

UENUM(BlueprintType)
enum class EPANCardType
{
	Start,
	ChangeLeader, //resource bonuses, +5% research, +5% money etc
	ChangeCountryStatus, //state of emergency, closed borders, cost
	VirusResearch,
	VaccineDelivery, //amount, countries, cost
	RandomEvent, //modify resources, other unique gameplay things
	CountryStatus,
	VirusStatus, //virus got spread, etc
	MAX
};

UENUM(BlueprintType)
enum class EPANChoiceAction
{
	NoAction,
	FinanceAdd,
	FinanceSub,
	MoodAdd,
	MoodSub,
	ResearchAdd,
	ResearchSub,
	StateOfEmergencyAdd,
	StateOfEmergencySub,
	BordersClose,
	BordersOpen,
	NeighbourBordersClose,
	NeighbourBordersOpen,
	ChangeLeader,
	DeliverVaccine,
	ResearchVirus,
	PostponeGood,
	PostponeBad,
	Cancel,

	MAX
};

UENUM(BlueprintType)
enum class EPANPostponeDecision
{
	Default,
	Good,
	Bad,

	MAX
};

USTRUCT(BlueprintType)
struct FPANCardData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	bool bTurn;

	UPROPERTY(EditAnywhere)
	FText Description;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UPANBaseCard> CardTemplate;

	UPROPERTY(EditAnywhere)
	FText ChoiceLeftText;

	UPROPERTY(EditAnywhere)
	TArray<TEnumAsByte<EPANChoiceAction>> ChoiceLeftActions;

	UPROPERTY(EditAnywhere)
	FText ChoiceRightText;

	UPROPERTY(EditAnywhere)
	TArray<TEnumAsByte<EPANChoiceAction>> ChoiceRightActions;

	FPANCardData()
	{
		bTurn = true;
		CardTemplate = NULL;
	}

	friend FArchive& operator<<(FArchive& Ar, FPANCardData& _cardData)
	{
		return Ar << _cardData.bTurn << _cardData.Description << _cardData.CardTemplate << _cardData.ChoiceLeftText << _cardData.ChoiceLeftActions << _cardData.ChoiceRightText << _cardData.ChoiceRightActions;
	}

	bool Serialize(FArchive& Ar)
	{
		Ar << *this;
		return true;
	}
};

class UCardSet;

USTRUCT(BlueprintType)
struct FPANCardsSet
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	UCardSet* CardSet;
};

USTRUCT(BlueprintType)
struct FPANCardsData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	TArray<FPANCardsSet> Sets;
};

/*Card Struct
- Type
- Description(formattable)
- ChoiceRightText(formattable)
- ChoiceLeftText(formattable)
- ChoiceRightCppClass
- ChoiceLeftCppClass*/