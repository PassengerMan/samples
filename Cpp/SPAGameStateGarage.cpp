#include "SpinnerArena.h"
#include "SPAGameStateGarage.h"
#include "Player/SPAPlayerController.h"
#include "Gameplay/SPAArenaActor.h"
#include "Gameplay/SPASpinnerActor.h"
#include "Gameplay/SPAGarageActor.h"

//UI
#include "UI/Widgets/SPAGameStateMenuWidget.h"
#include "UI/Widgets/SPAGarageWidget.h"

void FSPAGameStateGarage::InitBase()
{
	FSPAGameStateBase::InitBase();

	SwipeDelta.Set(0, 0, 0);
	Garage = NULL;
}

void FSPAGameStateGarage::Enter()
{
	FSPAGameStateBase::Enter();

	USPAGameSettings* gameSettings = GameMode->GetSPAGameSettings();

	const FSPAGameVisualConfigData& visualGameData = gameSettings->GameVisualConfigData;
	FSPAGameData& gameData = gameSettings->GameData;

	FSPAPlayerData& playerData = gameData.PlayerData;

	Garage = GetWorld()->SpawnActor<ASPAGarageActor>(
		visualGameData.GarageActorTemplate->GetDefaultObject<ASPAGarageActor>()->GetClass(),
		FTransform(FRotator::ZeroRotator, FVector(0, 0, 0))
		);

	Garage->AttachCamera(CameraActor);

	TArray<FSPASpinnerData> spinnerDatas = playerData.Spinners;
	for (int32 i = 0; i < spinnerDatas.Num(); i++)
	{
		ASPASpinnerActor* spinner = GetWorld()->SpawnActor<ASPASpinnerActor>(
			visualGameData.SpinnerActorTemplate->GetDefaultObject<ASPASpinnerActor>()->GetClass(),
			FTransform(FRotator::ZeroRotator, FVector::ZeroVector)
			);

		spinner->LoadConfig(spinnerDatas[i]);

		Garage->RegisterSpinner(spinner);
	}

	Garage->PositionSpinners();

	USPAGameStateMenuWidget* gameStateMenuWidget = GameMode->GetSPAGameInstance()->GetGameStateMenuWidget();
	gameStateMenuWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);

	GarageWidget = GameMode->GetSPAGameInstance()->GetGarageWidget();
	GarageWidget->GetNavLeftButton()->OnButtonClickedDelegate.AddRaw(this, &FSPAGameStateGarage::OnNavLeftClicked);
	GarageWidget->GetNavRightButton()->OnButtonClickedDelegate.AddRaw(this, &FSPAGameStateGarage::OnNavRightClicked);
	GarageWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);

	GarageWidget->UpdateSpinnerInfo(Garage->GetSelectedSpinner()->SpinnerData.Type, Garage->GetSelectedSpinner()->SpinnerData.Attributes, Garage->GetSelectedSpinner()->SpinnerData.Skills);
}

void FSPAGameStateGarage::Exit()
{
	FSPAGameStateBase::Exit();

	if (Garage)
		Garage->Destroy();

	Garage = NULL;

	USPAGameStateMenuWidget* gameWidget = GameMode->GetSPAGameInstance()->GetGameStateMenuWidget();
	gameWidget->SetVisibility(ESlateVisibility::Hidden);

	GarageWidget->SetVisibility(ESlateVisibility::Hidden);
	GarageWidget->GetNavLeftButton()->OnButtonClickedDelegate.RemoveAll(this);
	GarageWidget->GetNavRightButton()->OnButtonClickedDelegate.RemoveAll(this);
}

void FSPAGameStateGarage::Update(float _deltaTime)
{
	ASPAPlayerController* pc = GameMode->GetSPAPlayerController();

	if (pc->bMousePressed)
	{
		float mouseX = 0, mouseY = 0;
		pc->GetMousePosition(mouseX, mouseY);
		FVector swipeDelta = FVector(mouseX, mouseY, 0) - SwipeDelta;
		
		if (Garage)
		{
			if(FVector(mouseX, mouseY, 0).Size2D() > 0 && swipeDelta.Size2D() > 0 && SwipeDelta.Size2D() > 0)
				Garage->RotateSpinner(swipeDelta*1.5f);
		}

		SwipeDelta.Set(mouseX, mouseY, 0);
	}
	else
	{
		/*float mouseX = 0, mouseY = 0;
		pc->GetMousePosition(mouseX, mouseY);
		FVector swipeDelta = FVector(mouseX, mouseY, 0) - SwipeDelta;

		UE_LOG(GameLog, Warning, TEXT("%s"), *swipeDelta.ToString());

		if (FMath::Abs(swipeDelta.X) > 1 && !SwipeDelta.IsNearlyZero())
		{
			int32 dir = FMath::Sign(swipeDelta.X);
			if (dir == 1)
			{
				Garage->ShowNextSpinner();
			}
			else if (dir == -1)
			{
				Garage->ShowPreviousSpinner();
			}
		}*/

		SwipeDelta.Set(0.0f, 0.0f, 0.0f);
	}
}

void FSPAGameStateGarage::OnNavLeftClicked(class USPAButtonWidget* _button)
{
	Garage->ShowPreviousSpinner();

	GarageWidget->UpdateSpinnerInfo(Garage->GetSelectedSpinner()->SpinnerData.Type, Garage->GetSelectedSpinner()->SpinnerData.Attributes, Garage->GetSelectedSpinner()->SpinnerData.Skills);
}

void FSPAGameStateGarage::OnNavRightClicked(class USPAButtonWidget* _button)
{
	Garage->ShowNextSpinner();

	GarageWidget->UpdateSpinnerInfo(Garage->GetSelectedSpinner()->SpinnerData.Type, Garage->GetSelectedSpinner()->SpinnerData.Attributes, Garage->GetSelectedSpinner()->SpinnerData.Skills);
}