#include "SpinnerArena.h"
#include "SPAGameStateMatch.h"
#include "Player/SPAPlayerController.h"
#include "SPAGameInstance.h"
#include "SPAGameSettings.h"
#include "UI/SPAHUD.h"
#include "Gameplay/SPAArenaActor.h"
#include "Gameplay/SPASpinnerActor.h"

//UI
#include "UI/Widgets/SPAGameWidget.h"
#include "UI/Widgets/Elements/SPAMatchWidget.h"
#include "UI/Widgets/SPAGameStateMenuWidget.h"

void FSPAGameStateMatch::InitBase()
{
	Arena = NULL;
	PlayerSpinner = NULL;

	bStartedSwiping = false;
	bStartedDashing = false;

	MatchGameplayData.PreviousMatchState = ESPAMatchState::MAX;
	MatchGameplayData.MatchState = ESPAMatchState::JoiningMatch;

	NextMatchState = ESPAMatchState::JoiningMatch;
	MatchStateDelay = 0.0f;
	MatchStateDelaySumDelta = 0.0f;

	bSpinnerStopped = false;
}

void FSPAGameStateMatch::Enter()
{
	FSPAGameStateBase::Enter();

	MatchGameplayData.PreviousMatchState = ESPAMatchState::MAX;
	MatchGameplayData.MatchState = ESPAMatchState::JoiningMatch;
	MatchGameplayData.NumberOfRounds = 3;
	MatchGameplayData.CurrentRound = 0;
	MatchGameplayData.PlayerWins = 0;
	MatchGameplayData.EnemyWins = 0;
	MatchGameplayData.PlayerRoundsData.AddDefaulted(MatchGameplayData.NumberOfRounds);
	MatchGameplayData.EnemyRoundsData.AddDefaulted(MatchGameplayData.NumberOfRounds);

	NextMatchState = ESPAMatchState::JoiningMatch;
	MatchStateDelay = 0.0f;
	MatchStateDelaySumDelta = 0.0f;
	bStartedSwiping = false;
	bStartedDashing = false;
	bSpinnerStopped = false;

	//UI
	GameWidget = GameMode->GetSPAGameInstance()->GetGameWidget();
	GameWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);

	USPAGameStateMenuWidget* gameWidget = GameMode->GetSPAGameInstance()->GetGameStateMenuWidget();
	gameWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);

	if (GameWidget)
	{
		GameWidget->SetSpinnerInfoDatas(EnemyData.Spinners[0], PlayerSpinnerData);
	}
}

void FSPAGameStateMatch::InitMatch(const FSPAEnemyData& _enemyData, const FSPASpinnerData& _playerSpinnerData)
{
	EnemyData = _enemyData;
	PlayerSpinnerData = _playerSpinnerData;
}

void FSPAGameStateMatch::Exit()
{
	FSPAGameStateBase::Exit();

	MatchGameplayData.PlayerRoundsData.Reset();
	MatchGameplayData.EnemyRoundsData.Reset();

	if (Arena)
	{
		Arena->Destroy();
		Arena = NULL;
	}	

	if (PlayerSpinner)
	{
		GameWidget->RemoveHPBar(PlayerSpinner);

		PlayerSpinner->Destroy();
		PlayerSpinner = NULL;
	}

	for (int32 i = 0; i < EnemySpinners.Num(); i++)
	{
		GameWidget->RemoveHPBar(EnemySpinners[i]);

		if (EnemySpinners[i])
			EnemySpinners[i]->Destroy();
	}
	EnemySpinners.Reset();

	if (GameWidget)
		GameWidget->SetVisibility(ESlateVisibility::Hidden);

	USPAGameStateMenuWidget* gameWidget = GameMode->GetSPAGameInstance()->GetGameStateMenuWidget();
	gameWidget->SetVisibility(ESlateVisibility::Hidden);
}

void FSPAGameStateMatch::Update(float _deltaTime)
{
	if (GameWidget)
		GameWidget->Update(MatchGameplayData, MatchStateDelaySumDelta, MatchStateDelay);

	bool bCanContinue = UpdateMatchStateWithDelay(_deltaTime);
	if (!bCanContinue)
		return;

	if (MatchGameplayData.MatchState == ESPAMatchState::ThrowingSpinner)
	{
		ASPAPlayerController* pc = GameMode->GetSPAPlayerController();

		float mouseX = 0, mouseY = 0;
		pc->GetMousePosition(mouseX, mouseY);

		if (pc->bMousePressed)
		{
			if (bStartedSwiping == false)
			{
				SwipeStartPosition = FVector(mouseX, mouseY, 0);
				bStartedSwiping = true;
			}
				
		}
		else
		{
			if (bStartedSwiping == true)
			{
				SwipeEndPosition = FVector(mouseX, mouseY, 0);
				bStartedSwiping = false;
				ChangeMatchStateWithDelay(ESPAMatchState::StartingRound, 0.0f);
			}
		}
	}

	if (MatchGameplayData.MatchState == ESPAMatchState::PlayingRound)
	{
		ASPAPlayerController* pc = GameMode->GetSPAPlayerController();

		float mouseX = 0, mouseY = 0;
		pc->GetMousePosition(mouseX, mouseY);

		if (pc->bMousePressed)
		{
			if (bStartedDashing == false)
			{
				FVector dashStartPosition = FVector(mouseX, mouseY, 0);
				FVector mouseWorldDir;
				pc->DeprojectScreenPositionToWorld(dashStartPosition.X, dashStartPosition.Y, dashStartPosition, mouseWorldDir);
				dashStartPosition =
					USPAGameplayHelpers::IntersectRayWithPlane(
						dashStartPosition, mouseWorldDir, FPlane(FVector(-100, 0, 0), FVector(0, 0, 1)));

				FVector dashDirection = (dashStartPosition - PlayerSpinner->GetActorLocation()).GetSafeNormal();
				dashDirection *= 15000.0f;

				PlayerSpinner->Dash(dashDirection);

				bStartedDashing = true;
			}

		}
		else
		{
			if (bStartedDashing == true)
			{
				bStartedDashing = false;
			}
		}
	}

	if (MatchGameplayData.MatchState != MatchGameplayData.PreviousMatchState)
	{
		MatchGameplayData.PreviousMatchState = MatchGameplayData.MatchState;
		OnMatchStateChanged(MatchGameplayData.MatchState);
	}
}

void FSPAGameStateMatch::ChangeMatchStateWithDelay(ESPAMatchState _nextMatchState, float _delay)
{
	MatchStateDelaySumDelta = 0.0f;
	MatchStateDelay = _delay;
	NextMatchState = _nextMatchState;
}

bool FSPAGameStateMatch::UpdateMatchStateWithDelay(float _deltaTime)
{
	if (NextMatchState == ESPAMatchState::MAX || MatchGameplayData.MatchState == NextMatchState)
		return true;

	MatchStateDelaySumDelta += _deltaTime;

	if (MatchStateDelaySumDelta > MatchStateDelay)
	{
		MatchStateDelaySumDelta = 0.0f;
		MatchGameplayData.MatchState = NextMatchState;
	}

	return false;
}

void FSPAGameStateMatch::OnMatchStateChanged(ESPAMatchState _currentMatchState)
{
	/*
		JoiningMatch,
		SelectingSpinner,

		ThrowingSpinner,
		StartingRound,

		PlayingRound,

		EndingRound,

		FinishingMatch,
	*/
	USPAGameSettings* gameSettings = GameMode->GetSPAGameSettings();
	const FSPAGameVisualConfigData& visualGameData = gameSettings->GameVisualConfigData;


	switch (_currentMatchState)
	{
		case ESPAMatchState::JoiningMatch:
		{
			FSPAArenaData& arenaData = EnemyData.Arena;

			//spawn arena
			Arena = GetWorld()->SpawnActor<ASPAArenaActor>(
				visualGameData.ArenaActorTemplate->GetDefaultObject<ASPAArenaActor>()->GetClass(),
				FTransform(FRotator::ZeroRotator, FVector(0, 0, 0))
				);

			Arena->LoadConfig(arenaData);
			Arena->AttachCamera(CameraActor);

			//spawn player spinner
			PlayerSpinner = GetWorld()->SpawnActor<ASPASpinnerActor>(
				visualGameData.SpinnerActorTemplate->GetDefaultObject<ASPASpinnerActor>()->GetClass(),
				FTransform(FRotator::ZeroRotator, FVector::ZeroVector)
				);

			PlayerSpinner->LoadConfig(PlayerSpinnerData);
			PlayerSpinner->OnSpinnerStoppedDelegate.AddRaw(this, &FSPAGameStateMatch::OnSpinnerStopped);
			PlayerSpinner->OnSpinnerHitDelegate.AddRaw(this, &FSPAGameStateMatch::OnSpinnerHit);

			Arena->RegisterSpinner(PlayerSpinner);
			GameWidget->AddHPBar(PlayerSpinner);

			//spawn enemy spinners
			TArray<FSPASpinnerData>& enemySpinnerDatas = EnemyData.Spinners;
			for (int32 i = 0; i < enemySpinnerDatas.Num(); i++)
			{
				ASPASpinnerActor* spinner = GetWorld()->SpawnActor<ASPASpinnerActor>(
					visualGameData.SpinnerActorTemplate->GetDefaultObject<ASPASpinnerActor>()->GetClass(),
					FTransform(FRotator::ZeroRotator, FVector::ZeroVector)
					);

				spinner->LoadConfig(enemySpinnerDatas[i]);
				spinner->OnSpinnerStoppedDelegate.AddRaw(this, &FSPAGameStateMatch::OnSpinnerStopped);
				spinner->OnSpinnerHitDelegate.AddRaw(this, &FSPAGameStateMatch::OnSpinnerHit);

				Arena->RegisterSpinner(spinner);
				GameWidget->AddHPBar(spinner);

				EnemySpinners.Add(spinner);
			}

			//setup arena spinners
			Arena->PositionSpinners();

			SwipeStartPosition = FVector(0, 0, 0);
			SwipeEndPosition = FVector(0, 0, 0);

			ChangeMatchStateWithDelay(ESPAMatchState::ThrowingSpinner, 3.0f);
			break;
		}
		case ESPAMatchState::ThrowingSpinner:
		{

			break;
		}
		case ESPAMatchState::StartingRound:
		{
			FVector mouseWorldDir;
			ASPAPlayerController* pc = GameMode->GetSPAPlayerController();

			pc->DeprojectScreenPositionToWorld(SwipeStartPosition.X, SwipeStartPosition.Y, SwipeStartPosition, mouseWorldDir);
			SwipeStartPosition =
				USPAGameplayHelpers::IntersectRayWithPlane(
					SwipeStartPosition, mouseWorldDir, FPlane(FVector(-100, 0, 0), FVector(0, 0, 1)));

			pc->DeprojectScreenPositionToWorld(SwipeEndPosition.X, SwipeEndPosition.Y, SwipeEndPosition, mouseWorldDir);
			SwipeEndPosition =
				USPAGameplayHelpers::IntersectRayWithPlane(
					SwipeEndPosition, mouseWorldDir, FPlane(FVector(-100, 0, 0), FVector(0, 0, 1)));

			FVector playerSwipeDirection = SwipeEndPosition - SwipeStartPosition;
			float swipeStrength = FMath::Clamp(playerSwipeDirection.Size2D(), 1.0f, 1000.0f) * 10.0f;
			playerSwipeDirection.Normalize();
			playerSwipeDirection *= swipeStrength;

			Arena->StartSimulation(playerSwipeDirection);

			MatchGameplayData.CurrentRound += 1;

			ChangeMatchStateWithDelay(ESPAMatchState::PlayingRound, 0.0f);
			break;
		}
		case ESPAMatchState::PlayingRound:
		{

			break;
		}
		case ESPAMatchState::EndingRound:
		{
			Arena->StopSimulation();
			bSpinnerStopped = false;

			int32 roundWinsLimit = FMath::CeilToInt((float)MatchGameplayData.NumberOfRounds / 2.0f); //3 / 2 = 2 - bo3 //5 / 2 = 3 - bo5
			if (MatchGameplayData.PlayerWins >= roundWinsLimit)
			{
				ChangeMatchStateWithDelay(ESPAMatchState::FinishingMatch, 3.0f);
			}
			else if (MatchGameplayData.EnemyWins >= roundWinsLimit)
			{
				ChangeMatchStateWithDelay(ESPAMatchState::FinishingMatch, 3.0f);
			}
			else
			{
				ChangeMatchStateWithDelay(ESPAMatchState::RestartingRound, 3.0f);
				//MatchGameplayData.MatchState = ESPAMatchState::RestartingRound;
			}
			break;
		}
		case ESPAMatchState::RestartingRound:
		{
			Arena->PositionSpinners();

			SwipeStartPosition = FVector(0, 0, 0);
			SwipeEndPosition = FVector(0, 0, 0);

			ChangeMatchStateWithDelay(ESPAMatchState::ThrowingSpinner, 3.0f);
			break;
		}
		case ESPAMatchState::FinishingMatch:
		{
			GameMode->SetGameState(ESPAGameState::Garage);
			break;
		}
		default:
		{

			break;
		}
	}

	if (GameWidget)
		GameWidget->MatchStateChanged(MatchGameplayData);
}

void FSPAGameStateMatch::OnSpinnerStopped(ASPASpinnerActor* _spinnerActor)
{
	if (bSpinnerStopped || MatchGameplayData.MatchState != ESPAMatchState::PlayingRound)
		return;

	int32 spinnerIndex = -1;
	bool bFound = EnemySpinners.Find(_spinnerActor, spinnerIndex);
	if (!bFound) //enemy spinner
	{
		MatchGameplayData.EnemyWins += 1;
	}
	else if(_spinnerActor != PlayerSpinner) //player spinner
	{
		MatchGameplayData.PlayerWins += 1;
	}

	bSpinnerStopped = true;

	ChangeMatchStateWithDelay(ESPAMatchState::EndingRound, 5.0f);
}

void FSPAGameStateMatch::OnSpinnerHit(ASPASpinnerActor* _spinnerActor, const FVector& _dir, float _power)
{
	if (GameWidget)
	{
		GameWidget->SpawnDamageBar(_spinnerActor->GetActorLocation(), _dir, _power, _spinnerActor->SpinnerData.Visual.SecondaryColor);
	}
}