// Fill out your copyright notice in the Description page of Project Settings.

#include "SpinnerArena.h"
#include "SpinnerArenaGameModeBase.h"
#include "Player/SPAPlayerController.h"
#include "SPAGameInstance.h"
#include "SPAGameSettings.h"
#include "UI/SPAHUD.h"
#include "Gameplay/SPAArenaActor.h"
#include "Gameplay/SPASpinnerActor.h"
#include "Gameplay/GameStates/SPAGameStateMenu.h"
#include "Gameplay/GameStates/SPAGameStateWorld.h"
#include "Gameplay/GameStates/SPAGameStateGarage.h"
#include "Gameplay/GameStates/SPAGameStateTraining.h"
#include "Gameplay/GameStates/SPAGameStateMatch.h"

#include "Gameplay/Effects/SPAEffects.h"

#include "SPAGameplayTypes.h"

ASpinnerArenaGameModeBase::ASpinnerArenaGameModeBase(const FObjectInitializer& _oi) : Super(_oi)
{
	PrimaryActorTick.bCanEverTick = true;
	PreviousGameState = ESPAGameState::MAX;
	CurrentGameState = ESPAGameState::World;

	Seed = 16;
	RandomStream.Initialize(Seed);
}

void ASpinnerArenaGameModeBase::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	GetSPAGameInstance()->InitWidgets();

	GenerateNewWorld();
}

void ASpinnerArenaGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	
	FActorSpawnParameters spawnParams = FActorSpawnParameters();
	spawnParams.bNoFail = true;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	Effects = GetWorld()->SpawnActor<ASPAEffects>(ASPAEffects::StaticClass(), spawnParams);

	Effects->Init(GetSPAGameSettings());

	//GameStates[(uint8)ESPAGameState::Menu] = new FSPAGameStateMenu();
	//GameStates[(uint8)ESPAGameState::Menu]->Init(this, ESPAGameState::Menu);

	GameStates[(uint8)ESPAGameState::World] = new FSPAGameStateWorld();
	GameStates[(uint8)ESPAGameState::World]->Init(this, ESPAGameState::World);

	GameStates[(uint8)ESPAGameState::Garage] = new FSPAGameStateGarage();
	GameStates[(uint8)ESPAGameState::Garage]->Init(this, ESPAGameState::Garage);

	GameStates[(uint8)ESPAGameState::Training] = new FSPAGameStateTraining();
	GameStates[(uint8)ESPAGameState::Training]->Init(this, ESPAGameState::Training);

	GameStates[(uint8)ESPAGameState::Match] = new FSPAGameStateMatch();
	GameStates[(uint8)ESPAGameState::Match]->Init(this, ESPAGameState::Match);

	SetGameState(ESPAGameState::World);
}

void ASpinnerArenaGameModeBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void ASpinnerArenaGameModeBase::TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{
	Super::TickActor(DeltaTime, TickType, ThisTickFunction);

	if(GameStates[(uint8)CurrentGameState])
		GameStates[(uint8)CurrentGameState]->Update(DeltaTime);
}

void ASpinnerArenaGameModeBase::SetGameState(ESPAGameState _newGameState)
{
	PreviousGameState = CurrentGameState;
	CurrentGameState = _newGameState;

	if(PreviousGameState != ESPAGameState::MAX)
		GameStates[(uint8)PreviousGameState]->Exit();

	GameStates[(uint8)CurrentGameState]->Enter();
}

void ASpinnerArenaGameModeBase::EnterMatchState(const FSPAEnemyData& _enemyData, const FSPASpinnerData& _playerSpinnerData)
{
	PreviousGameState = CurrentGameState;
	CurrentGameState = ESPAGameState::Match;

	if (PreviousGameState != ESPAGameState::MAX)
		GameStates[(uint8)PreviousGameState]->Exit();

	FSPAGameStateMatch* matchGameState = (FSPAGameStateMatch*)GameStates[(uint8)CurrentGameState];
	ensure(matchGameState);
	if (matchGameState)
	{
		matchGameState->InitMatch(_enemyData, _playerSpinnerData);
		matchGameState->Enter();
	}
}

void ASpinnerArenaGameModeBase::JoinRandomMatch()
{
	USPAGameSettings* gameSettings = GetSPAGameSettings();

	FSPAPlayerData& currentPlayerData = gameSettings->GameData.PlayerData;
	FSPAWorldMapData& worldMapData = gameSettings->GameData.WorldMapData;

	FSPAEnemyData enemyData = worldMapData.GetRandomEnemyData();
	FSPASpinnerData playerSpinnerData = currentPlayerData.Spinners[RandomStream.RandRange(0, currentPlayerData.Spinners.Num() - 1)];
	EnterMatchState(enemyData, playerSpinnerData);
}

void ASpinnerArenaGameModeBase::GenerateNewWorld()
{
	USPAGameSettings* gameSettings = GetSPAGameSettings();

	const FSPAGameVisualConfigData& visualConfigData = gameSettings->GameVisualConfigData;

	//colors
	const TArray<FLinearColor>& colors = visualConfigData.Colors;
	ensure(colors.Num() > 0);

	FSPAGameData gameData;

	//player data
	FSPAPlayerData& playerData = gameData.PlayerData;

	//spinners
	TArray<FSPASpinnerData> playerSpinnerDatas;
	int32 numberOfStartingSpinners = 7;
	for (int32 i = 0; i < numberOfStartingSpinners; i++)
	{
		FSPASpinnerData spinnerData;

		ESPASpinnerType spinnerType = ESPASpinnerType::Aluminium;
		FSPASpinnerVisualData spinnerVisualData;
		FSPASpinnerAttributes spinnerAttributes;

		//spinner visual data
		const TArray<FSPASpinnerMeshData>& spinnerMeshDatas = visualConfigData.SpinnerMeshDatas.SpinnerMeshDatas;
		ensure(spinnerMeshDatas.Num() > 0);

		int32 randomSpinnerMeshDataIndex = RandomStream.RandRange(0, spinnerMeshDatas.Num() - 1);
		const FSPASpinnerMeshData& spinnerMeshData = spinnerMeshDatas[randomSpinnerMeshDataIndex];
		ensure(spinnerMeshData.SpinnerTextures[(uint8)spinnerType].SpinnerTextures.Num() > 0);

		const TArray<UTexture*>& spinnerTextures = spinnerMeshData.SpinnerTextures[(uint8)spinnerType].SpinnerTextures;

		UTexture* spinnerTexture = NULL;
		if (spinnerTextures.Num() > 0)
			spinnerTexture = spinnerTextures[0];

		FLinearColor spinnerColor = colors[FMath::RandRange(0, colors.Num() - 1)];

		spinnerVisualData.InitData(spinnerMeshData.SpinnerMesh, spinnerTexture, FLinearColor(0, 0, 0), spinnerColor);

		//spinner attribs
		spinnerType = (ESPASpinnerType)RandomStream.RandRange((uint8)0, (uint8)3);//(uint8)ESPASpinnerType::MAX - 1);
		spinnerAttributes = GetDefaultSpinnerAttributesByType(spinnerType);
		spinnerAttributes.Hp = FMath::RandRange(1, 10) * 10;

		int32 randExp = RandomStream.RandRange(50, 9999);
		spinnerAttributes.AddExp(randExp);

		//spinner skills
		FSPASpinnerSkills skills;
		TArray<ESPAPassiveSkillType> allSkills;
		for (uint8 j = 0; j < (uint8)ESPAPassiveSkillType::MAX; j++)
		{
			allSkills.Add((ESPAPassiveSkillType)j);
		}

		int32 randomNumberOfSkills = FMath::RandRange(0, 3);
		for (int32 j = 0; j < randomNumberOfSkills; j++)
		{
			int32 randomSkillIndex = FMath::RandRange(0, allSkills.Num() - 1);

			skills.AddSkill(allSkills[randomSkillIndex]);

			allSkills.RemoveAt(randomSkillIndex);
		}

		//init
		spinnerData.InitData(spinnerType, spinnerVisualData, spinnerAttributes, skills);

		playerSpinnerDatas.Add(spinnerData);
	}

	//resources
	FSPAResourcesData playerResources;
	playerResources.SetResource(ESPASpinnerType::Aluminium, 100);
	//init
	playerData.InitData(FText::FromString("LUKAS"), playerSpinnerDatas, playerResources);

	//world map data
	FSPAWorldMapData& worldMapData = gameData.WorldMapData;

	//init
	worldMapData.Init(FIntPoint(7, 15), Seed);

	TArray<TArray<FIntPoint>> enemyRows = worldMapData.GetEnemyRows();
	for (int32 i = 0; i < enemyRows.Num(); i++)
	{
		for (int32 j = 0; j < enemyRows[i].Num(); j++)
		{
			const FSPAEnemyData& enemyData = CreateRandomEnemyData();
			worldMapData.SetEnemyData(enemyRows[i][j], enemyData);
		}
	}

	//
	gameSettings->GameData = gameData;
}

void ASpinnerArenaGameModeBase::SaveWorld()
{

}

void ASpinnerArenaGameModeBase::LoadWorld()
{

}

FSPAEnemyData ASpinnerArenaGameModeBase::CreateRandomEnemyData()
{
	USPAGameSettings* gameSettings = GetSPAGameSettings();
	const FSPAGameVisualConfigData& visualConfigData = gameSettings->GameVisualConfigData;

	//colors
	const TArray<FLinearColor>& colors = visualConfigData.Colors;
	ensure(colors.Num() > 0);

	FLinearColor arenaColor = colors[FMath::RandRange(0, colors.Num() - 1)];
	FLinearColor spinnerColor = colors[FMath::RandRange(0, colors.Num() - 1)];


	FSPAEnemyData enemyData;

	//random arena
	const TArray<FSPAArenaMeshData>& arenaMeshDatas = visualConfigData.ArenaMeshDatas.ArenaMeshDatas;
	ensure(arenaMeshDatas.Num() > 0);

	int32 randomArenaIndex = RandomStream.RandRange(0, arenaMeshDatas.Num() - 1);
	const FSPAArenaMeshData& arenaMeshData = arenaMeshDatas[randomArenaIndex];
	
	FSPAArenaData arenaData;

	FSPAArenaVisualData arenaVisualData;
	FSPAArenaAttributes arenaAttributes;

	const TArray<FSPAArenaMeshTextureData>& arenaTextureDatas = arenaMeshData.ArenaTextureDatas;
	ensure(arenaTextureDatas.Num() > 0);

	int32 randomArenaTextureIndex = RandomStream.RandRange(0, arenaTextureDatas.Num() - 1);
	const FSPAArenaMeshTextureData& arenaTextureData = arenaTextureDatas[randomArenaTextureIndex];

	arenaVisualData.InitData(arenaMeshData.ArenaMesh, arenaMeshData.ArenaSideMesh, arenaTextureData.ArenaBottomTexture, arenaTextureData.ArenaSideTexture, arenaColor, arenaColor);

	//init arena data
	arenaData.InitData(arenaVisualData, arenaAttributes);

	//random spinner
	FSPASpinnerData spinnerData;

	ESPASpinnerType spinnerType = ESPASpinnerType::Aluminium;
	FSPASpinnerVisualData spinnerVisualData;
	FSPASpinnerAttributes spinnerAttributes;

	spinnerType = (ESPASpinnerType)RandomStream.RandRange((uint8)3, (uint8)ESPASpinnerType::MAX - 1);

	//spinner visual data
	const TArray<FSPASpinnerMeshData>& spinnerMeshDatas = visualConfigData.SpinnerMeshDatas.SpinnerMeshDatas;
	ensure(spinnerMeshDatas.Num() > 0);

	int32 randomSpinnerMeshDataIndex = RandomStream.RandRange(0, spinnerMeshDatas.Num() - 1);
	const FSPASpinnerMeshData& spinnerMeshData = spinnerMeshDatas[randomSpinnerMeshDataIndex];
	ensure(spinnerMeshData.SpinnerTextures[(uint8)spinnerType].SpinnerTextures.Num() > 0);

	const TArray<UTexture*>& spinnerTextures = spinnerMeshData.SpinnerTextures[(uint8)spinnerType].SpinnerTextures;

	UTexture* spinnerTexture = NULL;
	if (spinnerTextures.Num() > 0)
		spinnerTexture = spinnerTextures[0];

	spinnerVisualData.InitData(spinnerMeshData.SpinnerMesh, spinnerTexture, FLinearColor(0, 0, 0), spinnerColor);

	//spinner attribs
	spinnerAttributes = GetDefaultSpinnerAttributesByType(spinnerType);
	spinnerAttributes.Hp = FMath::RandRange(1, 10) * 10;

	int32 randExp = RandomStream.RandRange(50, 9999);
	spinnerAttributes.AddExp(randExp);

	//spinner skills
	FSPASpinnerSkills skills;
	TArray<ESPAPassiveSkillType> allSkills;
	for (uint8 j = 0; j < (uint8)ESPAPassiveSkillType::MAX; j++)
	{
		allSkills.Add((ESPAPassiveSkillType)j);
	}

	int32 randomNumberOfSkills = FMath::RandRange(0, 3);
	for (int32 j = 0; j < randomNumberOfSkills; j++)
	{
		int32 randomSkillIndex = FMath::RandRange(0, allSkills.Num() - 1);

		skills.AddSkill(allSkills[randomSkillIndex]);

		allSkills.RemoveAt(randomSkillIndex);
	}

	//init spinner data
	spinnerData.InitData(spinnerType, spinnerVisualData, spinnerAttributes, skills);

	//init enemy data
	FString enemyName = "Enemy ";
	enemyName.AppendInt(FMath::RandRange(1, 32));
	enemyData.InitData(FText::FromString(enemyName), arenaData, { spinnerData });

	return enemyData;
}

class ASPAEffects* ASpinnerArenaGameModeBase::GetEffects()
{
	return Effects;
}

ASPAPlayerController* ASpinnerArenaGameModeBase::GetSPAPlayerController()
{
	return Cast<ASPAPlayerController>(GetWorld()->GetFirstPlayerController());
}

ASPAHUD* ASpinnerArenaGameModeBase::GetSPAHUD()
{
	return Cast<ASPAHUD>(GetSPAPlayerController()->GetHUD());
}

USPAGameInstance* ASpinnerArenaGameModeBase::GetSPAGameInstance()
{
	return Cast<USPAGameInstance>(GetWorld()->GetGameInstance());
}

USPAGameSettings* ASpinnerArenaGameModeBase::GetSPAGameSettings()
{
	return GetSPAGameInstance()->GetGameSettings();
}
