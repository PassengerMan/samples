// krolbakal@gmail.com

#pragma once

#include "ARKTypes.generated.h"


UENUM(BlueprintType)
enum class EARKColor : uint8
{
	White,
	Black,
	Red,
	Green,
	Blue,

	MAX
};

struct FARKPositionSize
{
	FVector2D Position;
	FVector2D Size;

	FARKPositionSize(const FVector2D& _position, const FVector2D& _size)
	{
		Position = _position;
		Size = _size;
	}

	void SetPosition(const FVector2D& _position)
	{
		Position = _position;
	}

	void SetSize(const FVector2D& _size)
	{
		Size = _size;
	}
};

struct FARKLevelGenerationData
{
	uint16 StartX;
	uint16 EndX;
	uint16 StartY;
	uint16 EndY;

	uint16 WallStepX;
	uint16 WallStepY;
	uint16 StepX;
	uint16 StepY;

	uint8 MinHealth;
	uint8 MaxHealth;
	uint8 HealthGradientDirectionX;
	uint8 HealthGradientDirectionY;

	FARKLevelGenerationData()
	{
		StartX = 0;
		EndX = 0;
		StartY = 0;
		EndY = 0;

		WallStepX = 0;
		WallStepY = 0;
		StepX = 0;
		StepY = 0;

		MinHealth = 0;
		MaxHealth = 0;

		HealthGradientDirectionX = 0;
		HealthGradientDirectionY = 0;
	}

	FARKLevelGenerationData(uint16 _startX, uint16 _endX, uint16 _startY, uint16 _endY,
		uint16 _wallStepX, uint16 _wallStepY, uint16 _stepX, uint16 _stepY,
		uint8 _minHealth, uint8 _maxHealth, uint8 _healthGradientDirectionX, uint8 _healthGradientDirectionY)
	{
		Init(_startX, _endX, _startY, _endY,
			_wallStepX, _wallStepY, _stepX, _stepY,
			_minHealth, _maxHealth, _healthGradientDirectionX, _healthGradientDirectionY);
	}

	void Init(uint16 _startX, uint16 _endX, uint16 _startY, uint16 _endY,
		uint16 _wallStepX, uint16 _wallStepY, uint16 _stepX, uint16 _stepY,
		uint8 _minHealth, uint8 _maxHealth, uint8 _healthGradientDirectionX, uint8 _healthGradientDirectionY)
	{
		StartX = _startX;
		EndX = _endX;
		StartY = _startY;
		EndY = _endY;

		WallStepX = _wallStepX;
		WallStepY = _wallStepY;
		StepX = _stepX;
		StepY = _stepY;

		MinHealth = _minHealth;
		MaxHealth = _maxHealth;

		HealthGradientDirectionX = _healthGradientDirectionX;
		HealthGradientDirectionY = _healthGradientDirectionY;
	}
};

enum class EARKGridTileType : uint8
{
	Default,
	Obstacle,
	MAX
};

struct FARKGridTile
{
	EARKGridTileType Type;
	uint8 Health;

	FARKGridTile()
	{
		Type = EARKGridTileType::Default;
		Health = 0;
	}

	FARKGridTile(EARKGridTileType _type, uint8 _health)
	{
		Type = _type;
		Health = _health;
	}
};

struct FARKGrid
{
	uint16 SizeX;
	uint16 SizeY;

	TArray<FARKGridTile*> Tiles;

	int32 NumOfObstacles;

	FARKGrid()
	{
		Init(9, 13);
	}

	FARKGrid(uint16 _sizeX, uint16 _sizeY)
	{
		Init(_sizeX, _sizeY);
	}

	void Init(uint16 _sizeX, uint16 _sizeY)
	{
		NumOfObstacles = 0;

		SizeX = _sizeX;
		SizeY = _sizeY;

		uint32 size = _sizeX * _sizeY;

		Tiles.SetNumUninitialized(size);

		for (uint32 i = 0; i < size; ++i)
		{
			Tiles[i] = new FARKGridTile();
		}
	}

	void Clear()
	{
		for (uint32 i = 0; i < (uint32)Tiles.Num(); ++i)
		{
			delete Tiles[i];
		}
		Tiles.Reset();
	}

	void InitObstacles(const FARKLevelGenerationData& _levelGenerationData)
	{
		ClearTiles();

		for (uint16 i = _levelGenerationData.StartY + _levelGenerationData.WallStepY; i < _levelGenerationData.EndY - _levelGenerationData.WallStepY; i += _levelGenerationData.StepY)
		{
			for (uint16 j = _levelGenerationData.StartX + _levelGenerationData.WallStepX; j < _levelGenerationData.EndX - _levelGenerationData.WallStepX; j += _levelGenerationData.StepX)
			{
				uint32 index = (j * SizeY) + i;

				uint16 dirSize = 0;
				uint16 minSize = 0;
				uint16 maxSize = 0;
				uint16 colRow = 0;
				
				if (_levelGenerationData.HealthGradientDirectionX != 0 && _levelGenerationData.HealthGradientDirectionY == 0)
				{
					minSize = _levelGenerationData.StartX + _levelGenerationData.WallStepX;
					maxSize = _levelGenerationData.EndX - _levelGenerationData.WallStepX;
					colRow = j;
				}
				else
				{
					minSize = _levelGenerationData.StartY + _levelGenerationData.WallStepY;
					maxSize = _levelGenerationData.EndY - _levelGenerationData.WallStepY;
					colRow = i;
				}

				uint8 health = (colRow - minSize) * (_levelGenerationData.MaxHealth - _levelGenerationData.MinHealth) / (maxSize - minSize) + _levelGenerationData.MinHealth;

				FARKGridTile* tile = Tiles[index];
				tile->Type = EARKGridTileType::Obstacle;
				tile->Health = health;

				NumOfObstacles++;
			}
		}
	}

	void ClearTiles()
	{
		NumOfObstacles = 0;

		uint32 size = SizeX * SizeY;
		for (uint32 i = 0; i < size; ++i)
		{
			FARKGridTile* tile = Tiles[i];
			tile->Type = EARKGridTileType::Default;
			tile->Health = 0;
		}
	}

	void ChangeObstacleToDefault(uint32 _index)
	{
		FARKGridTile* tile = Tiles[_index];
		tile->Type = EARKGridTileType::Default;
		tile->Health = 0;

		NumOfObstacles--;
	}

	FARKGridTile* GetTile(uint16 _posX, uint16 _posY, uint32* _outIndex = nullptr)
	{
		uint32 index = (_posX * SizeY) + _posY;
		if (_outIndex)
			*_outIndex = index;

		if(Tiles.IsValidIndex(index))
			return Tiles[index];

		return nullptr;
	}

	FARKGridTile* GetTile(uint32 _index)
	{
		if (Tiles.IsValidIndex(_index))
			return Tiles[_index];

		return nullptr;
	}

	void GetNeighbourTiles(uint16 _posX, uint16 _posY, TArray<FARKGridTile*>& _neighbourTiles, TArray<uint32>& _tileIndexes)
	{
		_neighbourTiles.Reset();
		_tileIndexes.Reset();

		uint32 size = SizeX * SizeY;

		uint16 xOffsets[] = { -1, 0, 1, -1, 0, 1, -1, 0, 1 };
		uint16 yOffsets[] = { 1, 1, 1, 0, 0, 0, -1, -1, -1 };
		
		uint16 xCount = ARRAY_COUNT(xOffsets);
		uint16 yCount = ARRAY_COUNT(yOffsets);

		for (uint16 i = 0; i < yCount; ++i)
		{
			for (uint16 j = 0; j < xCount; ++j)
			{
				uint32 index = ((_posX + xOffsets[j]) * SizeY) + (_posY + yOffsets[i]);
				if (index >= 0 && index < size)
				{
					_neighbourTiles.Add(Tiles[index]);
					_tileIndexes.Add(index);
				}
			}
		}
	}
};

UENUM(BlueprintType)
enum class EARKGameState : uint8
{
	Menu,
	Game,

	MAX
};

UENUM(BlueprintType)
enum class EARKPowerUpType : uint8
{
	MultiBall,
	ExplodingBall,
	ExpandPaddle,
	MAX
};

struct FARKGameplayPaddleData
{
	FVector2D Position;
	FVector2D Size;

	EARKPowerUpType ActivePowerUp;

	FARKGameplayPaddleData()
	{
		Clear();
	}

	void Clear()
	{
		ActivePowerUp = EARKPowerUpType::MAX;
	}
};

struct FARKGameplayBallData
{
	FVector2D Position;
	float Radius; //unused xd

	FVector2D Direction;
	float Speed;

	int16 WidgetId;

	FARKGameplayBallData()
	{
		WidgetId = -1;

		Direction.Set(0, 0);
		Speed = 0.0f;
	}
};

struct FARKGameplayObstacleData
{
	FVector2D Position;
	FVector2D Size;
};


struct FARKGameplayPowerUpData
{
	FVector2D Position;
	FVector2D Size;

	EARKPowerUpType Type;

	int16 WidgetId;

	FARKGameplayPowerUpData()
	{
		WidgetId = -1;

		Type = EARKPowerUpType::MAX;
	}
};

struct FARKGameSimulationData
{
	FARKGameplayPaddleData Paddle;

	TArray<FARKGameplayObstacleData> Obstacles;

	TArray<FARKGameplayBallData> Balls;
	TArray<FARKGameplayPowerUpData> PowerUps;

	FARKGameSimulationData()
	{
		Clear();
	}

	void Clear()
	{
		Paddle.Clear();
		Obstacles.Reset();
		Balls.Reset();
		PowerUps.Reset();
	}
};

struct FARKGameRenderData
{
	class UARKPaddleWidget* Paddle;

	TArray<class UARKObstacleWidget*> Obstacles;

	//
	TArray<class UARKBallWidget*> Balls;
	TArray<class UARKPowerUpWidget*> PowerUps;
	TArray<class UARKPowerUpInfoWidget*> PowerUpInfos;
	uint16 LastBallIndex;
	uint16 LastPowerUpIndex;
	uint16 LastPowerUpInfoIndex;

	FARKGameRenderData()
	{
		Paddle = nullptr;
		Clear();
	}

	void Clear();

	class UARKBallWidget* GetNextBall(uint16* _widgetIndex)
	{
		if (_widgetIndex)
			(*_widgetIndex) = LastBallIndex;

		UARKBallWidget* widget = Balls[LastBallIndex++];
		LastBallIndex %= Balls.Num();

		return widget;
	}

	class UARKPowerUpWidget* GetNextPowerUp(uint16* _widgetIndex)
	{
		if (_widgetIndex)
			(*_widgetIndex) = LastPowerUpIndex;

		UARKPowerUpWidget* widget = PowerUps[LastPowerUpIndex++];
		LastPowerUpIndex %= PowerUps.Num();

		return widget;
	}

	class UARKPowerUpInfoWidget* GetNextPowerUpInfo(uint16* _widgetIndex)
	{
		if (_widgetIndex)
			(*_widgetIndex) = LastPowerUpInfoIndex;

		UARKPowerUpInfoWidget* widget = PowerUpInfos[LastPowerUpInfoIndex++];
		LastPowerUpInfoIndex %= PowerUpInfos.Num();

		return widget;
	}
};

static FVector2D Reflect2DVector(const FVector2D& _direction, const FVector2D& _normal)
{
	return _direction - 2 * (_direction | _normal.GetSafeNormal()) * _normal.GetSafeNormal();
}


UENUM(BlueprintType)
enum class EARKMatchState : uint8
{
	ThrowingBall,
	Playing,
	LosingBall,
	NextLevel,
	GameOver,
	MAX
};

struct FARKMatchData
{
	int32 Health;
	int32 Points;

	EARKMatchState MatchState;
	EARKMatchState PreviousMatchState;

	FARKMatchData()
	{
		Clear();
	}

	void Clear()
	{
		Health = 0;
		Points = 0;

		MatchState = EARKMatchState::MAX;
		PreviousMatchState = EARKMatchState::MAX;
	}
};

struct FARKGameInputData
{
	bool bLeft;
	bool bRight;

	bool bSpacePressed;

	float PaddleReflectionAngle;

	FARKGameInputData()
	{
		Clear();
	}

	void Clear()
	{
		bLeft = false;
		bRight = false;

		bSpacePressed = false;

		PaddleReflectionAngle = 0;
	}
};

template <typename T>
class FARKRandomBag
{
public:

	void Init(uint32 _numOfElements);
	void AddElements(T _element, uint32 _numOfElements);
	void FillEmptyElements(T _element);

	void Shuffle();

	T GetElement();

	void Clear();
protected:
	uint32 NumOfElements;
	TArray<T> Bag;

	uint32 LastAddedElementIndex;
	uint32 CurrentElementIndex;
};

template <typename T>
void FARKRandomBag<T>::Init(uint32 _numOfElements)
{
	Clear();

	NumOfElements = _numOfElements;
	Bag.SetNumUninitialized(NumOfElements);
}

template <typename T>
void FARKRandomBag<T>::AddElements(T _element, uint32 _numOfElements)
{
	for (uint32 i = LastAddedElementIndex; i < LastAddedElementIndex + _numOfElements; ++i)
	{
		Bag[i] = _element;
	}
	LastAddedElementIndex += _numOfElements;
}

template <typename T>
void FARKRandomBag<T>::FillEmptyElements(T _element)
{
	for (uint32 i = LastAddedElementIndex; i < NumOfElements; ++i)
	{
		Bag[i] = _element;
	}
}

template <typename T>
void FARKRandomBag<T>::Shuffle()
{
	for (uint32 i = 0; i < NumOfElements; ++i)
	{
		uint32 randomElementIndex = FMath::RandRange(0, NumOfElements - 1);

		T val = Bag[randomElementIndex];
		Bag[randomElementIndex] = Bag[i];
		Bag[i] = val;
	}
}

template <typename T>
T FARKRandomBag<T>::GetElement()
{
	T element = Bag[CurrentElementIndex];

	CurrentElementIndex++;
	CurrentElementIndex %= NumOfElements;

	return element;
}

template <typename T>
void FARKRandomBag<T>::Clear()
{
	Bag.Reset();

	CurrentElementIndex = 0;
	LastAddedElementIndex = 0;
}