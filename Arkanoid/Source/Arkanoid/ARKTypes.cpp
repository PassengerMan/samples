// krolbakal@gmail.com

#include "ARKTypes.h"

#include "Game/UI/Widgets/Elements/ARKPaddleWidget.h"
#include "Game/UI/Widgets/Elements/ARKBallWidget.h"
#include "Game/UI/Widgets/Elements/ARKObstacleWidget.h"
#include "Game/UI/Widgets/Elements/ARKPowerUpWidget.h"
#include "Game/UI/Widgets/Elements/ARKPowerUpInfoWidget.h"

void FARKGameRenderData::Clear()
{
	LastBallIndex = 0;
	LastPowerUpIndex = 0;
	LastPowerUpInfoIndex = 0;

	if (Paddle)
	{
		Paddle->RemoveFromParent();
		Paddle = nullptr;
	}

	for (int32 i = 0; i < Obstacles.Num(); ++i)
	{
		Obstacles[i]->RemoveFromParent();
	}
	Obstacles.Reset();

	for (int32 i = 0; i < Balls.Num(); ++i)
	{
		Balls[i]->RemoveFromParent();
	}
	Balls.Reset();

	for (int32 i = 0; i < PowerUps.Num(); ++i)
	{
		PowerUps[i]->RemoveFromParent();
	}
	PowerUps.Reset();

	for (int32 i = 0; i < PowerUpInfos.Num(); ++i)
	{
		PowerUpInfos[i]->RemoveFromParent();
	}
	PowerUpInfos.Reset();
}