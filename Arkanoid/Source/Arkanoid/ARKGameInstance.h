// krolbakal@gmail.com

#pragma once

#include "Arkanoid.h"
#include "Engine/GameInstance.h"
#include "ARKGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	virtual void Init() override;
	virtual void Shutdown() override;

	void InitFullscreenWidget();

	class UARKGameWidget* GetARKGameWidget();
	class UARKGameSettings* GetARKGameSettings();

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UARKGameSettings> GameSettingsTemplate;
private:
	class UARKFullscreenWidget* FullscreenWidget;
	
};
