// krolbakal@gmail.com

#pragma once

#include "ARKTypes.h"
#include "UObject/NoExportTypes.h"
#include "ARKGameSettings.generated.h"

/**
 * 
 */


USTRUCT(BlueprintType)
struct FARKGameWidgetsConfig
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class UARKPaddleWidget> PaddleTemplate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class UARKBallWidget> BallTemplate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class UARKObstacleWidget> ObstacleTemplate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class UARKPowerUpWidget> PowerUpTemplate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class UARKPowerUpInfoWidget> PowerUpInfoTemplate;
	
};

USTRUCT(BlueprintType)
struct FARKWidgetTemplates
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class UARKFullscreenWidget> FullscreenWidgetTemplate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FARKGameWidgetsConfig GameWidgetsConfig;
};

USTRUCT()
struct FTextContainer
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	FText Text;

	FTextContainer()
	{
		
	}
};

USTRUCT(BlueprintType)
struct FARKTypeNames
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	FTextContainer PowerUpNames[EARKPowerUpType::MAX];

	UPROPERTY(EditAnywhere)
	FTextContainer MatchInfoNames[EARKMatchState::MAX];
};

UCLASS(Blueprintable)
class ARKANOID_API UARKGameSettings : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FARKWidgetTemplates WidgetTemplates;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FARKTypeNames TypeNames;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FLinearColor> ObstacleColors;

	const FLinearColor& GetObstacleColor(uint8 _health);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FLinearColor DefaultBallColor;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FLinearColor ExplodingBallColor;
};
