// krolbakal@gmail.com

#include "ARKGameSettings.h"

#include "ARKTypes.h"


const FLinearColor& UARKGameSettings::GetObstacleColor(uint8 _health)
{
	if (ObstacleColors.IsValidIndex(_health))
	{
		return ObstacleColors[_health];
	}

	return FLinearColor::White;
}