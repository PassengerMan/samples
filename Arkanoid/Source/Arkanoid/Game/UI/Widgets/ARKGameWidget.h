// krolbakal@gmail.com

#pragma once

#include "Elements/ARKBaseWidget.h"
#include "ARKGameWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKGameWidget : public UARKBaseWidget
{
	GENERATED_BODY()
	
public:
	UARKGameWidget(const FObjectInitializer& _oi);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void RegisterWidgets(class UCanvasPanel* _canvasPanel, class UCanvasPanel* _rightPanel, class UARKScoreWidget* _scoreWidget, class UARKMatchInfoWidget* _matchInfoWidget);

	class UCanvasPanel* GetCanvasPanel();
	class UCanvasPanel* GetRightPanel();
	class UARKScoreWidget* GetScoreWidget();
	class UARKMatchInfoWidget* GetMatchInfoWidget();
protected:
	class UCanvasPanel* CanvasPanel;
	class UCanvasPanel* RightPanel;
	class UARKScoreWidget* ScoreWidget;
	class UARKMatchInfoWidget* MatchInfoWidget;
};
