// krolbakal@gmail.com

#include "ARKScoreWidget.h"

#include "Components/TextBlock.h"

UARKScoreWidget::UARKScoreWidget(const FObjectInitializer& _oi) : Super(_oi)
{

}

void UARKScoreWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UARKScoreWidget::RegisterWidgets(class UTextBlock* _score, class UTextBlock* _health, class UTextBlock* _powerUpTextBlock)
{
	Score = _score;
	Health = _health;
	PowerUpTextBlock = _powerUpTextBlock;
}

void UARKScoreWidget::SetScore(int32 _score)
{
	Score->SetText(FText::AsNumber(_score));
}

void UARKScoreWidget::SetHealth(int32 _health)
{
	Health->SetText(FText::AsNumber(_health));
}

void UARKScoreWidget::SetActivePowerUpName(const FText& _powerUpName)
{
	PowerUpTextBlock->SetText(_powerUpName);
}