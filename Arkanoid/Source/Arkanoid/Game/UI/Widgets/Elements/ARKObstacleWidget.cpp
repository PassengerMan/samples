// krolbakal@gmail.com

#include "ARKObstacleWidget.h"
#include "Components/Image.h"

UARKObstacleWidget::UARKObstacleWidget(const FObjectInitializer& _oi) : Super(_oi)
{
	MaxHealth = 1;
}

void UARKObstacleWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UARKObstacleWidget::RegisterWidgets(class UImage* _image)
{
	Image = _image;
	MaterialInstance = Image->GetDynamicMaterial();
}

void UARKObstacleWidget::SetColorVis(const FLinearColor& _color)
{
	if (MaterialInstance)
	{
		MaterialInstance->SetVectorParameterValue(FName("Color"), _color);
	}
}

void UARKObstacleWidget::SetHealthVis(uint8 _health, uint8* _maxHealth)
{
	if (_maxHealth)
	{
		MaxHealth = (*_maxHealth);
	}

	if (MaterialInstance)
	{
		MaterialInstance->SetScalarParameterValue(FName("Health"), (float)_health / MaxHealth);
	}
}