// krolbakal@gmail.com

#include "ARKPowerUpInfoWidget.h"

#include "Components/TextBlock.h"


UARKPowerUpInfoWidget::UARKPowerUpInfoWidget(const FObjectInitializer& _oi) : Super(_oi)
{

}

void UARKPowerUpInfoWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UARKPowerUpInfoWidget::RegisterWidgets(class UTextBlock* _textBlock)
{
	TextBlock = _textBlock;
}

void UARKPowerUpInfoWidget::SetPowerUpName(const FText& _powerUpName)
{
	TextBlock->SetText(_powerUpName);
}