// krolbakal@gmail.com

#pragma once

#include "ARKBaseWidget.h"
#include "ARKPaddleWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKPaddleWidget : public UARKBaseWidget
{
	GENERATED_BODY()
	
public:
	UARKPaddleWidget(const FObjectInitializer& _oi);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
protected:
};
