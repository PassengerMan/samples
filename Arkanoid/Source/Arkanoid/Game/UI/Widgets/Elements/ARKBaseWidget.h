// krolbakal@gmail.com

#pragma once

#include "Blueprint/UserWidget.h"
#include "ARKBaseWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKBaseWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UARKBaseWidget(const FObjectInitializer& _oi);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

protected:
};
