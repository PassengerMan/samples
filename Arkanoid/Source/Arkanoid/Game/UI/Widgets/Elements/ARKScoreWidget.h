// krolbakal@gmail.com

#pragma once

#include "ARKBaseWidget.h"
#include "ARKScoreWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKScoreWidget : public UARKBaseWidget
{
	GENERATED_BODY()
	
public:
	UARKScoreWidget(const FObjectInitializer& _oi);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void RegisterWidgets(class UTextBlock* _score, class UTextBlock* _health, class UTextBlock* _powerUpTextBlock);

	void SetScore(int32 _score);
	void SetHealth(int32 _health);
	void SetActivePowerUpName(const FText& _powerUpName);
protected:
	class UTextBlock* Score;
	class UTextBlock* Health;
	class UTextBlock* PowerUpTextBlock;
};
