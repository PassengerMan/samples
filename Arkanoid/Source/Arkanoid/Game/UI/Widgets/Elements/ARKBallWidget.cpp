// krolbakal@gmail.com

#include "ARKBallWidget.h"

#include "Components/Image.h"

UARKBallWidget::UARKBallWidget(const FObjectInitializer& _oi) : Super(_oi)
{

}

void UARKBallWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UARKBallWidget::RegisterWidgets(class UImage* _image)
{
	Image = _image;
	MaterialInstance = Image->GetDynamicMaterial();
}

void UARKBallWidget::SetColorVis(const FLinearColor& _color)
{
	if (MaterialInstance)
	{
		MaterialInstance->SetVectorParameterValue(FName("Color"), _color);
	}
}

void UARKBallWidget::SetAnimate(float _animate)
{
	if (MaterialInstance)
	{
		MaterialInstance->SetScalarParameterValue(FName("Animate"), _animate);
	}
}