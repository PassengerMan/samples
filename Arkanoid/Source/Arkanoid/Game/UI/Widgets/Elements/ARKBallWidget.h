// krolbakal@gmail.com

#pragma once

#include "ARKBaseWidget.h"
#include "ARKBallWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKBallWidget : public UARKBaseWidget
{
	GENERATED_BODY()
	
public:
	UARKBallWidget(const FObjectInitializer& _oi);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void RegisterWidgets(class UImage* _image);

	void SetColorVis(const FLinearColor& _color);
	void SetAnimate(float _animate);
protected:
	class UImage* Image;
	class UMaterialInstanceDynamic* MaterialInstance;
};
