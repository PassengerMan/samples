// krolbakal@gmail.com

#pragma once

#include "ARKBaseWidget.h"
#include "ARKPowerUpWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKPowerUpWidget : public UARKBaseWidget
{
	GENERATED_BODY()
	
public:
	UARKPowerUpWidget(const FObjectInitializer& _oi);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

protected:

};
