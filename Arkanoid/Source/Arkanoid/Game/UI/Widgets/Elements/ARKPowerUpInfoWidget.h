// krolbakal@gmail.com

#pragma once

#include "ARKBaseWidget.h"
#include "ARKPowerUpInfoWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKPowerUpInfoWidget : public UARKBaseWidget
{
	GENERATED_BODY()
	
public:
	UARKPowerUpInfoWidget(const FObjectInitializer& _oi);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void RegisterWidgets(class UTextBlock* _textBlock);

	void SetPowerUpName(const FText& _powerUpName);
protected:

	class UTextBlock* TextBlock;
};
