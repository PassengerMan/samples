// krolbakal@gmail.com

#pragma once

#include "ARKBaseWidget.h"
#include "ARKObstacleWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKObstacleWidget : public UARKBaseWidget
{
	GENERATED_BODY()
	
public:
	UARKObstacleWidget(const FObjectInitializer& _oi);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void RegisterWidgets(class UImage* _image);

	void SetColorVis(const FLinearColor& _color);
	void SetHealthVis(uint8 _health, uint8* _maxHealth = nullptr);
protected:
	class UImage* Image;
	UMaterialInstanceDynamic* MaterialInstance;

	uint8 MaxHealth;
};
