// krolbakal@gmail.com

#pragma once

#include "ARKBaseWidget.h"
#include "ARKMatchInfoWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKMatchInfoWidget : public UARKBaseWidget
{
	GENERATED_BODY()
	
public:
	UARKMatchInfoWidget(const FObjectInitializer& _oi);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void RegisterWidgets(class UTextBlock* _matchText);

	void SetMatchText(const FText& _matchText);

protected:
	class UTextBlock* MatchText;
};
