// krolbakal@gmail.com

#include "ARKMatchInfoWidget.h"

#include "Components/TextBlock.h"

UARKMatchInfoWidget::UARKMatchInfoWidget(const FObjectInitializer& _oi) : Super(_oi)
{

}

void UARKMatchInfoWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UARKMatchInfoWidget::RegisterWidgets(class UTextBlock* _matchText)
{
	MatchText = _matchText;
}

void UARKMatchInfoWidget::SetMatchText(const FText& _matchText)
{
	MatchText->SetText(_matchText);
}