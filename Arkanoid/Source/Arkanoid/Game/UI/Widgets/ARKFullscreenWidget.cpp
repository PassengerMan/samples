// krolbakal@gmail.com

#include "ARKFullscreenWidget.h"


UARKFullscreenWidget::UARKFullscreenWidget(const FObjectInitializer& _oi) : Super(_oi)
{
	MainMenuWidget = nullptr;
	GameWidget = nullptr;
}

void UARKFullscreenWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UARKFullscreenWidget::RegisterWidgets(class UARKMainMenuWidget* _mainMenuWidget, class UARKGameWidget* _gameWidget)
{
	MainMenuWidget = _mainMenuWidget;
	GameWidget = _gameWidget;
}

class UARKGameWidget* UARKFullscreenWidget::GetARKGameWidget()
{
	return GameWidget;
}