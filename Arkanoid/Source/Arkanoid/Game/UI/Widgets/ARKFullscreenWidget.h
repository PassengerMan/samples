// krolbakal@gmail.com

#pragma once

#include "Elements/ARKBaseWidget.h"
#include "ARKFullscreenWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKFullscreenWidget : public UARKBaseWidget
{
	GENERATED_BODY()
	
public:
	UARKFullscreenWidget(const FObjectInitializer& _oi);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void RegisterWidgets(class UARKMainMenuWidget* _mainMenuWidget, class UARKGameWidget* _gameWidget);

	class UARKGameWidget* GetARKGameWidget();
protected:
	class UARKMainMenuWidget* MainMenuWidget;
	class UARKGameWidget* GameWidget;
};
