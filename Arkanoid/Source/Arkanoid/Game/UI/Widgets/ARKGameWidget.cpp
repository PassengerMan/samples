// krolbakal@gmail.com

#include "ARKGameWidget.h"

#include "Game/UI/Widgets/Elements/ARKScoreWidget.h"

UARKGameWidget::UARKGameWidget(const FObjectInitializer& _oi) : Super(_oi)
{

}

void UARKGameWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UARKGameWidget::RegisterWidgets(class UCanvasPanel* _canvasPanel, class UCanvasPanel* _rightPanel, class UARKScoreWidget* _scoreWidget, class UARKMatchInfoWidget* _matchInfoWidget)
{
	CanvasPanel = _canvasPanel;
	RightPanel = _rightPanel;
	ScoreWidget = _scoreWidget;
	MatchInfoWidget = _matchInfoWidget;
}

class UCanvasPanel* UARKGameWidget::GetCanvasPanel()
{
	return CanvasPanel;
}

class UCanvasPanel* UARKGameWidget::GetRightPanel()
{
	return RightPanel;
}

class UARKScoreWidget* UARKGameWidget::GetScoreWidget()
{
	return ScoreWidget;
}

class UARKMatchInfoWidget* UARKGameWidget::GetMatchInfoWidget()
{
	return MatchInfoWidget;
}