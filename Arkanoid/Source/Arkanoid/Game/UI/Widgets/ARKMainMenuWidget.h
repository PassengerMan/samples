// krolbakal@gmail.com

#pragma once

#include "Elements/ARKBaseWidget.h"
#include "ARKMainMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API UARKMainMenuWidget : public UARKBaseWidget
{
	GENERATED_BODY()
	
public:
	UARKMainMenuWidget(const FObjectInitializer& _oi);
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	//UFUNCTION(BlueprintCallable)
	//void RegisterWidgets(class UCanvasPanel* _canvasPanel);

	//class UCanvasPanel* GetCanvasPanel();
protected:
	//class UCanvasPanel* CanvasPanel;
};
