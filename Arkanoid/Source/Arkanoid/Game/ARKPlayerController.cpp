// krolbakal@gmail.com

#include "ARKPlayerController.h"

AARKPlayerController::AARKPlayerController(const FObjectInitializer& _oi) : Super(_oi)
{
	PrimaryActorTick.bCanEverTick = true;
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	bEnableMouseOverEvents = true;
	bEnableTouchOverEvents = true;
}

void AARKPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
}
