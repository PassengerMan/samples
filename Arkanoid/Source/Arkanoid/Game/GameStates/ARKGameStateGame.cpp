// krolbakal@gmail.com

#include "ARKGameStateGame.h"
#include "ARKGameInstance.h"
#include "ARKGameSettings.h"

#include "Game/UI/Widgets/ARKGameWidget.h"
#include "Game/UI/Widgets/Elements/ARKPaddleWidget.h"
#include "Game/UI/Widgets/Elements/ARKBallWidget.h"
#include "Game/UI/Widgets/Elements/ARKObstacleWidget.h"
#include "Game/UI/Widgets/Elements/ARKPowerUpWidget.h"
#include "Game/UI/Widgets/Elements/ARKPowerUpInfoWidget.h"
#include "Game/UI/Widgets/Elements/ARKScoreWidget.h"
#include "Game/UI/Widgets/Elements/ARKMatchInfoWidget.h"

#include "Components/CanvasPanel.h"
#include "Components/CanvasPanelSlot.h"

#include "Game/ARKPlayerController.h"

void FARKGameStateGame::InitBase()
{
	FARKGameStateBase::InitBase();

	Grid = nullptr;
	FixedResolution.X = ((500 * 5) + 250) / 2;
	FixedResolution.Y = (1000 * 4) / 2;
	TileSize.X = 250 / 2;
	TileSize.Y = 100 / 2;

	GameWidget = nullptr;
}

void FARKGameStateGame::Enter()
{
	FARKGameStateBase::Enter();

	GameWidget = GameInstance->GetARKGameWidget();
	if (UCanvasPanelSlot* canvasPanelSlot = Cast<UCanvasPanelSlot>(GameWidget->GetCanvasPanel()->Slot))
	{
		canvasPanelSlot->SetSize(FixedResolution);
	}
	GameWidget->SetVisibility(ESlateVisibility::Visible);

	InitGame();
}

void FARKGameStateGame::Exit()
{
	GameWidget->SetVisibility(ESlateVisibility::Hidden);

	Grid->Clear();
	delete Grid;
	Grid = nullptr;

	MatchData.Clear();
	PowerUpsBag.Clear();

	GameSimulationData.Clear();
	GameRenderData.Clear();
	GameInputData.Clear();

	LevelGenerationDatas.Reset();
	CurrentLevelIndex = 0;

	FARKGameStateBase::Exit();
}

void FARKGameStateGame::Update(float _deltaTime)
{
	UpdateInput(_deltaTime);
	UpdateVisuals(_deltaTime);
	UpdateGame(_deltaTime);
}

void FARKGameStateGame::InitGame()
{
	UARKGameSettings* gameSettings = GameInstance->GetARKGameSettings();
	UCanvasPanel* canvasPanel = GameWidget->GetCanvasPanel();
	UCanvasPanel* rightPanel = GameWidget->GetRightPanel();

	Grid = new FARKGrid(FixedResolution.X / TileSize.X, FixedResolution.Y / TileSize.Y);

	uint16 gridSizeX = Grid->SizeX;
	uint16 gridSizeY = Grid->SizeY;
	uint32 gridSize = gridSizeX * gridSizeY;

	GameSimulationData.Obstacles.SetNumUninitialized(gridSize);
	GameRenderData.Obstacles.SetNumUninitialized(gridSize);

	//init paddle
	GameRenderData.Paddle = CreateWidget<UARKPaddleWidget>(GameInstance, gameSettings->WidgetTemplates.GameWidgetsConfig.PaddleTemplate);
	FVector2D paddlePosition((FixedResolution.X / 2.0f) - TileSize.X / 2.0f, FixedResolution.Y - (TileSize.Y));
	FVector2D paddleSize(TileSize.X * 2.0f, TileSize.Y);

	UCanvasPanelSlot* paddleSlot = canvasPanel->AddChildToCanvas(GameRenderData.Paddle);
	paddleSlot->SetSize(paddleSize);
	paddleSlot->SetPosition(paddlePosition);
	paddleSlot->SetZOrder(1);

	GameSimulationData.Paddle.Position = paddlePosition;
	GameSimulationData.Paddle.Size = paddleSize;

	//init balls
	uint16 maxBalls = 32;
	GameRenderData.Balls.SetNumUninitialized(maxBalls);
	UCanvasPanelSlot* firstBallSlot = nullptr;

	FVector2D ballPosition(0, 0);
	FVector2D ballSize(TileSize.Y / 2.0f, TileSize.Y / 2.0f);
	for (uint16 i = 0; i < maxBalls; ++i)
	{
		UARKBallWidget* ballWidget = CreateWidget<UARKBallWidget>(GameInstance, gameSettings->WidgetTemplates.GameWidgetsConfig.BallTemplate);
		ballWidget->SetVisibility(ESlateVisibility::Hidden);

		UCanvasPanelSlot* ballSlot = canvasPanel->AddChildToCanvas(ballWidget);
		ballSlot->SetSize(ballSize);
		ballSlot->SetPosition(ballPosition);
		ballSlot->SetZOrder(16);
		ballSlot->SetAlignment(FVector2D(0.5f, 0.5f));

		if (i == 0)
			firstBallSlot = ballSlot;

		GameRenderData.Balls[i] = ballWidget;
	}

	//init powerups
	uint16 maxPowerUps = 16;
	GameRenderData.PowerUps.SetNumUninitialized(maxPowerUps);

	FVector2D powerUpPosition(0, 0);
	FVector2D powerUpSize(TileSize.Y / 2.0f, TileSize.Y / 2.0f);
	for (uint16 i = 0; i < maxPowerUps; ++i)
	{
		UARKPowerUpWidget* powerUpWidget = CreateWidget<UARKPowerUpWidget>(GameInstance, gameSettings->WidgetTemplates.GameWidgetsConfig.PowerUpTemplate);
		powerUpWidget->SetVisibility(ESlateVisibility::Hidden);

		UCanvasPanelSlot* powerUpSlot = canvasPanel->AddChildToCanvas(powerUpWidget);
		powerUpSlot->SetSize(powerUpSize);
		powerUpSlot->SetPosition(powerUpPosition);
		powerUpSlot->SetZOrder(3);
		powerUpSlot->SetAlignment(FVector2D(0.5f, 0.5f));

		GameRenderData.PowerUps[i] = powerUpWidget;
	}

	//init powerup infos
	GameRenderData.PowerUpInfos.SetNumUninitialized(maxPowerUps);

	for (uint16 i = 0; i < maxPowerUps; ++i)
	{
		UARKPowerUpInfoWidget* powerUpInfoWidget = CreateWidget<UARKPowerUpInfoWidget>(GameInstance, gameSettings->WidgetTemplates.GameWidgetsConfig.PowerUpInfoTemplate);
		powerUpInfoWidget->SetVisibility(ESlateVisibility::Hidden);

		UCanvasPanelSlot* powerUpInfoSlot = rightPanel->AddChildToCanvas(powerUpInfoWidget);
		powerUpInfoSlot->SetAutoSize(true);
		powerUpInfoSlot->SetPosition(powerUpPosition);
		powerUpInfoSlot->SetZOrder(3);
		powerUpInfoSlot->SetAlignment(FVector2D(0.0f, 0.5f));

		GameRenderData.PowerUpInfos[i] = powerUpInfoWidget;
	}

	//obstacles
	for (uint16 i = 0; i < gridSizeY; i++)
	{
		for (uint16 j = 0; j < gridSizeX; j++)
		{
			FARKGridTile* gridTile = Grid->GetTile(j, i);

			uint32 index = (j * gridSizeY) + i;

			float posX = (j * TileSize.X);
			float posY = (i * TileSize.Y);

			UARKObstacleWidget* obstacleWidget = CreateWidget<UARKObstacleWidget>(GameInstance, gameSettings->WidgetTemplates.GameWidgetsConfig.ObstacleTemplate);
			GameRenderData.Obstacles[index] = obstacleWidget;

			UCanvasPanelSlot* obstacleSlot = canvasPanel->AddChildToCanvas(obstacleWidget);
			obstacleSlot->SetSize(FVector2D(TileSize.X - 6, TileSize.Y - 6));
			obstacleSlot->SetPosition(FVector2D(posX + 3, posY + 3));
			obstacleSlot->SetZOrder(2);

			GameSimulationData.Obstacles[index].Position.Set(posX, posY);
			GameSimulationData.Obstacles[index].Size = TileSize;
		}
	}

	MatchData.MatchState = EARKMatchState::MAX;
	MatchData.PreviousMatchState = EARKMatchState::MAX;

	CurrentLevelIndex = 0;

	/*
		uint16 _startX, uint16 _endX, uint16 _startY, uint16 _endY,
		uint16 _wallStepX, uint16 _wallStepY, uint16 _stepX, uint16 _stepY,
		uint8 _minHealth, uint8 _maxHealth, uint8 _healthGradientDirectionX, uint8 _healthGradientDirectionY)
	*/

	FARKLevelGenerationData level1;
	level1.Init(0, gridSizeX, 0, 5,						1, 1, 1, 1,		 1, 2, 0, 1);

	FARKLevelGenerationData level2;
	level2.Init(0, gridSizeX, 0, gridSizeY / 2.0f,		1, 1, 2, 2,		 1, 4, 1, 0);

	FARKLevelGenerationData level3;
	level3.Init(0, gridSizeX, 0, gridSizeY / 1.5f,		1, 2, 1, 2,		 1, 5, 0, 1);
	//FARKLevelGenerationData level4;

	//FARKLevelGenerationData level5;

	LevelGenerationDatas.Add(level1);
	LevelGenerationDatas.Add(level2);
	LevelGenerationDatas.Add(level3);

	RestartGame();
}

void FARKGameStateGame::RestartGame()
{
	UARKGameSettings* gameSettings = GameInstance->GetARKGameSettings();

	uint16 gridSizeX = Grid->SizeX;
	uint16 gridSizeY = Grid->SizeY;
	uint32 gridSize = gridSizeX * gridSizeY;

	if (MatchData.MatchState == EARKMatchState::GameOver || MatchData.MatchState == EARKMatchState::MAX)
	{
		Grid->InitObstacles(GetCurrentLevelData());
	}
	else if(MatchData.MatchState == EARKMatchState::NextLevel)
	{
		Grid->InitObstacles(GetNextLevelData());
	}

	//obstacles
	for (uint32 i = 0; i < gridSize; i++)
	{
		FARKGridTile* gridTile = Grid->GetTile(i);
		UARKObstacleWidget* obstacleWidget = GameRenderData.Obstacles[i];

		if (gridTile->Type == EARKGridTileType::Obstacle)
		{	
			uint8 health = gridTile->Health;
			obstacleWidget->SetColorVis(gameSettings->GetObstacleColor(health)); //color based on life
			obstacleWidget->SetHealthVis(health, &health);
			obstacleWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
		}
		else
		{
			obstacleWidget->SetVisibility(ESlateVisibility::Hidden);
		}
	}

	//balls
	for (int32 i = 0; i < GameSimulationData.Balls.Num(); ++i)
	{
		FARKGameplayBallData& ballData = GameSimulationData.Balls[i];
		if (ballData.WidgetId != -1)
		{
			GameRenderData.Balls[ballData.WidgetId]->SetVisibility(ESlateVisibility::Hidden);
		}
	}
	GameSimulationData.Balls.Reset();

	//paddle
	GameSimulationData.Paddle.Clear();
	GameSimulationData.Paddle.Size = FVector2D(TileSize.X * 2.0f, TileSize.Y);

	//powerups
	for (int32 i = 0; i < GameSimulationData.PowerUps.Num(); ++i)
	{
		FARKGameplayPowerUpData& powerUpData = GameSimulationData.PowerUps[i];
		if (powerUpData.WidgetId != -1)
		{
			GameRenderData.PowerUps[powerUpData.WidgetId]->SetVisibility(ESlateVisibility::Hidden);
			GameRenderData.PowerUpInfos[powerUpData.WidgetId]->SetVisibility(ESlateVisibility::Hidden);
		}
	}
	GameSimulationData.PowerUps.Reset();

	MatchData.Health = 5;
	MatchData.Points = 0;

	PowerUpsBag.Init(Grid->NumOfObstacles);
	PowerUpsBag.AddElements(EARKPowerUpType::ExplodingBall, 3);
	PowerUpsBag.AddElements(EARKPowerUpType::MultiBall, 3);
	PowerUpsBag.AddElements(EARKPowerUpType::ExpandPaddle, 3);
	PowerUpsBag.FillEmptyElements(EARKPowerUpType::MAX);
	PowerUpsBag.Shuffle();

	ChangeMatchState(EARKMatchState::ThrowingBall);
}

void FARKGameStateGame::UpdateVisuals(float _deltaTime)
{
	//update paddle
	if (UCanvasPanelSlot* paddleSlot = Cast<UCanvasPanelSlot>(GameRenderData.Paddle->Slot))
	{
		paddleSlot->SetPosition(GameSimulationData.Paddle.Position);
		paddleSlot->SetSize(GameSimulationData.Paddle.Size);
	}

	//update balls
	TArray<FARKGameplayBallData>& ballsData = GameSimulationData.Balls;
	for (uint16 i = 0; i < ballsData.Num(); ++i)
	{
		FARKGameplayBallData& ballData = ballsData[i];

		if (UCanvasPanelSlot* ballSlot = Cast<UCanvasPanelSlot>(GameRenderData.Balls[ballData.WidgetId]->Slot))
		{
			ballSlot->SetPosition(ballData.Position);
		}
	}

	//update power ups
	TArray<FARKGameplayPowerUpData>& powerUpsData = GameSimulationData.PowerUps;
	for (uint16 i = 0; i < powerUpsData.Num(); ++i)
	{
		FARKGameplayPowerUpData& powerUpData = powerUpsData[i];

		if (UCanvasPanelSlot* powerUpSlot = Cast<UCanvasPanelSlot>(GameRenderData.PowerUps[powerUpData.WidgetId]->Slot))
		{
			powerUpSlot->SetPosition(powerUpData.Position);
		}

		if (UCanvasPanelSlot* powerUpInfoSlot = Cast<UCanvasPanelSlot>(GameRenderData.PowerUpInfos[powerUpData.WidgetId]->Slot))
		{
			powerUpInfoSlot->SetPosition(FVector2D(0, powerUpData.Position.Y));
		}
	}

	//update score widget
	UARKGameSettings* gameSettings = GameInstance->GetARKGameSettings();
	UARKScoreWidget* scoreWidget = GameWidget->GetScoreWidget();

	if (GameSimulationData.Paddle.ActivePowerUp != EARKPowerUpType::MAX)
	{
		const FText& powerUpName = gameSettings->TypeNames.PowerUpNames[(uint8)GameSimulationData.Paddle.ActivePowerUp].Text;
		scoreWidget->SetActivePowerUpName(powerUpName);

	}
	else
	{
		scoreWidget->SetActivePowerUpName(FText::FromString("No Power Up"));
	}

	scoreWidget->SetHealth(MatchData.Health);
	scoreWidget->SetScore(MatchData.Points);

	//ball color
	if (GameSimulationData.Balls.Num() > 0)
	{
		FARKGameplayBallData& firstBallData = GameSimulationData.Balls[0];
		UARKBallWidget* ballWidget = GameRenderData.Balls[firstBallData.WidgetId];
		if (GameSimulationData.Paddle.ActivePowerUp == EARKPowerUpType::ExplodingBall)
		{
			ballWidget->SetColorVis(gameSettings->ExplodingBallColor);
			ballWidget->SetAnimate(1.0f);
		}
		else
		{
			ballWidget->SetColorVis(gameSettings->DefaultBallColor);
			ballWidget->SetAnimate(0.0f);
		}
	}



}

void FARKGameStateGame::UpdateInput(float _deltaTime)
{
	AARKPlayerController* pc = Cast<AARKPlayerController>(GetWorld()->GetFirstPlayerController());

	bool bLeft = pc->IsInputKeyDown(EKeys::A);
	bool bRight = pc->IsInputKeyDown(EKeys::D);
	bool bSpace = pc->WasInputKeyJustReleased(EKeys::SpaceBar);

	GameInputData.bSpacePressed = bSpace;

	//paddle
	FVector2D& paddlePosition = GameSimulationData.Paddle.Position;
	FVector2D& paddleSize = GameSimulationData.Paddle.Size;
	if (bLeft)
	{
		GameInputData.bLeft = true;
		GameInputData.bRight = false;
		GameInputData.PaddleReflectionAngle = 15;

		paddlePosition.X -= 20 * (1 + _deltaTime);
	}
	else if (bRight)
	{
		GameInputData.bLeft = false;
		GameInputData.bRight = true;
		GameInputData.PaddleReflectionAngle = -15;

		paddlePosition.X += 20 * (1 + _deltaTime);
	}
	else
	{
		GameInputData.bLeft = false;
		GameInputData.bRight = false;
		GameInputData.PaddleReflectionAngle = 0;
	}

	paddlePosition.X = FMath::Clamp(paddlePosition.X, 0.0f, (float)FixedResolution.X - paddleSize.X);

	switch (MatchData.MatchState)
	{
		case EARKMatchState::ThrowingBall:
		{
			if (GameSimulationData.Balls.Num() > 0)
			{
				FVector2D& ballPosition = GameSimulationData.Balls[0].Position;
				ballPosition.X = paddlePosition.X + (paddleSize.X / 2.0f);
				if (bSpace)
				{
					FVector2D& ballDirection = GameSimulationData.Balls[0].Direction;
					ballDirection.Set(0.0f, -1.0f);
					ballDirection = ballDirection.GetRotated(GameInputData.PaddleReflectionAngle == 0 ? 15 : GameInputData.PaddleReflectionAngle);

					ChangeMatchState(EARKMatchState::Playing);
				}
			}

			break;
		}
		case EARKMatchState::Playing:
		{
			if (bSpace)
			{
				UsePowerUp();
			}
			break;
		}
		case EARKMatchState::GameOver:
		case EARKMatchState::NextLevel:
		{
			if (bSpace)
			{
				RestartGame();
			}
			break;
		}
	}
}

void FARKGameStateGame::UpdateGame(float _deltaTime)
{
	if (MatchData.MatchState != EARKMatchState::Playing)
		return;

	FVector2D& paddlePosition = GameSimulationData.Paddle.Position;
	FVector2D& paddleSize = GameSimulationData.Paddle.Size;

	TArray<FARKGameplayBallData>& ballsData = GameSimulationData.Balls;
	TArray<uint16> ballsToDelete;
	for (uint16 i = 0; i < ballsData.Num(); ++i)
	{
		FARKGameplayBallData& ballData = ballsData[i];

		//ball
		FVector2D& ballPosition = ballData.Position;
		float& ballSpeed = ballData.Speed;
		FVector2D& ballDirection = ballData.Direction;

		FVector2D ballPositionDelta = ballDirection * (ballSpeed * (1 + _deltaTime));
		ballPosition += ballPositionDelta;

		//ball paddle collision
		FVector2D newBallDirection;
		bool bSuccess = CollideBallWithObstacle(ballPosition, ballPositionDelta, ballDirection, paddlePosition, paddleSize, newBallDirection);
		if (bSuccess)
		{
			ballPosition -= ballPositionDelta;

			//if (GameInputData.PaddleReflectionAngle != 0)
			//{
				float angle = FMath::RadiansToDegrees(FMath::Atan2(newBallDirection.Y, newBallDirection.X)) + 90.0f;
				float clampedAngle = FMath::Clamp(angle + GameInputData.PaddleReflectionAngle, -60.0f, 60.0f);

				if (clampedAngle > -15 && clampedAngle < 15)
				{
					int32 angleSign = FMath::Sign((int32)clampedAngle);
					clampedAngle = angleSign == 0 ? GameInputData.PaddleReflectionAngle : angleSign * 15.0f;
				}
					

				newBallDirection = FVector2D(0, -1).GetRotated(clampedAngle);
			//}
				
			ballDirection = newBallDirection;
		}

		//ball obstacle collision
		FIntPoint ballGridPosition;
		ViewportPositionToGridPosition(ballPosition, ballGridPosition);

		uint32 ballTileIndex = UINT32_MAX;
		FARKGridTile* ballTile = Grid->GetTile(ballGridPosition.X, ballGridPosition.Y, &ballTileIndex);
		if (ballTile->Type == EARKGridTileType::Obstacle)
		{
			FVector2D& obstaclePosition = GameSimulationData.Obstacles[ballTileIndex].Position;
			FVector2D& obstacleSize = GameSimulationData.Obstacles[ballTileIndex].Size;
			FVector2D newBallDirection;
			bool bSuccess = CollideBallWithObstacle(ballPosition, ballPositionDelta, ballDirection, obstaclePosition, obstacleSize, newBallDirection);
			if (bSuccess)
			{
				ballPosition -= ballPositionDelta;
				ballDirection = newBallDirection;

				OnObstacleHit(ballTileIndex);

				if (ballTile->Health <= 0)
				{
					GameRenderData.Obstacles[ballTileIndex]->SetVisibility(ESlateVisibility::Hidden);

					OnObstacleDestroyed(ballTileIndex);
				}
			}
		}

		//ball wall collision
		if (ballPosition.X >= FixedResolution.X)
		{
			ballPosition -= ballPositionDelta;
			ballDirection = Reflect2DVector(ballDirection, FVector2D(-1, 0));
		}
		else if (ballPosition.X <= 0)
		{
			ballPosition -= ballPositionDelta;
			ballDirection = Reflect2DVector(ballDirection, FVector2D(1, 0));
		}
		if (ballPosition.Y <= 0)
		{
			ballPosition -= ballPositionDelta;
			ballDirection = Reflect2DVector(ballDirection, FVector2D(0, 1));
		}
		else if (ballPosition.Y >= FixedResolution.Y)
		{
			ballsToDelete.Add(i);
		}
	}

	TArray<uint16> powerUpsToDelete;
	TArray<FARKGameplayPowerUpData>& powerUpsData = GameSimulationData.PowerUps;
	for (int32 i = 0; i < powerUpsData.Num(); ++i)
	{
		FARKGameplayPowerUpData& powerUpData = powerUpsData[i];
		FVector2D powerUpPositionDelta = (10.0f * (1.0f + _deltaTime)) * FVector2D(0, 1);
		powerUpData.Position += powerUpPositionDelta;
		
		FVector2D tempDir;
		bool bSuccess = CollideBallWithObstacle(powerUpData.Position, powerUpPositionDelta, FVector2D(0, 1), paddlePosition, paddleSize, tempDir);
		if (bSuccess)
		{
			OnPowerUpPicked(powerUpData.Type);
			powerUpsToDelete.Add(i);
		}
		else
		{
			if (powerUpData.Position.Y >= FixedResolution.Y)
			{
				powerUpsToDelete.Add(i);
			}
		}
	}

	if (powerUpsToDelete.Num() > 0)
	{
		for (int16 i = powerUpsToDelete.Num() - 1; i >= 0; i--)
		{
			uint16 powerUpIndex = powerUpsToDelete[i];

			FARKGameplayPowerUpData& powerUpData = powerUpsData[powerUpIndex];

			//reset positions
			GameRenderData.PowerUps[powerUpData.WidgetId]->SetVisibility(ESlateVisibility::Hidden);
			GameRenderData.PowerUpInfos[powerUpData.WidgetId]->SetVisibility(ESlateVisibility::Hidden);
			powerUpsData.RemoveAt(powerUpIndex, 1, false);
		}
	}

	if (ballsToDelete.Num() > 0)
	{
		for (int16 i = ballsToDelete.Num() - 1; i >= 0; i--)
		{
			uint16 ballIndex = ballsToDelete[i];

			FARKGameplayBallData& ballData = ballsData[ballIndex];

			//reset positions
			GameRenderData.Balls[ballData.WidgetId]->SetVisibility(ESlateVisibility::Hidden);
			ballsData.RemoveAt(ballIndex, 1, false);
		}
	}

	if (ballsData.Num() <= 0)
	{
		OnBallLost();
	}
}

void FARKGameStateGame::UpdateAnimation(float _deltaTime)
{
	/*for (auto it = ObstacleAnimations.CreateIterator(); it; ++it)
	{
		FLocalAnimationData& animData = (*it);
		animData.Duration -= _deltaTime;
		if (animData.Duration <= 0)
		{
			it.RemoveCurrent();
		}
	}*/
}

void FARKGameStateGame::ChangeMatchState(EARKMatchState _matchState)
{
	MatchData.PreviousMatchState = MatchData.MatchState;
	MatchData.MatchState = _matchState;

	OnChangedMatchState(_matchState);
}

void FARKGameStateGame::OnChangedMatchState(EARKMatchState _matchState)
{
	GameWidget = GameInstance->GetARKGameWidget();
	UARKMatchInfoWidget* matchInfoWidget = GameWidget->GetMatchInfoWidget();

	UARKGameSettings* gameSettings = GameInstance->GetARKGameSettings();
	const FText& matchInfoName = gameSettings->TypeNames.MatchInfoNames[(uint8)_matchState].Text;
	if (!matchInfoName.IsEmpty())
	{
		matchInfoWidget->SetMatchText(matchInfoName);
		matchInfoWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	}
	else
	{
		matchInfoWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	

	switch (_matchState)
	{
		case EARKMatchState::ThrowingBall:
		{
			FVector2D& paddlePosition = GameSimulationData.Paddle.Position;
			FVector2D& paddleSize = GameSimulationData.Paddle.Size;

			//init first ball
			uint16 lastBallIndex;
			UARKBallWidget* ballWidget = GameRenderData.GetNextBall(&lastBallIndex);
			ballWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);

			FARKGameplayBallData ballData;

			FVector2D& ballPosition = ballData.Position;
			ballPosition.X = paddlePosition.X + (paddleSize.X / 2.0f);
			ballPosition.Y = paddlePosition.Y - 16;

			ballData.Speed = 15.0f;
			FVector2D& ballDirection = ballData.Direction;
			ballDirection.Set(0.0f, 0.0f);

			ballData.WidgetId = lastBallIndex;

			GameSimulationData.Balls.Add(ballData);
			break;
		}
		case EARKMatchState::Playing:
		{

			break;
		}
		case EARKMatchState::LosingBall:
		{
			MatchData.Health -= 1;
			if (MatchData.Health <= 0)
				ChangeMatchState(EARKMatchState::GameOver);
			else
				ChangeMatchState(EARKMatchState::ThrowingBall);

			break;
		}
	}
}

void FARKGameStateGame::OnBallLost()
{
	UE_LOG(GameLog, Warning, TEXT("You have no balls!"));
	ChangeMatchState(EARKMatchState::LosingBall);
}

void FARKGameStateGame::OnObstacleHit(uint32 _tileIndex)
{
	FARKGridTile* tile = Grid->GetTile(_tileIndex);
	tile->Health -= 1;

	UARKObstacleWidget* obstacleWidget = GameRenderData.Obstacles[_tileIndex];
	obstacleWidget->SetHealthVis(tile->Health);

	MatchData.Points += 1;
}

void FARKGameStateGame::OnObstacleDestroyed(uint32 _tileIndex)
{
	SpawnPowerUp(_tileIndex);
	Grid->ChangeObstacleToDefault(_tileIndex);

	if(Grid->NumOfObstacles <= 0)
		ChangeMatchState(EARKMatchState::NextLevel);
}

void FARKGameStateGame::OnPowerUpPicked(EARKPowerUpType _powerUpType)
{
	GameSimulationData.Paddle.ActivePowerUp = _powerUpType;

	//auto use
	switch (_powerUpType)
	{
		case EARKPowerUpType::ExpandPaddle:
		{
			GameSimulationData.Paddle.Size = FVector2D(TileSize.X * 4.0f, TileSize.Y);
			break;
		}
		default:
		{
			GameSimulationData.Paddle.Size = FVector2D(TileSize.X * 2.0f, TileSize.Y);
			break;
		}
	}
}

void FARKGameStateGame::SpawnPowerUp(uint32 _tileIndex)
{
	EARKPowerUpType powerUpType = PowerUpsBag.GetElement();
	if (powerUpType != EARKPowerUpType::MAX)
	{
		FVector2D& obstaclePosition = GameSimulationData.Obstacles[_tileIndex].Position;
		FVector2D& obstacleSize = GameSimulationData.Obstacles[_tileIndex].Size;

		uint16 lastPowerUpIndex;
		UARKPowerUpWidget* powerUpWidget = GameRenderData.GetNextPowerUp(&lastPowerUpIndex);

		powerUpWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);

		FARKGameplayPowerUpData powerUpData;

		FVector2D& powerUpPosition = powerUpData.Position;
		powerUpPosition.X = obstaclePosition.X + (obstacleSize.X / 2.0f);
		powerUpPosition.Y = obstaclePosition.Y + (obstacleSize.Y / 2.0f);

		powerUpData.Type = powerUpType;
		powerUpData.WidgetId = lastPowerUpIndex;

		GameSimulationData.PowerUps.Add(powerUpData);

		//
		UARKGameSettings* gameSettings = GameInstance->GetARKGameSettings();
		const FText& powerUpName = gameSettings->TypeNames.PowerUpNames[(uint8)powerUpType].Text;

		UARKPowerUpInfoWidget* powerUpInfoWidget = GameRenderData.PowerUpInfos[powerUpData.WidgetId];
		powerUpInfoWidget->SetPowerUpName(powerUpName);
		powerUpInfoWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	}
}

void FARKGameStateGame::UsePowerUp()
{
	EARKPowerUpType activePowerUp = GameSimulationData.Paddle.ActivePowerUp;
	if (activePowerUp != EARKPowerUpType::MAX)
	{
		switch (activePowerUp)
		{
			case EARKPowerUpType::ExplodingBall:
			{
				FARKGameplayBallData& firstBallData = GameSimulationData.Balls[0];
				//ball obstacle collision
				FIntPoint ballGridPosition;
				ViewportPositionToGridPosition(firstBallData.Position, ballGridPosition);

				uint32 ballTileIndex = UINT32_MAX;
				FARKGridTile* ballTile = Grid->GetTile(ballGridPosition.X, ballGridPosition.Y, &ballTileIndex);

				if (ballTile)
				{
					TArray<FIntPoint> neighbourPositions;
					for (int32 i = -8; i < 8; ++i)
					{
						if (i == 0)
							continue;

						neighbourPositions.Add(FIntPoint(ballGridPosition.X + i, ballGridPosition.Y));
						neighbourPositions.Add(FIntPoint(ballGridPosition.X, ballGridPosition.Y + i));
					}

					for (int32 i = 0; i < neighbourPositions.Num(); i++)
					{
						uint32 neighbourIndex = UINT32_MAX;
						FARKGridTile* neighbourTile = Grid->GetTile(neighbourPositions[i].X, neighbourPositions[i].Y, &neighbourIndex);
						if (neighbourTile)
						{
							if (neighbourTile->Type == EARKGridTileType::Obstacle)
							{
								neighbourTile->Health = 0;

								MatchData.Points += 1;

								GameRenderData.Obstacles[neighbourIndex]->SetVisibility(ESlateVisibility::Hidden);
								OnObstacleDestroyed(neighbourIndex);
							}
						}
					}
				}
				GameSimulationData.Paddle.ActivePowerUp = EARKPowerUpType::MAX;
				break;
			}

			case EARKPowerUpType::MultiBall:
			{
				//add balls
				uint16 ballsCount = 3;
				FARKGameplayBallData& firstBallData = GameSimulationData.Balls[0];
				for (uint16 i = 0; i < ballsCount; ++i)
				{
					uint16 lastBallIndex;
					UARKBallWidget* ballWidget = GameRenderData.GetNextBall(&lastBallIndex);
					ballWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);

					FARKGameplayBallData ballData;

					if (UCanvasPanelSlot* ballSlot = Cast<UCanvasPanelSlot>(ballWidget->Slot))
					{
						ballSlot->SetPosition(firstBallData.Position);
					}

					ballData.Position = firstBallData.Position;

					ballData.Speed = FMath::RandRange(10, 15);
					FVector2D ballDirection(0, -1);
					ballDirection = ballDirection.GetRotated((FMath::RandRange(0, 3) * 90) + 45);
					ballDirection.Normalize();

					ballData.Direction = ballDirection;
					ballData.WidgetId = lastBallIndex;

					FVector2D newBallPos = ballData.Position + (ballData.Direction * 1.5f * ballData.Speed);
					if (newBallPos.X <= 0 || newBallPos.Y <= 0 || newBallPos.X >= FixedResolution.X || newBallPos.Y >= FixedResolution.Y)
					{
						//do not add if outside the level
					}
					else
					{
						GameSimulationData.Balls.Add(ballData);
					}	
				}
				GameSimulationData.Paddle.ActivePowerUp = EARKPowerUpType::MAX;
				break;
			}
		}
	}
}


const FARKLevelGenerationData& FARKGameStateGame::GetCurrentLevelData()
{
	return LevelGenerationDatas[CurrentLevelIndex];
}

const FARKLevelGenerationData& FARKGameStateGame::GetNextLevelData()
{
	CurrentLevelIndex++;
	CurrentLevelIndex %= LevelGenerationDatas.Num();

	return LevelGenerationDatas[CurrentLevelIndex];
}

void FARKGameStateGame::ViewportPositionToGridPosition(const FVector2D& _position, FIntPoint& _gridPosition)
{
	_gridPosition.X = FMath::Clamp((uint16)(_position.X / TileSize.X), (uint16)0, (uint16)(Grid->SizeX - 1));
	_gridPosition.Y = FMath::Clamp((uint16)(_position.Y / TileSize.Y), (uint16)0, (uint16)(Grid->SizeY - 1));
}

bool FARKGameStateGame::CollideBallWithObstacle(const FVector2D& _ballPosition, const FVector2D& _ballPositionDelta, const FVector2D& _ballDirection, const FVector2D& _obstaclePosition, const FVector2D& _obstacleSize, FVector2D& _newBallDirection)
{
	FVector2D ballPosition = _ballPosition;

	float left = _obstaclePosition.X;
	float right = _obstaclePosition.X + _obstacleSize.X;
	float top = _obstaclePosition.Y;
	float bottom = _obstaclePosition.Y + _obstacleSize.Y;

	bool bBallInObstacle = _ballPosition.X >= left && _ballPosition.X <= right && _ballPosition.Y >= top && _ballPosition.Y <= bottom;
	if (bBallInObstacle)
	{
		ballPosition -= _ballPositionDelta;

		TArray<FVector4> edges = { FVector4(left, top, right, top), FVector4(right, top, right, bottom), FVector4(right, bottom, left, bottom), FVector4(left, bottom, left, top) };
		TArray<FVector2D> normals = { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };

		uint8 closestPointIndex = 254;
		float minDist = FLT_MAX;
		for (uint8 i = 0; i < edges.Num(); ++i)
		{
			FVector4& edge = edges[i];
			FVector2D pointOnSegment = FMath::ClosestPointOnSegment2D(ballPosition, FVector2D(edge.X, edge.Y), FVector2D(edge.Z, edge.W));
			float dist = (ballPosition - pointOnSegment).SizeSquared();
			if (dist < minDist)
			{
				minDist = dist;
				closestPointIndex = i;
			}
		}

		if (closestPointIndex != 254)
		{
			_newBallDirection = Reflect2DVector(_ballDirection, normals[closestPointIndex]);
			return true;
		}

		ensure(false);
		UE_LOG(GameLog, Warning, TEXT("Ball is inside, but didn't reflect!"));
	}

	return false;
}