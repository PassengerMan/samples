// krolbakal@gmail.com

#pragma once

#include "ARKGameStateBase.h"

class FARKGameStateMenu : public FARKGameStateBase
{
public:
	FARKGameStateMenu() {};
	virtual ~FARKGameStateMenu() {};

	virtual void InitBase() override;

	virtual void Enter() override;
	virtual void Exit() override;
	virtual void Update(float _deltaTime) override;

protected:
};
