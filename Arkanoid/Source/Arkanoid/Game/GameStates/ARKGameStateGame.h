// krolbakal@gmail.com

#pragma once

#include "ARKGameStateBase.h"

class FARKGameStateGame : public FARKGameStateBase
{
public:
	FARKGameStateGame() {};
	virtual ~FARKGameStateGame() {};

	virtual void InitBase() override;

	virtual void Enter() override;
	virtual void Exit() override;
	virtual void Update(float _deltaTime) override;

protected:

	FARKGrid* Grid;
	FARKGameSimulationData GameSimulationData;
	FARKGameRenderData GameRenderData;
	FARKGameInputData GameInputData;

	TArray<FARKLevelGenerationData> LevelGenerationDatas;
	uint8 CurrentLevelIndex;

	FARKMatchData MatchData;

	FARKRandomBag<EARKPowerUpType> PowerUpsBag;

	/*struct FLocalAnimationData
	{
		uint32 TileIndex;
		float Duration;
	};

	TArray<FLocalAnimationData> ObstacleAnimations;*/

	//config
	FIntPoint FixedResolution;
	FIntPoint TileSize;

	//widgets
	class UARKGameWidget* GameWidget;

	//
	void InitGame();

	void RestartGame();

	void UpdateVisuals(float _deltaTime);
	void UpdateInput(float _deltaTime);
	void UpdateGame(float _deltaTime);

	void UpdateAnimation(float _deltaTime);

	//
	void ChangeMatchState(EARKMatchState _matchState);
	void OnChangedMatchState(EARKMatchState _matchState);

	//
	void OnBallLost();
	void OnObstacleHit(uint32 _tileIndex);
	void OnObstacleDestroyed(uint32 _tileIndex);
	void OnPowerUpPicked(EARKPowerUpType _powerUpType);


	void SpawnPowerUp(uint32 _tileIndex);

	void UsePowerUp();

	const FARKLevelGenerationData& GetCurrentLevelData();
	const FARKLevelGenerationData& GetNextLevelData();

	void ViewportPositionToGridPosition(const FVector2D& _position, FIntPoint& _gridPosition);
	bool CollideBallWithObstacle(const FVector2D& _ballPosition, const FVector2D& _ballPositionDelta, const FVector2D& _ballDirection, const FVector2D& _obstaclePosition, const FVector2D& _obstacleSize, FVector2D& _newBallDirection);
};
