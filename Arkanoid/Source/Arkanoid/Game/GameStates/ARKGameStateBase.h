// krolbakal@gmail.com

#pragma once

#include "Arkanoid.h"

class FARKGameStateBase
{
public:
	FARKGameStateBase() {};
	virtual ~FARKGameStateBase() {};

	void Init(class UARKGameInstance* _gameInstance, EARKGameState _gameStateType);

	virtual void InitBase() {};

	virtual void Enter() {};
	virtual void Exit() {};
	virtual void Update(float _deltaTime) = 0;

	class UWorld* GetWorld();
protected:
	EARKGameState GameStateType;
	class UARKGameInstance* GameInstance;
	
};
