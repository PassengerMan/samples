// krolbakal@gmail.com

#include "ARKGameStateBase.h"
#include "ARKGameInstance.h"

void FARKGameStateBase::Init(class UARKGameInstance* _gameInstance, EARKGameState _gameStateType)
{
	GameInstance = _gameInstance;
	GameStateType = _gameStateType;
	InitBase();
}

class UWorld* FARKGameStateBase::GetWorld()
{
	return GameInstance != nullptr ? GameInstance->GetWorld() : nullptr;
}