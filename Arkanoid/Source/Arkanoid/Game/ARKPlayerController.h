// krolbakal@gmail.com

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ARKPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API AARKPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AARKPlayerController(const FObjectInitializer& _oi);
	
	virtual void SetupInputComponent() override;
};
