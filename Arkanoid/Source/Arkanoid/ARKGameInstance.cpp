// krolbakal@gmail.com

#include "ARKGameInstance.h"
#include "ARKGameSettings.h"

#include "Game/UI/Widgets/ARKFullscreenWidget.h"

void UARKGameInstance::Init()
{
	Super::Init();
}

void UARKGameInstance::Shutdown()
{

	Super::Shutdown();
}

void UARKGameInstance::InitFullscreenWidget()
{
	UARKGameSettings* gameSettings = GetARKGameSettings();
	UClass* fullscreenWidgetClass = gameSettings->WidgetTemplates.FullscreenWidgetTemplate;
	if (fullscreenWidgetClass)
	{
		FullscreenWidget = CreateWidget<UARKFullscreenWidget>(this, fullscreenWidgetClass);
		FullscreenWidget->AddToViewport();
	}
}

class UARKGameWidget* UARKGameInstance::GetARKGameWidget()
{
	return FullscreenWidget->GetARKGameWidget();
}

UARKGameSettings* UARKGameInstance::GetARKGameSettings()
{
	return GameSettingsTemplate != nullptr ? GameSettingsTemplate->GetDefaultObject<UARKGameSettings>() : nullptr;
}

