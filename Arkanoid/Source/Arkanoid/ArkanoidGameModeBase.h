// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Arkanoid.h"
#include "GameFramework/GameModeBase.h"
#include "ArkanoidGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API AArkanoidGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AArkanoidGameModeBase(const FObjectInitializer& _oi);
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason);
	virtual void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;

	void SetGameState(EARKGameState _newGameState);

	class UARKGameInstance* GetARKGameInstance();
	class UARKGameSettings* GetARKGameSettings();
protected:
	EARKGameState PreviousGameState;
	EARKGameState CurrentGameState;

	class FARKGameStateBase* GameStates[EARKGameState::MAX];
};
