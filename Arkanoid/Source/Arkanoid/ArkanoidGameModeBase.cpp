// Fill out your copyright notice in the Description page of Project Settings.

#include "ArkanoidGameModeBase.h"

#include "ARKGameInstance.h"
#include "ArkGameSettings.h"
#include "Game/GameStates/ARKGameStateBase.h"
#include "Game/GameStates/ARKGameStateMenu.h"
#include "Game/GameStates/ARKGameStateGame.h"

#include "Game/ARKPlayerController.h"

AArkanoidGameModeBase::AArkanoidGameModeBase(const FObjectInitializer& _oi) : Super(_oi)
{
	PrimaryActorTick.bCanEverTick = true;

	PlayerControllerClass = AARKPlayerController::StaticClass();
}

void AArkanoidGameModeBase::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	GetARKGameInstance()->InitFullscreenWidget();

	PreviousGameState = CurrentGameState = EARKGameState::MAX;

	GameStates[(uint8)EARKGameState::Menu] = new FARKGameStateMenu();
	GameStates[(uint8)EARKGameState::Menu]->Init(GetARKGameInstance(), EARKGameState::Menu);
	GameStates[(uint8)EARKGameState::Game] = new FARKGameStateGame();
	GameStates[(uint8)EARKGameState::Game]->Init(GetARKGameInstance(), EARKGameState::Game);

	SetGameState(EARKGameState::Game);
}

void AArkanoidGameModeBase::BeginPlay()
{
	Super::BeginPlay();

}

void AArkanoidGameModeBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	for (uint8 i = 0; i < (uint8)EARKGameState::MAX; i++)
	{
		delete GameStates[i];
	}

	Super::EndPlay(EndPlayReason);
}

void AArkanoidGameModeBase::TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{
	Super::TickActor(DeltaTime, TickType, ThisTickFunction);

	FARKGameStateBase* currentGameState = GameStates[(uint8)CurrentGameState];
	if (currentGameState)
	{
		currentGameState->Update(DeltaTime);
	}
}

void AArkanoidGameModeBase::SetGameState(EARKGameState _newGameState)
{
	PreviousGameState = CurrentGameState;
	CurrentGameState = _newGameState;

	if (PreviousGameState != EARKGameState::MAX)
	{
		GameStates[(uint8)PreviousGameState]->Exit();
	}

	GameStates[(uint8)CurrentGameState]->Enter();
}

UARKGameInstance* AArkanoidGameModeBase::GetARKGameInstance()
{
	return Cast<UARKGameInstance>(GetGameInstance());
}

UARKGameSettings* AArkanoidGameModeBase::GetARKGameSettings()
{
	if (UARKGameInstance* gameInstance = GetARKGameInstance())
		return gameInstance->GetARKGameSettings();

	return nullptr;
}
